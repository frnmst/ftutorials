# python-makefile
# MIT License
# Copyright (C) 2024 Franco Masotti (see /README.md)
# https://software.franco.net.eu.org/frnmst/python-makefile

# Change these two. Required.
PROJECT_NAME := ftutorials
PYTHON_MODULE_NAME := ftutorials

# Required.
MAKEFILE_SOURCE := https://software.franco.net.eu.org/frnmst/python-makefile/raw/branch/master/Makefile.example
bootstrap:
	curl -o Makefile $(MAKEFILE_SOURCE)

# Add extra targets here. Optional.
check-docs-deps:
	which dot || { echo 'Graphviz missing: install graphviz' && exit 1; }

epub: check-docs-deps
	# See
	# https://github.com/sphinx-doc/sphinx/commit/6d900c34f12db0d21c581c58a5dd38ab49c8ea35
	$(VENV_ACTIVATE) \
		&& $(MAKE) -C docs epub -e SPHINXOPTS="-Dsuppress_warnings=true"; \
		$(VENV_DEACTIVATE)

translate:
	$(VENV_ACTIVATE) \
		&& $(MAKE) -C docs -e SPHINXOPTS="-Dlanguage='it'" html; \
		$(VENV_DEACTIVATE)

doc-translate-update: clean check-docs-deps
	$(VENV_ACTIVATE) \
		&& $(MAKE) -C docs gettext; \
		$(VENV_DEACTIVATE)
	$(VENV_ACTIVATE) \
		&& $(MAKE) -C docs -e SPHINXOPTS="-Dlanguage='it'" html; \
		$(VENV_DEACTIVATE)

doc-translate-generate: clean check-docs-deps
	$(VENV_ACTIVATE) \
		&& $(MAKE) -C docs gettext; \
		$(VENV_DEACTIVATE)
	$(VENV_ACTIVATE) \
		&& cd docs; sphinx-intl update -p _build/gettext -l it; \
		$(VENV_DEACTIVATE)
	$(VENV_ACTIVATE) \
		&& $(MAKE) -C docs -e SPHINXOPTS="-Dlanguage='it'" html; \
		$(VENV_DEACTIVATE)

# Override targets in the Makefile. Optional.
OVERRIDE_CLEAN := true
clean::
	rm -rf build dist *.egg-info
	$(VENV_ACTIVATE) \
		&& $(MAKE) -C docs clean; \
		$(VENV_DEACTIVATE)

OVERRIDE_DOC := true
doc:: check-docs-deps
	$(VENV_ACTIVATE) \
		&& $(MAKE) -C docs html; \
		$(VENV_DEACTIVATE)

#		&& $(MAKE) -C docs latexpdf; \

OVERRIDE_INSTALL := true
install::
	@echo "setup not available for this project"

OVERRIDE_UNINSTALL := true
uninstall::
	@echo "setup not available for this project"
