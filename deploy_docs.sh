#!/usr/bin/env bash
#
# ftutorials (c) by Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# ftutorials is licensed under a
# Creative Commons Attribution-ShareAlike 4.0 International License.
#
# You should have received a copy of the license along with this
# work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

set -euo pipefail

VENV_CMD=". .venv/bin/activate"
CODE_REMOTE_BRANCH='master'
CODE_REMOTE='origin'
DOCS_REMOTE_BRACH='docs'
DOCS_REMOTE='deploy-docs'
GIT_CLONE_TMP_DIR='/tmp/ftutorials'

# Space separated string of extra languages, excluding 'en'.
EXTRA_LANGUAGES="it"

# Get remote.
docs_remote_git_subcommand="$(git remote -v | grep "${DOCS_REMOTE}" | head -n1 | awk '{print $1,$2}')"

# Clean working directory and compile.
rm -rf "${GIT_CLONE_TMP_DIR}" \
    && git clone "$(pwd)" "${GIT_CLONE_TMP_DIR}" \
    && pushd "${GIT_CLONE_TMP_DIR}" \
    && git remote add ${docs_remote_git_subcommand} \
    && make -f .project.mk bootstrap \
    && make install-dev \
    && make clean \
    && rm -rf ~/html \

# Process English.
make doc \
    && mkdir -p ~/html/en \
    && cp -aR docs/_build/html/* ~/html/en

# Compile the translations.
${VENV_CMD} && make -C docs gettext && deactivate
for l in ${EXTRA_LANGUAGES}; do
    if [ "${l}" != 'en' ]; then
        ${VENV_CMD} \
            && make -C docs -e SPHINXOPTS="-Dlanguage='${l}'" html \
            && mkdir -p  ~/html/"${l}" \
            && mv docs/_build/html/* ~/html/"${l}" \
            && deactivate
    fi
done

# Create an empty branch.
git checkout --orphan "${DOCS_REMOTE_BRACH}"
git rm --cached -r .
git clean -f \
    && git clean -f -d

# Deploy.
mv ~/html/* . \
    && git add -A \
    && git commit --no-verify -m "New release." \
    && git push --force "${DOCS_REMOTE}" "${DOCS_REMOTE_BRACH}" \
    && git checkout "${CODE_REMOTE_BRANCH}"

popd
