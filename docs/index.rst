ftutorials
==========

This documentation is about deployments, tips, advices originated and adapted
from various sources. Those resources helped me solved some problems faster than
looking for solutions myself, for example through the manuals.

.. important:: Most instructions reported here refer to the oldstable version of
               Debian GNU/Linux, x86-64 architecture.
               As of today it is Debian 12, codenamed *Bookworm*.

.. toctree::
   :maxdepth: 4
   :glob:

   prerequisites
   content/index
   meta
