Prerequisites
=============

Explanation
-----------

Script and services are placed in a home directory called ``jobs``

- scripts: ``/home/jobs/scripts/by-user/${running_username}``
- Systemd service files: ``/home/jobs/services/by-user/${running_username}``

.. _jobs instructions:

Instructions
------------

All instructions must be run be the ``root`` user by default.
You should access the root user with

.. code-block:: shell-session

   sudo -i

Specific instructions are available

- when new users must be created
- when commands need to be run be users other than ``root``

#. create the jobs user and the directories

   .. code-block:: shell-session

      useradd -m -s /bin/bash -U jobs
      mkdir -p /home/jobs/{script,services}/by-user
      chown -R jobs:jobs /home/jobs
      chmod -R 070 /home/jobs

#. add the running username to the jobs group

   .. code-block:: shell-session

      usermod -aG jobs ${running_username}

#. when you change a service file run the :download:`deploy script <includes/home/jobs/services/deploy.py>` as ``root``.
   This _`deploy script` will copy the Systemd files in the appropriate directories and
   run and enable services and timers. You need to install:

   - Python 3
   - `fpyutils <https://pypi.org/project/fpyutils/>`_ (see also the next section)

   to be able to run this script

   .. literalinclude:: includes/home/jobs/services/deploy.py
      :language: python
      :linenos:
      :caption: /home/jobs/services/deploy.py

.. _installation of fpyutils:

Installation of fpyutils
------------------------

The recommended way, for the moment, is to install fpyutils globally
(avaiable for all users)

.. tabs::

   .. tab:: Debian

      .. tabs::

         .. tab:: Package manager

            - `Bookworm package <https://packages.debian.org/bookworm/python3-fpyutils>`_
            - `Sid package <https://packages.debian.org/sid/python3-fpyutils>`_

         .. tab:: Manual install

            #. manually install the dependencies using APT

               .. code-block:: shell-session

                  apt-get install python3-requests python3-atomicwrites

            #. change the PIP repository. Update the :download:`configuration file <includes/root/.config/pip/pip.conf>`

               .. literalinclude:: includes/root/.config/pip/pip.conf
                  :language: ini
                  :linenos:
                  :caption: /root/.config/pip/pip.conf

            #. install with pip

               .. warning:: This is a dangerous operation. See:

                            - https://pages.charlesreid1.com/dont-sudo-pip
                            - https://stackoverflow.com/questions/21055859/what-are-the-risks-of-running-sudo-pip

               .. code-block:: shell-session

                  pip3 install fpyutils>=2.2

   .. tab:: Ubuntu

      `Ubuntu package <https://packages.ubuntu.com/jammy/python3-fpyutils>`_

   .. tab:: ArchLinux

      `AUR package <https://aur.archlinux.org/packages/python-fpyutils>`_

   .. tab:: NixOS

      `NixOS packages <https://search.nixos.org/packages?channel=unstable&from=0&size=50&sort=relevance&type=packages&query=fpyutils>`_
