# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

sys.path.insert(0, os.path.abspath('..'))

# -- Project information -----------------------------------------------------

project = 'ftutorials'
copyright = '2022-2024, Franco Masotti - Creative Commons Attribution-ShareAlike 4.0 International License, unless otherwise specified'
author = 'Franco Masotti'

# Enable for epub output.
version = 'dev'

# Use index.rst.
master_doc = 'index'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc', 'sphinx.ext.coverage', 'sphinx.ext.graphviz',
    'sphinx_tabs.tabs', 'sphinx_copybutton'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'default'

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_book_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# -- Extension configuration -------------------------------------------------

# -- Options for todo extension ----------------------------------------------

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = False

html_baseurl = 'https://docs.franco.net.eu.org/ftutorials/'

html_theme_options = {
    'repository_provider':
    'github',
    'repository_url':
    'https://software.franco.net.eu.org/frnmst/ftutorials',
    'use_repository_button':
    True,
    'use_download_button':
    True,
    'use_issues_button':
    True,
    'announcement':
    ('⚠️at this moment ftutorials is still a rolling release documentation, '
     'things change often!⚠️'),
}

latex_engine = 'xelatex'
latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    #
    'papersize': 'a4paper',
    # The font size ('10pt', '11pt' or '12pt').
    #
    # 'pointsize': '10pt',

    # Additional stuff for the LaTeX preamble.
    #
    # 'preamble': '',

    # Latex figure (float) alignment
    #
    # 'figure_align': 'htbp',
}

# sphinx-intl
locale_dirs = ['locale/']  # path is example but recommended.
gettext_compact = False  # optional.

# These paths are either relative to html_static_path
# or fully qualified paths (eg. https://...)
html_css_files = [
    'css/custom.css',
]

pygments_style = 'default'
html_last_updated_fmt = '%Y-%m-%d %H:%M:%S %z'
copybutton_line_continuation_character = '\\'

# Epub.
epub_theme = 'epub'
epub_author = 'Franco Masotti'
epub_theme_options = {
    'relbar1': False,
    'footer': False,
}
epub_css_style = [
    'css/epub.css',
]
