Firefox
=======

Disable search in address bar
-----------------------------

These settings disable search engine queries in the search bar.

.. seealso::

   - _`I want to disable search in the address bar and browser.urlbar.unifiedcomplete is not an option anymore | Firefox Support Forum | Mozilla Support` [#f1]_

#. open Firefox
#. open ``about:config`` in the address bar
#. Set these values to false

   ::

        keyword.enabled                     false
        browser.fixup.alternate.enabled     false

Profiles
--------

You can handle multiple Firefox profiles in separate sandboxes with this script

.. seealso::

   - A collection of scripts I have written and/or adapted that I currently use on my systems as automated tasks [#f2]_
   - _`GitHub - netblue30/firejail: Linux namespaces and seccomp-bpf sandbox` [#f3]_

#. install the dependencies

   .. code-block:: shell-session

      apt-get install firejail/bullseye-backports yad python3-yaml firefox-esr

   .. note:: You need to enable the backports before installing firejail. The upstream
             repository `recommends <https://github.com/netblue30/firejail#debian>`_ this version for Debian.

#. install fpyutils. See :ref:`reference <installation of fpyutils>`
#. create the jobs directories. See :ref:`reference <jobs instructions>`

   .. code-block:: shell-session

      mkdir -p /home/jobs/scripts/by-user/myuser
      chown -R myuser:myuser /home/jobs/scripts/by-user/myuser
      chmod 700 -R /home/jobs/scripts/by-user/myuser
      usermod -aG jobs myuser

#. create the :download:`script <includes/home/jobs/scripts/myuser/firefox_profile_runner.py>`

   .. literalinclude:: includes/home/jobs/scripts/myuser/firefox_profile_runner.py
      :language: python
      :caption: /home/jobs/scripts/myuser/firefox_profile_runner.py

#. import the :download:`configuration file <includes/home/jobs/scripts/myuser/firefox_profile_runner.yaml>`

   .. literalinclude:: includes/home/jobs/scripts/myuser/firefox_profile_runner.yaml
      :language: yaml
      :caption: /home/jobs/scripts/myuser/firefox_profile_runner.yaml

#. if your configuration uses private home directories through the
   ``--private=`` option you must create them before running the script
#. go back to your regular desktop user

   .. code-block:: shell-session

      exit

#. install `python-yad <https://pypi.org/project/yad/>`_

   .. code-block:: shell-session

      pip3 install --user yad

#. run the script

   .. code-block:: shell-session

      pushd /home/jobs/scripts/by-user/myuser && ./firefox_profile_runner.py ./firefox_profile_runner.yaml && popd

   .. note:: You can create, for example, a launcher on your desktop or a
             key combination to launch the script. This is what I use in my
             specrtwm configuration

             .. code-block:: ini

                program[firefox]     = bash -c "pushd /home/jobs/scripts/by-user/myuser && ./firefox_profile_runner.py ./firefox_profile_runner.yaml 1>/dev/null 2>/dev/null &" 1/dev/null 2>/dev/null'
                bind[firefox]        = MOD+i

Block all domains except one
----------------------------

If you use Firefox profiles for different purposes you might need to block
some domains. I use this for example for my Gitea Firefox profile:
when Gitea renders a markdown file, and there are images hosted
on external websites, you can block them like this:

#. open Firefox
#. install the `uBlock Origin <https://ublockorigin.com/>`_ extension
#. go into uBlock Origin's settings and open the ``My Filters`` section
#. add this content

   .. code-block:: ini

      https://*.*
      http://*.*
      @@||my.domain.org

   where ``my.domain.org`` is the whitelisted domain

Disable all images
------------------

Set ``permissions.default.images = 2`` in about:config

.. rubric:: Footnotes

.. [#f1] https://support.mozilla.org/en-US/questions/1213978 unknown license
.. [#f2] https://software.franco.net.eu.org/frnmst/automated-tasks GNU GPLv3+, copyright (c) 2019-2022, Franco Masotti
.. [#f3] https://github.com/netblue30/firejail GNU GPL v2.0, Copyright (C) 2014-2022, Firejail Authors
