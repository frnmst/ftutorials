Videos
======

Download videos from various platforms.

.. seealso::

   - A collection of scripts I have written and/or adapted that I currently use on my systems as automated tasks [#f1]_
   - _`youtube-dl - ArchWiki` [#f2]_
   - _`GitHub - yt-dlp/yt-dlp: A youtube-dl fork with additional features and fixes` [#f3]_

#. install the dependencies

   .. code-block:: shell-session

      apt-get install python3-yaml aria2 yt-dlp/bullseye-backports

   .. note:: If you use the provided :ref:`options file <all the options used by yt-dlp>` as-is you need to install aria2

   .. note:: You need to enable the backports before installing yt-dlp

#. install fpyutils. See :ref:`reference <installation of fpyutils>`
#. add the user to the jobs group

   .. code-block:: shell-session

      usermod -aG jobs myuser

#. create the jobs directories. See :ref:`reference <jobs instructions>`

   .. code-block:: shell-session

       mkdir -p /home/jobs/{scripts,services}/by-user/myuser
       chmod 700 /home/jobs/{scripts,services}/by-user/myuser

#. create the :download:`script <includes/home/jobs/scripts/by-user/myuser/youtube_dl.py>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/myuser/youtube_dl.py
      :language: python
      :caption: /home/jobs/scripts/by-user/myuser/youtube_dl.py

#. create a :download:`configuration file <includes/home/jobs/scripts/by-user/myuser/youtube_dl.some_subject.yaml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/myuser/youtube_dl.some_subject.yaml
      :language: yaml
      :caption: /home/jobs/scripts/by-user/myuser/youtube_dl.some_subject.yaml

#. create the :download:`options file <includes/home/jobs/scripts/by-user/myuser/youtube_dl.some_subject.options>` which select
   _`all the options used by yt-dlp`

   .. literalinclude:: includes/home/jobs/scripts/by-user/myuser/youtube_dl.some_subject.options
      :language: ini
      :caption: /home/jobs/scripts/by-user/myuser/youtube_dl.some_subject.options

#. create a :download:`text file <includes/home/jobs/scripts/by-user/myuser/youtube_dl.some_subject.txt>` containing URLs of channels or playlists, one for each line

   .. literalinclude:: includes/home/jobs/scripts/by-user/myuser/youtube_dl.some_subject.txt
      :language: text
      :caption: /home/jobs/scripts/by-user/myuser/youtube_dl.some_subject.txt

#. create the :download:`Systemd service unit file <includes/home/jobs/services/by-user/myuser/youtube-dl.some_subject.service>`

   .. literalinclude:: includes/home/jobs/services/by-user/myuser/youtube-dl.some_subject.service
      :language: ini
      :caption: /home/jobs/services/by-user/myuser/youtube-dl.some_subject.service

#. create the :download:`Systemd service timer unit file <includes/home/jobs/services/by-user/myuser/youtube-dl.some_subject.timer>`

   .. literalinclude:: includes/home/jobs/services/by-user/myuser/youtube-dl.some_subject.timer
      :language: ini
      :caption: /home/jobs/services/by-user/myuser/youtube-dl.some_subject.timer

#. fix the permissions

   .. code-block:: shell-session

      chown -R myuser:myuser /home/jobs/{scripts,services}/by-user/myuser
      chmod 700 -R /home/jobs/{scripts,services}/by-user/myuser

#. run the :ref:`deploy script <deploy script>`

.. rubric:: Footnotes

.. [#f1] https://software.franco.net.eu.org/frnmst/automated-tasks GNU GPLv3+, copyright (c) 2019-2022, Franco Masotti
.. [#f2] https://wiki.archlinux.org/index.php/Youtube-dl GNU Free Documentation License 1.3 or later,  Copyright (c) ArchWiki contributors
.. [#f3] https://github.com/yt-dlp/yt-dlp Unilicense
