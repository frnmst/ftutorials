Invoices
========

I use this script to download, archive and print invoice files
which are originally generated and handled by *Sogei SpA* and the *Agenzia delle Entrate*.

Invoice files are downloaded from
`PEC accounts (certified mail) <https://en.wikipedia.org/wiki/Certified_email>`_
as attachments. An HTML file corresponding to the decoded XML invoice file
is archived and printed. Finally optional notifications about the operation are sent.

During this process cryptographical signatures and integrity checks are performed.

.. seealso::

   - A collection of scripts I have written and/or adapted that I currently use on my systems as automated tasks [#f1]_
   - _`scripts/getmail.py at master · markuz/scripts · GitHub` [#f2]_

.. important:: `CUPS <https://www.cups.org/>`_ must be up and running with a default printer set.
               If not, do this before proceeding.

#. install the dependencies

   .. code-block:: shell-session

      apt-get install python3-pip libcups2-dev ttf-dejavu

#. create the jobs directories. See :ref:`reference <jobs instructions>`

   .. code-block:: shell-session

      mkdir -p /home/jobs/{scripts,services}/by-user/myuser
      chown -R myuser:myuser /home/jobs/{scripts,services}/by-user/myuser
      chmod 700 -R /home/jobs/{scripts,services}/by-user/myuser

#. create the :download:`script <includes/home/jobs/scripts/by-user/myuser/archive_invoice_files.py>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/myuser/archive_invoice_files.py
      :language: python
      :caption: /home/jobs/scripts/by-user/myuser/archive_invoice_files.py

#. create a :download:`configuration file <includes/home/jobs/scripts/by-user/myuser/archive_invoice_files.myuser.yaml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/myuser/archive_invoice_files.myuser.yaml
      :language: yaml
      :caption: /home/jobs/scripts/by-user/myuser/archive_invoice_files.myuser.yaml

#. use this :download:`Systemd service unit file <includes/home/jobs/services/by-user/myuser/archive-invoice-files.myuser.service>`

   .. literalinclude:: includes/home/jobs/services/by-user/myuser/archive-invoice-files.myuser.service
      :language: ini
      :caption: /home/jobs/services/by-user/myuser/archive-invoice-files.myuser.service

#. use this :download:`Systemd timer unit file <includes/home/jobs/services/by-user/myuser/archive-invoice-files.myuser.timer>`

   .. literalinclude:: includes/home/jobs/services/by-user/myuser/archive-invoice-files.myuser.timer
      :language: ini
      :caption: /home/jobs/services/by-user/myuser/archive-invoice-files.myuser.timer

#. fix the permissions

   .. code-block:: shell-session

      chown -R myuser:myuser /home/jobs/{scripts,services}/by-user/myuser
      chmod 700 -R /home/jobs/{scripts,services}/by-user/myuser

#. run the :ref:`deploy script <deploy script>`
#. go back to the desktop user create a new virtual environment

   .. code-block:: shell-session

      exit
      export ENVIRONMENT_NAME="archive_invoice_files"
      python3 -m venv ~/.local/venv/"${ENVIRONMENT_NAME}"
      . ~/.local/venv/"${ENVIRONMENT_NAME}"/bin/activate
      pip3 install wheel
      pip3 install fpyutils==3.0.1 python-dateutil fattura-elettronica-reader==3.0.5 WeasyPrint==52.1 pycups
      deactivate

.. rubric:: Footnotes

.. [#f1] https://software.franco.net.eu.org/frnmst/automated-tasks GNU GPLv3+, copyright (c) 2019-2022, Franco Masotti
.. [#f2] https://github.com/markuz/scripts/blob/master/getmail.py GNU GPL v2+, Copyright (c) 2011 Marco Antonio Islas Cruz
