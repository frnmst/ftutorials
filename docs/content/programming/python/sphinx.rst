Sphinx
======

.. seealso::

   - _`html - How can I avoid the horizontal scrollbar in a ReST table? - Stack Overflow` [#f1]_

.. rubric:: Footnotes

.. [#f1] https://stackoverflow.com/a/40650120 CC BY-SA 4.0, Copyright (c) 2016, 2017 Anudeep Gupta, Pang (at stackoverflow.com)
