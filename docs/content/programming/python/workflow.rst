Workflow
========

Rules
-----

- Assume that the root of the repository is ``./``.
- 3.5 <= Python version < 4

Variable reference
------------------

.. seealso::

   - _`Arch User Repository - ArchWiki` [#f1]_
   - _`Semantic Versioning 2.0.0 | Semantic Versioning` [#f2]_

+--------------------------------+------------------------------------------------------+
| Variable name                  | Description                                          |
+================================+======================================================+
| ``project_python_module_name`` | the name of the project as a Python module           |
+--------------------------------+------------------------------------------------------+
| ``project_directory``          | a relative path that corresponds project name        |
+--------------------------------+------------------------------------------------------+
| ``projects_aur_git_directory`` | the directory of the corresponding AUR package       |
+--------------------------------+------------------------------------------------------+
| ``gpg_signing_key``            | a valid PGP key used to sign commits                 |
+--------------------------------+------------------------------------------------------+
| ``dev_branch``                 | development git branch                               |
+--------------------------------+------------------------------------------------------+
| ``version_id``                 | the version identifier used in the git tag           |
+--------------------------------+------------------------------------------------------+
| ``MAJOR``                      | a variable of the Semantic Versioning document       |
+--------------------------------+------------------------------------------------------+
| ``MINOR``                      | a variable of the Semantic Versioning document       |
+--------------------------------+------------------------------------------------------+
| ``PATCH``                      | a variable of the Semantic Versioning document       |
+--------------------------------+------------------------------------------------------+

Sequence
--------

#. enable git signing

   .. code-block:: shell-session

      git config commit.gpgsign true
      git config user.signingkey ${gpg_signing_key}

#. setup a bare git repository serving static pages.
   `Follow this blog post <https://blog.franco.net.eu.org/post/an-alternative-to-github-pages.html>`_
#. if you need to publish a package on AUR follow the :ref:`instructions <AUR setup>`
#. finish working on the development branch

   .. code-block:: shell-session

      cd ${project_directory}

#. check that the current branch is not ``master``

   .. code-block:: shell-session

      git branch --no-color --show-current
      # dev

#. if you need to publish a new asciinema video follow the :ref:`instructions <asciinema update>`
#. commit

   .. code-block:: shell-session

      git add -A
      git commit
      git push

#. update version numbers

   - ``./setup.cfg``
   - ``./docs/conf.py``
   - ``./packages/*``

#. if necessary update dependencies in

   - ``./requirements.txt``
   - ``./requirements-dev.txt``
   - ``./setup.cfg``

   and regenerate the pinned dependencies

   .. code-block:: shell-session

      make regenerate-freeze

   Read the changelogs of the dependencies and decide what packages to update
#. run a final test

   .. code-block:: shell-session

      make uninstall-dev; make install-dev
      make doc
      make test
      make install
      cd ~ && python3 -c "import ${package_name}" && cd ${OLDPWD}
      make uninstall
      make clean

#. For each of these files update copyright years, emails and contributors

   - ``./README.md``
   - ``./docs/conf.py``
   - ``./docs/misc.rst``
   - all changed Python source files
   - ``./packages/*``

#. commit

   .. code-block:: shell-session

      git add -A
      make pre-commit
      git commit
      git push

#. update the documentation

   .. code-block:: shell-session

      make clean && make doc
      rm -rf ~/html && cp -aR docs/_build/html ~
      git branch --delete --force docs
      git checkout --orphan docs
      rm -rf .mypy_cache .venv
      git rm --cached -r .
      git clean -f && git clean -f -d
      mv ~/html/{*,.buildinfo} .
      git add -A
      git commit --no-verify -m "New release"
      git push --force deploy-docs docs

#. merge the branches and tag the release

   .. code-block:: shell-session

      git checkout master
      git merge ${dev_branch}

#. tag the release.

   .. code-block:: shell-session

      git tag -s -a ${version_id}

   Write a message like this. Use the infinitive tense

   .. code-block::

      Release ${MAJOR}.${MINOR}.${PATCH}

      - Item 1
      - Item 2
      - [ ... ]
      - Item n

#. push the changes

   .. code-block:: shell-session

      git push
      git push --tags

#. upload the package to PyPI if applicable

   .. code-block:: shell-session

      make install-dev
      make clean
      make dist
      make upload

#. upload the package on the software page.
   Follow the instructions reported `here <https://blog.franco.net.eu.org/software/instructions/#upload-what-i-have-to-do>`_
#. update downstream distribution packages. See the :ref:`updating packages <updating packages>` section
#. update other resources if applicable

   - `Free Software Directory <https://directory.fsf.org/wiki/Main_Page>`_

Other steps
-----------

Upstream packages
`````````````````

.. _AUR setup:

AUR
~~~

.. seealso::

   - _`An alternative to GitHub Pages` [#f3]_

#. create an Arch Linux virtual machine
#. create a bare repository

   .. code-block:: shell-session

      git init --bare ${project_directory}
      cd ${project_directory}

#. use this :download:`post-receive <includes/dot_git/hooks/post-receive>` git hook

   .. literalinclude:: includes/dot_git/hooks/post-receive
      :language: shell
      :caption: .git/hooks/post-receive

#. use this :download:`configuration <includes/dot_git/hooks/post-receive.conf>`

   .. literalinclude:: includes/dot_git/hooks/post-receive.conf
      :language: shell
      :caption: .git/hooks/post-receive.conf

#. mark the hook as executable

   .. code-block:: shell-session

      chmod 700 .git/hooks/post-receive

#. add the ``pacman`` command to the ones not requiring password in the sudoers file

   .. code-block:: shell-session

      visudo
      build-user ALL=NOPASSWD: /bin/pacman

#. on the local repository (development machine) create a new remote called
   ``packages-aur`` which points to the Arch Linux virtual machine
#. create a repository containing the AUR source. Add a normal ``origin`` remote
   and a git ``aur`` remote that to points to
   ``https://aur.archlinux.org/"$(basedir "${projects_aur_git_directory}")".git``

.. _updating packages:

Updating packages
`````````````````

AUR
~~~

.. code-block:: shell-session

   git push --atomic packages-aur master ${MAJOR}.${MINOR}.${PATCH}
   make clean
   git branch -D packages-aur && git fetch --all && git checkout packages-aur
   cp PKGBUILD .SRCINFO ${projects_aur_git_directory}
   git checkout ${dev_branch}
   cd ${projects_aur_git_directory}
   git add PKGBUILD .SRCINFO
   git commit -m "New release."
   git push
   git push aur

Asciinema
`````````

.. _asciinema update:

.. tabs::

   .. tab:: Relevant changes

            #. create a new asciinema demo file

               .. code-block:: shell-session

                  cd ./asciinema
                  touch ${project_directory}_asciinema_${MAJOR}_${MINOR}_${PATCH}_demo.sh

            #. edit the demo file
            #. record and check the demo file

               .. code-block:: shell-session

                  . .venv/bin/activate
                  asciinema rec --command=./${project_directory}_asciinema_${MAJOR}_${MINOR}_${PATCH}_demo.sh ${project_directory}_asciinema_${MAJOR}_${MINOR}_${PATCH}.json
                  asciinema play ${project_directory}_asciinema_${MAJOR}_${MINOR}_${PATCH}.json
                  deactivate

            #. upload

               .. code-block:: shell-session

                  . .venv/bin/activate
                  asciinema upload ${project_directory}_asciinema_${MAJOR}_${MINOR}_${PATCH}.json
                  deactivate

            #. edit the ``./README.md`` file with the new asciinema link
            #. commit

               .. code-block:: shell-session

                  git add -A
                  git commit
                  git push

   .. tab:: Irrelevant changes

            #. symlink to the previous relevant release

               .. code-block:: shell-session

                  cd ./asciinema
                  ln -s ${project_directory}_asciinema_${MAJOR}_${OLD_MINOR}_${OLD_PATCH}_demo.sh ${project_directory}_asciinema_${MAJOR}_${MINOR}_${PATCH}_demo.sh
                  ln -s ${project_directory}_asciinema_${MAJOR}_${OLD_MINOR}_${OLD_PATCH}.json ${project_directory}_asciinema_${MAJOR}_${MINOR}_${PATCH}.json

            #. commit

               .. code-block:: shell-session

                  git add -A
                  git commit
                  git push

Files
-----

Makefile
````````

.. code-block:: Makefile

   #
   # Makefile
   #

   export PACKAGE_NAME=${project_python_module_name}

   # See
   # https://docs.python.org/3/library/venv.html#how-venvs-work
   export VENV_CMD=. .venv/bin/activate

   default: install-dev

   doc:
   	$(VENV_CMD) \
   		&& $(MAKE) -C docs html \
   		&& deactivate

    install:
   	pip3 install . --user

   uninstall:
   	pip3 uninstall --verbose --yes $(PACKAGE_NAME)

   install-dev:
   	python3 -m venv .venv
   	$(VENV_CMD) \
   		&& pip install --requirement requirements-freeze.txt \
   		&& deactivate
   	$(VENV_CMD) \
   		&& pre-commit install \
   		&& deactivate
   	$(VENV_CMD) \
   		&& pre-commit install --hook-type commit-msg \
   		&& deactivate

   regenerate-freeze: uninstall-dev
   	python3 -m venv .venv
   	$(VENV_CMD) \
   		&& pip install --requirement requirements.txt --requirement requirements-dev.txt \
   		&& pip freeze --local > requirements-freeze.txt \
   		&& deactivate

   uninstall-dev:
   	rm -rf .venv

   update: install-dev
   	$(VENV_CMD) \
   		&& pre-commit autoupdate \
   			--repo https://github.com/pre-commit/pre-commit-hooks \
   			--repo https://github.com/PyCQA/bandit \
   			--repo https://github.com/pycqa/isort \
   			--repo https://codeberg.org/frnmst/licheck \
   			--repo https://codeberg.org/frnmst/md-toc \
   			--repo https://github.com/mgedmin/check-manifest \
   			--repo https://github.com/jorisroovers/gitlint \
   		&& deactivate
   		# --repo https://github.com/pre-commit/mirrors-mypy \

   test:
   	$(VENV_CMD) \
   		&& python -m unittest $(PACKAGE_NAME).tests.tests --failfast --locals --verbose \
   		&& deactivate

   pre-commit:
   	$(VENV_CMD) \
   		&& pre-commit run --all \
   		&& deactivate

   dist:
   	# Create a reproducible archive at least on the wheel.
   	# See
   	# https://bugs.python.org/issue31526
   	# https://bugs.python.org/issue38727
   	# https://github.com/pypa/setuptools/issues/1468
   	# https://github.com/pypa/setuptools/issues/2133
   	# https://reproducible-builds.org/docs/source-date-epoch/
   	$(VENV_CMD) \
   		&& SOURCE_DATE_EPOCH=$$(git -c log.showSignature='false' log -1 --pretty=%ct) \
   		python -m build \
   		&& deactivate
   	$(VENV_CMD) \
   		&& twine check --strict dist/* \
   		&& deactivate

   upload:
   	$(VENV_CMD) \
   		&& twine upload dist/* \
   		&& deactivate

   clean:
   	rm -rf build dist *.egg-info tests/benchmark-results
   	# Remove all markdown files except the readmes.
   	find -regex ".*\.[mM][dD]" ! -name 'README.md' ! -name 'CONTRIBUTING.md' -type f -exec rm -f {} +
   	$(VENV_CMD) \
   		&& $(MAKE) -C docs clean \
   		&& deactivate

   .PHONY: default doc install uninstall install-dev uninstall-dev update test pre-commit

.. rubric:: Footnotes

.. [#f1] https://wiki.archlinux.org/index.php/Arch_User_Repository GNU Free Documentation License 1.3 or late, copyright (c) ArchWiki contributors
.. [#f2] https://semver.org/#summary CC BY 3.0 semver contributors
.. [#f3] https://blog.franco.net.eu.org/post/an-alternative-to-github-pages.html CC-BY-SA 4.0, copyright (c) 2021-2022, Franco Masotti
