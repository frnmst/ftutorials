Contributing
============

Git branches policy
-------------------

What follows is a table of the git branches used in the repositories.

Pull requests must be opened on the ``dev`` branch.

=====================================   ====================================================   ==============================
Branch                                  Description                                            Update schedule
=====================================   ====================================================   ==============================
``master``                              the main branch                                        every new release
``dev``                                 recent changes are merged here before a new release    at will
=====================================   ====================================================   ==============================

Development environment
-----------------------

Basic
`````

#. install

   - `Python venv <https://packages.debian.org/bullseye/python3-venv>`_.
     See this `answer <https://stackoverflow.com/a/41573588>`_ as well

#. clone the repository
#. install the development environment

   .. code-block:: shell-session

      make install-dev

PDF output for Sphinx documentation
```````````````````````````````````

#. If you are using Debian install these packages

   .. code-block:: shell-session

      apt install texlive-latex-extra texlive-fonts-extra latexmk xindy


Multiple Python environments
````````````````````````````

#. install `asdf <https://asdf-vm.com/>`_ and setup the shell environment
#. install sqlite

   .. code-block:: shell-session

      apt install libsqlite3-dev

#. setup multiple Python environments by installing them, for example

   .. code-block:: shell-session

      asdf install python 3.12.0 \
          && asdf install python 3.11.6

   Have a look at the ``[tox:tox]`` section in the ``/setup.cfg`` of the project.

   Add these Python versions to the ``~/.tools-versions`` file.

Contribution steps
------------------

#. write or change code
#. write unit tests if relevant parts of the code have been added or changed
#. unit tests are run like this

   .. code-block:: shell-session

      make test

#. run this to install the Python program in your user directory

   .. code-block:: shell-session

      make install

   .. important:: add ``~/.local/bin`` to the ``PATH`` enviroment variable
      to have a working executable

#. if applicable update relevant documentation
#. to rebuild the documentation, both HTML and PDF, run

   .. code-block:: shell-session

      make doc

#. create a new pull request

.. note:: Use ``.venv/bin/python3 -m ${module}`` to run the Python program in
          the development environment.
