QEMU
====

.. _qemu client:

Client
------

Use declarative configuration to connect to a server running QEMU

#. install these packages

   .. code-block:: shell-session

      apt-get install tigervnc-viewer openssh-client

#. create the jobs directories. See :ref:`reference <jobs instructions>`

   .. code-block:: shell-session

      mkdir -p /home/jobs/scripts/by-user/myuser

#. follow the instructions from the :ref:`QEMU server <qemu server>` section
#. create the :ref:`qvm <qvm>` script in ``/home/jobs/scripts/by-user/myuser/qvm.py``
#. create the :download:`configuration file <includes/home/jobs/scripts/by-user/myuser/qvm.yaml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/myuser/qvm.yaml
      :language: yaml
      :linenos:
      :caption: /home/jobs/scripts/by-user/myuser/qvm.yaml

#. fix the permissions

   .. code-block:: shell-session

      chown -R myuser:myuser /home/jobs/scripts/by-user/myuser
      chmod 700 -R /home/jobs/scripts/by-user/myuser

#. go back to the desktop user

   .. code-block:: shell-session

      exit

#. connect to the server via SSH or VNC (see ``remote.test.system.display.enabled`` option)

   .. code-block:: shell-session

      pushd /home/jobs/scripts/by-user/myuser
      ./qvm.py ./qvm.yaml remote test

   .. note:: You can also create a shell alias such as this (works for GNU Bash)

             .. code-block:: shell-session

                alias test='/home/jobs/scripts/by-user/myuser/qvm.py /home/jobs/scripts/by-user/myuser/qvm.yaml remote test'
