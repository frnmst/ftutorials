Sudo
====

Migration to doas
-----------------

As an lightweight alternative to sudo you can use doas.

.. seealso::

   - _`doas - ArchWiki` [#f1]_
   - _`doas - ArchWiki - doas persist feature` [#f2]_

.. warning:: doas does not seem to be production ready. See [#f2]_

#. install the package

   .. code-block:: bash

      apt-get install doas

#. configure doas. Replace ``USER`` with your user

   .. literalinclude:: includes/etc/doas.conf
      :linenos:
      :caption: /etc/doas.conf

#. test it

   .. code-block:: bash

      doas -s

#. use this bash alias and function to convert ``sudo`` and ``sudo -i``
   to the ``doas`` equivalents

   .. code-block:: bash
      :linenos:

      function doas_to_sudo_interactive() {
          # rename alias
          [ "${1}" = '-i' ] && doas -s || doas ${1}
      }
      alias sudo='doas_to_sudo_interactive'

#. If you want to remove sudo you have to set an environment variable first

   .. code-block:: bash

      SUDO_FORCE_REMOVE=yes
      apt purge sudo

.. rubric:: Footnotes

.. [#f1] https://wiki.archlinux.org/title/Doas
.. [#f2] https://wiki.archlinux.org/title/Doas#doas_persist_feature
