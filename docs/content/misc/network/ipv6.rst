IPv6
====

Filtering
---------

GNU/Linux
`````````

To filter all IPv6 packets except ICMPv6 messages, run these
commands.

#. filter all IPv6 packets

   .. code-block:: shell-session

      ip6tables --policy INPUT DROP
      ip6tables --policy FORWARD DROP
      ip6tables --policy OUTPUT ACCEPT
      ip6tables --append INPUT --in-interface lo --jump ACCEPT
      ip6tables --append OUTPUT --out-interface lo --jump ACCEPT

#. accept ICMPv6 messages

   .. code-block:: shell-session

      ip6tables --append INPUT --protocol ipv6-icmp --jump ACCEPT

#. save the rules

   .. code-block:: shell-session

      dpkg-reconfigure iptables-persistent

Disabling
---------

See `this youtube video <https://yewtu.be/watch?v=UpmMzQLmR18>`_

GNU/Linux
`````````

.. seealso::

   - _`networking - How to disable IPv6 permanently? - Ask Ubuntu` [#f1]_
   - _`IPv6 - ArchWiki` [#f2]_

#. append these lines to the :download:`Sysctl configuration file <includes/etc/sysctl.conf>`

   .. literalinclude:: includes/etc/sysctl.conf
      :language: ini
      :caption: /etc/sysctl.conf

#. reload the configuration

   .. code-block:: shell-session

      sysctl -p /etc/sysctl.conf

#. comment IPv6 hosts in ``/etc/hosts``
#. reboot and check that everything still works

.. warning:: Disabling IPv6 on a server is not without dangers! See reference 2.
             Remember to disable IPv6 from server configurations such as
             :download:`OpenSSH <includes/etc/ssh/sshd_config>`
             and
             :download:`Unbound <includes/etc/unbound/unbound.conf>`,
             for example.

             .. tabs::

               .. tab:: OpenSSH

                  .. literalinclude:: includes/etc/ssh/sshd_config
                     :language: ini
                     :caption: /etc/ssh/sshd_config

               .. tab:: Unbound

                  .. literalinclude:: includes/etc/unbound/unbound.conf
                     :language: ini
                     :caption: /etc/unbound/unbound.conf

OpenWRT
```````

You may want to disable IPv6 on OpenWrt because of DNS
issues with recent Android OSes. Android uses the IPv6 DNSes advertised
by an OpenWRT router even if you set a static IPv4 DNS.

.. seealso::

   - _`Be aware of Android's shady IPv6 DNS - General - Pi-hole Userspace` [#f3]_
   - _`[Solved] How can I completely disable ipv6 from LuCI? - Installing and Using OpenWrt / Network and Wireless Configuration - OpenWrt Forum` [#f4]_

#. login the LuCI web UI
#. append this content to ``System`` -> ``Startup`` -> ``Local Startup`` before the
   ``exit 0`` command

   .. code-block:: shell-session

      sysctl -w net.ipv6.conf.all.disable_ipv6=1
      sysctl -w net.ipv6.conf.default.disable_ipv6=1

   .. note:: The ``net.ipv6.conf.lo.disable_ipv6=1`` option does not seem to have any effect on the loopback interface.

#. reboot

.. rubric:: Footnotes

.. [#f1] https://askubuntu.com/a/309463 CC BY-SA 4.0, Copyright (c) 2013, 2018 Eric Carvalho, abu_bua (at askubuntu.com)
.. [#f2] https://wiki.archlinux.org/title/IPv6#Disable_IPv6 GNU Free Documentation License 1.3 or later, Copyright (c) ArchWiki contributors
.. [#f3] https://discourse.pi-hole.net/t/be-aware-of-androids-shady-ipv6-dns/36636 unknown license
.. [#f4] https://forum.openwrt.org/t/solved-how-can-i-completely-disable-ipv6-from-l unknown license
