Debug
=====

Check for duplicate packets
---------------------------

If you have strange network behaviour it might be caused by duplicate packets.

In my case the most noticeable effect of duplicate packets is that
interactive SSH sessions stutter: for example, when I keep the space
button pressed, every now and then the terminal freezes for a moment.

You can check for duplicate packet with Wireshark using this filter

::

    tcp.analysis.duplicate_ack or tcp.analysis.retransmission


If you get lots of duplicate packets try restarting all your network devices
(router, switches, clients, etc...).

Rebooting, for the moment, seems the only viable fix.

.. seealso::

   - _`DuplicatePackets - Wireshark` [#f1]_

Drop AVM Fritzbox HomePlug packets
----------------------------------

If you run Wireshark in a network with certain AVM Fritzbox devices
you will see lots of broadcast packets every seconds, such as this

::

    15 1.780941119 AVMAudio_00:00:00 AtherosC_00:00:00 HomePlug AV 60 Qualcomm Atheros, OP_ATTR.REQ (Get Device Attributes Request)

GNU/Linux
`````````

If you run this command on an interface you will see all the dropped packets

.. code-block:: shell-session

   cat /sys/class/net/${interface}/statistics/rx_dropped

This is all unnecessary traffic which apparently you cannot disable from the router. What you can do is let
the computers ignore all this traffic.

.. seealso::

   - _`firewall - How can I use iptables to drop packages for an invalid ether-type? - Server Fault` [#f2]_
   - _`Dropped HomePlug AV packets` [#f3]_

#. install the netfilter package

   .. code-block:: shell-session

      apt-get install nftables netfilter-persistent

#. add this rule with a terminal or scripts

   .. code-block:: shell-session

      nft table netdev filter '{ chain ingress { type filter hook ingress device eno1 priority 0; policy accept;  meta protocol {0x8912, 0x88e1} drop; }; }'

OpenWRT
```````

.. seealso::

   - _`OpenWRT/Lede AP: Drop FritzBox Homeplug Broadcasts · GitHub` [#f4]_

#. install the ebtables package. Go to ``System`` -> ``Software``
#. click on ``Update lists...``
#. filter ``ebtables``
#. click on ``Install...``
#. go to ``Network`` -> ``Firewall`` -> ``Custom Rules``
#. append these commands

   .. code-block:: shell-session

      ebtables -A FORWARD -p 88e1 -j DROP
      ebtables -A FORWARD -p 8912 -j DROP

#. go to ``System`` -> ``Reboot``
#. click on ``Perform reboot``

.. note:: For some reason I can still see the packets when a computer is connected to an OpenWRT device
          using these rules which in turn is connected to the Fritzbox!

.. rubric:: Footnotes

.. [#f1] https://wiki.wireshark.org/DuplicatePackets GNU General Public License, Copyright (c) Wireshark Wiki contributors
.. [#f2] https://serverfault.com/questions/1019460/how-can-i-use-iptables-to-drop-packages-for-an-invalid-ether-type CC-BY-SA 4.0, Copyright (c) 2021 Ingo (at serverfault.com)
.. [#f3] https://pablo.tools/blog/computers/dropped-packets/ unknown license
.. [#f4] https://gist.github.com/tillz/41f50e744b3230f53fe420119034af1c unknown license
