Performance
===========

MTU
---

Increasing the MTU value can, in some cases, improve network efficiency.

.. warning:: Changing the MTU value could have unintended consequences!

GNU/Linux
`````````

You can change the default Ethernet (IEEE 802.3) MTU value from 1500 to 9000 if all devices
you want to use support it (including dumb Ethernet switches).

If you use the ``networking`` systemd service provided by Debian
there are various possibilities. Once you done the configuration you can
check it with ICMP requests like these

.. seealso::

   - _`How to change MTU size in Linux` [#f1]_
   - _`linux - how to verify if MTU 9000 configured properly on all component - Unix & Linux Stack Exchange` [#f2]_
   - _`sysctl - ArchWiki` [#f3]_

.. code-block:: ini

    ping ${other_host_using_mtu_9000} -s 8972 -Mdo

.. tabs::

   .. tab:: Static address

            1. append the ``mtu`` line to the :download:`interface configuration file <includes/etc/network/interfaces>`

               .. literalinclude:: includes/etc/network/interfaces
                  :language: ini
                  :linenos:
                  :caption: /etc/network/interfaces

   .. tab:: DHCP

            1. append this :download:`configuration <includes/etc/dhcp/dhclient.conf>`

               .. literalinclude:: includes/etc/dhcp/dhclient.conf
                  :language: ini
                  :linenos:
                  :caption: /etc/dhcp/dhclient.conf

2. reboot
3. optionally enable MTU probing. See reference 3

   .. code-block:: shell-session

      echo "net.ipv4.tcp_mtu_probing = 1" >> /etc/sysctl.conf
      sysctl -p /etc/sysctl.conf
      exit

Docker
``````

.. seealso::

   - _`Fixing networking for Docker - Civo.com` [#f4]_
   - _`Docker mtu problem - Support - Drone` [#f5]_
   - _`How we spent a full day figuring out a MTU issue with docker | by Sylvain Witmeyer | Medium` [#f6]_

#. add this setting to the :download:`daemon configuration file <includes/etc/docker/daemon.json>`

   .. literalinclude:: includes/etc/docker/daemon.json
      :language: json
      :linenos:
      :caption: /etc/docker/daemon.json

#. restart the service

   .. code-block:: shell-session

      systemctl restart docker

#. add a new network with the new MTU value in the Docker compose file

   .. code-block:: yaml

      services:
          myservice_0:
              # [ ... ]
              networks:
                  - mynetwork

          myservice_1:
              # [ ... ]
              networks:
                  - mynetwork

          # [ ... ]

           myservice_n:
              # [ ... ]
              networks:
                  - mynetwork

      # [ ... ]

      networks:
          mynetwork:
            driver: bridge
            driver_opts:
              com.docker.network.driver.mtu: 9000

.. rubric:: Footnotes

.. [#f1] https://linuxhint.com/how-to-change-mtu-size-in-linux/ unknown license
.. [#f2] https://unix.stackexchange.com/a/466981 CC BY-SA 4.0, Copyright (c) 2018 Mr Shunz (at unix.stackexchange.com)
.. [#f3] https://wiki.archlinux.org/title/sysctl#Enable_MTU_probing GNU Free Documentation License 1.3 or later, Copyright (c) ArchWiki contributors
.. [#f4] https://www.civo.com/learn/fixing-networking-for-docker unknown license
.. [#f5] https://discourse.drone.io/t/docker-mtu-problem/1207 unknown license
.. [#f6] https://sylwit.medium.com/how-we-spent-a-full-day-figuring-out-a-mtu-issue-with-docker-4d81fdfe2caf unknown license
