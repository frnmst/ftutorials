Recording
=========

Motion
------

script to record streams captured by video devices
with `Motion <https://motion-project.github.io/>`_.

We will assume that Motion is already configured and running.

You can use hardware acceleration instead of using software for the encoding
process to reduce the load on the processors, although the quality is lower than
software encoding. Since we are dealing with video surveillance footage we don't
care about quality so much.

.. seealso::

    - _`Hardware video acceleration - ArchWiki` [#f1]_
    - _`HWAccelIntro – FFmpeg` [#f2]_
    - _`Hardware/VAAPI – FFmpeg` [#f3]_

Setup
-----

#. install the dependencies

   .. code-block:: shell-session

      apt-get install bash ffmpeg python3-yaml python3-requests

#. install fpyutils. See :ref:`reference <installation of fpyutils>`
#. create a new user

   .. code-block:: shell-session

      useradd --system -s /bin/bash -U surveillance
      passwd surveillance
      usermod -aG jobs surveillance

#. create the jobs directories. See :ref:`reference <jobs instructions>`

   .. code-block:: shell-session

      mkdir -p /home/jobs/{scripts,services}/by-user/surveillance

#. create the :download:`script <includes/home/jobs/scripts/by-user/surveillance/record_motion.py>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/surveillance/record_motion.py
      :language: python
      :linenos:
      :caption: /home/jobs/scripts/by-user/surveillance/record_motion.py

#. create a :download:`configuration file <includes/home/jobs/scripts/by-user/surveillance/record_motion.camera1.yaml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/surveillance/record_motion.camera1.yaml
      :language: yaml
      :linenos:
      :caption: /home/jobs/scripts/by-user/surveillance/record_motion.camera1.yaml

   .. note::

      If you have an intel processor that supports VAAPI (Video Acceleration API)
      you can use the following settings

      .. code-block:: yaml

         video:
             # [ ... ]
             codec: 'mjpeg_vaapi'
         extra_options:
             pre: '-hwaccel vaapi -hwaccel_device /dev/dri/renderD128 -hwaccel_output_format vaapi'
             # [ ... ]
         quality:
             # If empty use quality otherwise use global_quality.
             global_quality: '60'
             # [ ... ]
         # [ ... ]

#. fix the permissions

   .. code-block:: shell-session

      chown -R surveillance:surveillance /home/jobs/{scripts,services}/by-user/surveillance
      chmod 700 -R /home/jobs/{scripts,services}/by-user/surveillance

#. run the :ref:`deploy script <deploy script>`

.. rubric:: Footnotes

.. [#f1] https://wiki.archlinux.org/index.php/Hardware_video_acceleration GNU Free Documentation License 1.3 or late, copyright (c) ArchWiki contributors
.. [#f2] https://trac.ffmpeg.org/wiki/HWAccelIntro CC-BY-SA 3.0 License, copyright (c) Contributors to the FFmpeg Community Documentation Wiki
.. [#f3] https://trac.ffmpeg.org/wiki/Hardware/VAAPI CC-BY-SA 3.0 License, copyright (c) Contributors to the FFmpeg Community Documentation Wiki
