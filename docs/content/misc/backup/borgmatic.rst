Borgmatic
=========

Let's say we want to backup the root (``/``) partition. By using Borgmatic and these
scripts you get:

- compressed and differential backups
- optional notifications before, after and during the backup
- possibility browse through all versions of a backup and to have optional notifications about the mount action

.. seealso::

   - A collection of scripts I have written and/or adapted that I currently use on my systems as automated tasks [#f1]_
   - _`borgmatic` [#f2]_
   - _`How to monitor your backups - borgmatic` [#f3]_
   - _`How to deal with very large backups - borgmatic` [#f4]_
   - _`Low power consumption home backup server project - Where is it?` [#f5]_
   - _`Borgmatic Systemd unit service file` [#f6]_
   - _`Borgmatic Systemd unit timer file` [#f7]_
   - _`GitHub - caronc/apprise: Apprise - Push Notifications that work with just about every platform!` [#f11]_

Setup
-----

#. install the dependencies

   .. code-block:: shell-session

      apt-get install borgmatic python3-yaml python3-venv

#. create the jobs directories. See :ref:`reference <jobs instructions>`

   .. code-block:: shell-session

      mkdir -p /home/jobs/{scripts,services}/by-user/root
      chmod 700 -R /home/jobs/{scripts,services}/by-user/root

#. create a new virtual environment

   .. code-block:: shell-session

      cd /home/jobs/scripts/by-user/root
      python3 -m venv .venv_borgmatic_hooks
      . .venv_borgmatic_hooks/bin/activate

#. install fpyutils. See :ref:`reference <installation of fpyutils>`
#. create the :download:`requirements_notify_unit_status.txt <includes/home/jobs/scripts/by-user/root/requirements_borgmatic_hooks.txt>` file

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/requirements_borgmatic_hooks.txt
      :caption: /home/jobs/scripts/by-user/root/requirements_borgmatic_hooks.txt

#. install the dependencies

   .. code-block:: shell-session

      pip3 install -r requirements_borgmatic_hooks.txt
      deactivate

#. create the :download:`hooks script <includes/home/jobs/scripts/by-user/root/borgmatic_hooks.py>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/borgmatic_hooks.py
      :language: python
      :linenos:
      :caption: /home/jobs/scripts/by-user/root/borgmatic_hooks.py

#. create the :download:`mount script <includes/home/jobs/scripts/by-user/root/borgmatic_mount.py>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/borgmatic_mount.py
      :language: python
      :linenos:
      :caption: /home/jobs/scripts/by-user/root/borgmatic_mount.py

#. create the :download:`borgmatic service <includes/home/jobs/services/by-user/root/borgmatic.myhostname_root.service>`

   .. literalinclude:: includes/home/jobs/services/by-user/root/borgmatic.myhostname_root.service
      :language: ini
      :linenos:
      :caption: /home/jobs/services/by-user/root/borgmatic.myhostname_root.service

#. create the :download:`borgmatic timer <includes/home/jobs/services/by-user/root/borgmatic.myhostname_root.timer>`

   .. literalinclude:: includes/home/jobs/services/by-user/root/borgmatic.myhostname_root.timer
      :language: ini
      :linenos:
      :caption: /home/jobs/services/by-user/root/borgmatic.myhostname_root.timer

#. create the :download:`borgmatic mount service <includes/home/jobs/services/by-user/root/borgmatic-mount.myhostname_root.service>`

   .. literalinclude:: includes/home/jobs/services/by-user/root/borgmatic-mount.myhostname_root.service
      :language: ini
      :linenos:
      :caption: /home/jobs/services/by-user/root/borgmatic-mount.myhostname_root.service

#. create a :download:`borgmatic hooks configuration file <includes/home/jobs/scripts/by-user/root/borgmatic_hooks.myhostname_root.yaml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/borgmatic_hooks.myhostname_root.yaml
      :language: yaml
      :linenos:
      :caption: /home/jobs/scripts/by-user/root/borgmatic_hooks.myhostname_root.yaml

#. create a :download:`borgmatic mount configuration file <includes/home/jobs/scripts/by-user/root/borgmatic_mount.myhostname_root.yaml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/borgmatic_mount.myhostname_root.yaml
      :language: yaml
      :linenos:
      :caption: /home/jobs/scripts/by-user/root/borgmatic_mount.myhostname_root.yaml

New repository
--------------

Before enabling the backup you need to decide exactly where to backup the directory,
what to exclude, etc...

In this example we are not using Borg's encryption because:

- it works with older versions of borg
- data is simpler to recover in case things go wrong
- these are not to be considered offsite backups

By using Borgmatic you can either decide to backup on a local directory or on
a remote machine via SSH.

.. seealso::

   - _`borg init — Borg - Deduplicating Archiver 1.2.0 documentation` [#f8]_

Types of backup
```````````````

.. tabs::

   .. tab:: Local

            +--------------+--------------------+
            | Run as user  | Instruction number |
            +--------------+--------------------+
            | ``root``     | \*                 |
            +--------------+--------------------+

            If you are backing up on a local directory you simply have to create
            a new repository according to the configuration.

            #. create a new :download:`configuration file <includes/home/jobs/scripts/by-user/root/borgmatic.myhostname_root.yaml>` for the local backup

               .. literalinclude:: includes/home/jobs/scripts/by-user/root/borgmatic.myhostname_root.yaml
                  :language: yaml
                  :lines: 1-19,21-52
                  :linenos:
                  :caption: /home/jobs/scripts/by-user/root/borgmatic.myhostname_root.yaml

            #. create the new repository

               .. code-block:: shell-session

                  borg init -e none /mnt/backups/myhostname_root.borg

   .. tab:: Remote

            +---------------------------+--------------------+
            | Run as user               | Instruction number |
            +---------------------------+--------------------+
            | ``root``                  | 2,4-5              |
            +---------------------------+--------------------+
            | ``root`` (remote)         | 1                  |
            +---------------------------+--------------------+
            | ``borgmatic`` (remote)    | 3                  |
            +---------------------------+--------------------+

            #. connect to the remote machine via SSH and create a new user called ``borgmatic``

               .. code-block:: shell-session

                  useradd -m -s /bin/bash -U borgmatic
                  passwd borgmatic

            #. quit the remote machine and create SSH key pair: do not add a passphrase
               when prompted

               .. code-block:: shell-session

                  ssh-keygen -t rsa -b 16384 -C "root@myhostname-$(date "+%F")"

            #. add the public key to the authorized keys file on remote machine

               .. code-block:: shell-session

                  cat mykey.pub >> ~/.ssh/authorized_keys

            #. create the new repository

               .. code-block:: shell-session

                  borg init -e none user@remote-host:/mnt/backups/myhostname_root.borg

            #. create a new :download:`configuration file <includes/home/jobs/scripts/by-user/root/borgmatic.myhostname_root.yaml>` for the backup

               .. literalinclude:: includes/home/jobs/scripts/by-user/root/borgmatic.myhostname_root.yaml
                  :language: yaml
                  :lines: -19,55-
                  :linenos:
                  :caption: /home/jobs/scripts/by-user/root/borgmatic.myhostname_root.yaml

Deploy
------

#. run the :ref:`deploy script <deploy script>`
#. check if the backup service works

.. note:: If you get an *unknown repo* error try adding this key:
          ``storage.unknown_unencrypted_repo_access_is_ok: true``

Mount
-----

To mount the backup you just have to run the Systemd service

.. code-block:: shell-session

   systemctl start borgmatic-mount.myhostname_root.service

To unmount, stop the service

.. code-block:: shell-session

   systemctl stop borgmatic-mount.myhostname_root.service

Other
-----

Databases
`````````

Borgmatic also support backing up databases. Just put ``[]`` as ``location.source_directories``
and add a database object in the ``hooks`` section.

.. seealso::

   - _`How to backup your databases - borgmatic` [#f9]_

Running the service before computer shutdown
````````````````````````````````````````````

.. seealso::

   - _`linux - How do I run a script before everything else on shutdown with systemd? - Super User` [#f10]_

#. change some of the original borgmatic service Systemd unit file lines with these:

   .. code-block:: ini

      ExecStart=/bin/true
      RemainAfterExit=yes
      TimeoutStopSec=infinity
      ExecStop=/usr/bin/borgmatic --config /home/jobs/scripts/by-user/root/borgmatic.myhostname_root.yaml --syslog-verbosity 1

Although this service remains active all the time, the syncronization action
runs when the system is halted using an ``ExecStop`` directive. Since we don't
know how much time the syncronization takes, a ``TimeoutStopSec=infinity``
directive is present.

Automatic backup on a removable USB drive on plug in
````````````````````````````````````````````````````

#. create a partition on a block device
#. create a filesystem. In this example the filsystem's UUID is ``ABCD-0123``
   and is obtained with this command

   .. code-block:: shell-session

      udevadm info --name=${partition} | grep "ID_FS_UUID="

#. add its entry in the :download:`fstab file <includes/etc/fstab>`, for example

   .. literalinclude:: includes/etc/fstab
      :language: ini
      :linenos:
      :caption: /etc/fstab

#. create a new :download:`udev rule <includes/etc/udev/rules.d/99-usb-automount.rule>` to enable the auto mounting

   .. literalinclude:: includes/etc/udev/rules.d/99-usb-automount.rule
      :language: ini
      :linenos:
      :caption: /etc/udev/rules.d/99-usb-automount.rule

#. reboot to apply the rules
#. plug in the USB device and check if the service starts

Mounting backups on other computers
```````````````````````````````````

Let's say computer A backups on computer B, but A is offline.

You can access A's backups via B on a third computer, C.
C just needs to have SSH access to B and a copy of A's original borgmatic files.

.. rubric:: Footnotes

.. [#f1] https://software.franco.net.eu.org/frnmst/automated-tasks GNU GPLv3+, copyright (c) 2019-2022, Franco Masotti
.. [#f2] https://torsion.org/borgmatic/ GNU GPLv3+, copyright (c) 2014-2022, Dan Helfman and borgmatic contributors
.. [#f3] https://torsion.org/borgmatic/docs/how-to/monitor-your-backups/ GNU GPLv3+, copyright (c) 2014-2022, Dan Helfman and borgmatic contributors
.. [#f4] https://torsion.org/borgmatic/docs/how-to/deal-with-very-large-backups/ GNU GPLv3+, copyright (c) 2014-2022, Dan Helfman and borgmatic contributors
.. [#f5] https://medspx.fr/projects/backup/ CC-BY-SA 4.0, copyright (c) 2022, Médéric Ribreux
.. [#f6] https://projects.torsion.org/witten/borgmatic/raw/branch/master/sample/systemd/borgmatic.service GNU GPLv3+, copyright (c) 2014-2022, Dan Helfman and borgmatic contributors
.. [#f7] https://projects.torsion.org/witten/borgmatic/raw/branch/master/sample/systemd/borgmatic.timer GNU GPLv3+, copyright (c) 2014-2022, Dan Helfman and borgmatic contributors
.. [#f8] https://borgbackup.readthedocs.io/en/stable/usage/init.html?highlight=encryption BSD-3-clause license, Copyright: 2015-2020 The Borg Collective
.. [#f9] https://torsion.org/borgmatic/docs/how-to/backup-your-databases/ GNU GPLv3+, copyright (c) 2014-2022, Dan Helfman and borgmatic contributors
.. [#f10] https://superuser.com/a/1016848 CC BY-SA 3.0, le_me (at superuser.com)
.. [#f11] https://github.com/caronc/apprise BSD 2-Clause License, Copyright (c) 2024, Chris Caron, Apprise contributors
