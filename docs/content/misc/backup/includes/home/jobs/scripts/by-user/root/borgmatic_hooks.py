#!/usr/bin/env python3
#
# borgmatic_hooks.py
#
# Copyright (C) 2021-2024 Franco Masotti
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
r"""Send a notification after the backup operation."""

import shlex
import sys

import apprise
import yaml

if __name__ == '__main__':
    configuration_file = shlex.quote(sys.argv[1])
    config = yaml.load(open(configuration_file), Loader=yaml.SafeLoader)
    action = sys.argv[2]
    borgmatic_config_file = shlex.quote(sys.argv[3])
    repository = sys.argv[4]
    output = sys.argv[5]
    error = sys.argv[6]

    message = action + ' on ' + repository + ' using ' + borgmatic_config_file + ': ' + '\n' + 'output: ' + output + '\n' + 'error: ' + error

    # Create an Apprise instance.
    apobj = apprise.Apprise()

    # Add all of the notification services by their server url.
    for uri in config['apprise_notifiers']['dest']:
        apobj.add(uri)

    apobj.notify(
        body=message,
        title=config['apprise_notifiers']['title'],
    )
