Git
===

Gitignore
---------

.. seealso::

   - _`git - Gitignore not working - Stack Overflow` [#f1]_

Troubleshooting
---------------

GPG signing
```````````

You may get this error while signing a GIT commit

.. code-block::

   error: gpg failed to sign the data
   fatal: failed to write commit object

Change the pin entry mode

.. code-block:: shell-session

   echo 'pinentry-mode loopback' >> ~/.gnupg/gpg.conf

.. rubric:: Footnotes

.. [#f1] https://stackoverflow.com/questions/25436312/gitignore-not-working CC BY-SA 4.0, copyright (c) 2014, stackoverflow.com contributors
