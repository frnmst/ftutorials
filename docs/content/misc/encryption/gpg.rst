GPG
===

As many tutorials point out it is better to keep the master key stored in an offline
computer and to export subkeys when needed.

In this tutorial we use

- ``a@b.com`` as the email that identifies the key (key id)
- ``EF987654`` as the fingerprint of the public key
- ``0123ABCD`` as the fingerprint of the public signing subkey

.. _GPG Generate a key pair:

Generate a key pair
-------------------

We will generate a gpg keypair on an air gapped computer for added security.
We will also use one or more USB sticks as backups. The computer hard drive
and USB sticks must be encrypted for added security. You can then use a
third USB stick to transfer public key files. This last one does not need
to be encrypted.

.. seealso::

   - _`GnuPG - ArchWiki` [#f4]_

#. get an old laptop you do not use anymore
#. install Debian stable on it using LUKS encryption
#. after the installation is finished disconnect the computer from the network
#. generate a key pair

   .. code-block:: shell-session

      gpg --full-gen-key

   You may use 4096 as key size and a functional email as identity

.. _GPG Export:

Export
------

Exporting a keypair means that you can backup you private keys and publish
your public keys.
If you want your public key to be known easily you can publish it on a
keyserver. Use an unencrypted USB stick to transfer public key files on
other computers

.. seealso::

   - _`disaster recovery - How to backup GPG? - Server Fault` [#f5]_

#. export everyting on the home directory

   .. code-block:: shell-session

      cd ~
      gpg --export --armor a@b.com > a@b.com.pub.asc
      gpg --export-secret-keys --armor a@b.com > a@b.com.priv.asc
      gpg --export-secret-subkeys --armor a@b.com > a@b.com.sub_priv.asc
      gpg --export-ownertrust > ownertrust.txt

#. if you need to export a secret subkey only, for example for GIT signing,
   you can use this command

   .. code-block:: shell-session

      gpg --export-secret-subkeys --armor 0x0123ABCD! > 0x0123ABCD.secret_subkey.asc

   .. important:: Preserve the ``!`` at the end of the command.

   To export the public subkey use this command

   .. code-block:: shell-session

      gpg --export --armor --output 0123ABCD.subkey.txt 0123ABCD!

   .. important:: Preserve the ``!`` at the end of the command.

#. copy all the files to the backup USB sticks
#. export the needed public key to the unencrypted USB stick

Import
------

#. on your other devices you can simply import any type of key like this

   .. code-block:: shell-session

      gpg --import ${some_key}

Renew expiration date
---------------------

.. seealso::

   - _`Extending expiration date - GnuPG - ArchWiki` [#f1]_
   - _`gnupg - Is it possible to export a GPG subkey's public component? - Information Security Stack Exchange` [#f2]_

#. get the keys. You can use the email as identifier (we are interested in the ``uid`` which in this case is ``a@b.com``)

   .. code-block:: shell-session

      gpg --list-keys a@b.com

#. edit the keys. You can, for example, extend the master subkey for another 2
   years while the signing (``S``), certification (``C``) and encryption (``E``) subkeys
   can be extended by 1 year. You will get an interactive interface after running the
   first command

   .. code-block:: shell-session

      gpg --edit-key a@b.com

      key⏎
      expire⏎
      730⏎
      y⏎

      key 1⏎
      expire⏎
      365⏎
      y⏎
      key 1⏎

      key 2⏎
      expire⏎
      365⏎
      y⏎
      key 2⏎

      key 3⏎
      expire⏎
      365⏎
      y⏎
      key 3⏎

      save⏎

      exit⏎

#. :ref:`re-export <GPG Export>` and share you public key(s)

.. note:: There is no need to update the backups or re-export the secret key.

Add new email id
----------------

#. edit the key: create a new email and set ultimate trust

   .. code-block:: shell-session

      gpg --edit-key a@b.com

      adduid⏎

                # follow the instructions

      2⏎        # the id of the new email (usually 2 if you had only one email before)

      primary⏎  # set the new email as primary

      trust⏎
      5⏎        # set ultimate trust for the new email

      save⏎
      exit⏎

#. :ref:`re-export <GPG Export>` and share you public key(s)

.. note:: There is no need to update the backups or re-export the secret key.

Keyoxide setup
--------------

Keyoxide is a website independent way of claiming ownership of web profiles
such as DNS records, Gitea, Gitlab, Gitlab and other profiles.

If you have a look at the Keyoxide homepage you will find a list of supported
services. In this example we will verify a DNS domain with Keyoxide

.. seealso::

   - _`Keyoxide? — Keyoxide Docs` [#f6]_
   - _`OpenPGP with GnuPG — Keyoxide Docs` [#f7]_
   - _`Using GnuPG — Keyoxide Docs` [#f8]_
   - _`DNS — Keyoxide Docs` [#f9]_
   - _`gitea_proof - Codeberg.org` [#f10]_

#. :ref:`generate a key pair <GPG Generate a key pair>`
#. edit you keypair. In this example we will add a DNS claim

   .. code-block:: shell-session

      gpg --edit-key a@b.com

      uid 1⏎
      notation⏎
      proof@ariadne.id=dns:yourdomain.org?type=TXT⏎
      save⏎

#. :ref:`export <GPG Export>` the public key: export the whole key, not subkeys
#. you can check the notations like this

   .. code-block:: shell-session

      gpg --edit-key a@b.com

      showpref⏎
      exit⏎

#. get the public key fingerprint, without spaces

   .. code-block:: shell-session

      gpg --with-colons --keyid-format long --list-keys a@b.com

   The key fingerprint corresponds to one of the ``fpr`` lines

#. Add the proof as a TXT record

   .. code-block::

      openpgp4fpr:${fingerprint}

#. upload the public key to `keys.openpgp.org <https://keys.openpgp.org>`_

.. important:: If you need to add a claim after , just add it, export the whole
               public key and upload it to keys.openpgp.org. There is no need
               to do any backups in this case.

Troubleshooting
---------------

Problem with the agent
``````````````````````

If you get ``gpg: problem with the agent: Permission denied`` while signing
append ``pinentry-mode loopback`` to ``~/.gnupg/gpg.conf``

.. seealso::

   - _`18.04 - gpg: problem with the agent: Permission denied - Ask Ubuntu` [#f3]_

.. rubric:: Footnotes

.. [#f1] https://wiki.archlinux.org/title/GnuPG#Extending_expiration_date GNU Free Documentation License 1.3 or later, Copyright (c) ArchWiki contributors
.. [#f2] https://security.stackexchange.com/a/226986 CC BY-SA 4.0, Copyright (c) 2020 qrtLs (at security.stackexchange.com)
.. [#f3] https://askubuntu.com/a/1158297 CC BY-SA 4.0, Copyright (c) 2021 Deathgrip (at askubuntu.com)
.. [#f4] https://wiki.archlinux.org/title/GnuPG
.. [#f5] https://serverfault.com/a/1040984 CC BY-SA 4.0, Copyright (c) 2020-2021, serverfault.com contributors
.. [#f6] https://docs.keyoxide.org/understanding-keyoxide/keyoxide/ CC BY-NC-SA 4.0, Copyright (c) Keyoxide contributors
.. [#f7] https://docs.keyoxide.org/using-cryptography/openpgp-gnupg/ CC BY-NC-SA 4.0, Copyright (c) Keyoxide contributors
.. [#f8] https://docs.keyoxide.org/openpgp-profiles/using-gnupg/ CC BY-NC-SA 4.0, Copyright (c) Keyoxide contributors
.. [#f9] https://docs.keyoxide.org/service-providers/dns/ CC BY-NC-SA 4.0, Copyright (c) Keyoxide contributors
.. [#f10] https://codeberg.org/forgejo/gitea_proof unknown license
