BTRFS
=====

.. seealso::

   - _`Using Btrfs with Multiple Devices - btrfs Wiki` [#f1]_
   - _`Btrfs - ArchWiki` [#f2]_
   - _`Btrfs/Checksum Algorithms - Forza's ramblings` [#f3]_

RAID
----

Commands
````````

These examples assume that a RAID 1 BTRFS array (not BTRFS over mdadm RAID)
is mounted on ``/data``.

.. note::

    After creating, adding or removing devices from an array always run the balance operation.
    You may want to use GNU screen to put the process in background, like this

    .. code-block:: shell-session

       apt-get install screen
       screen btrfs balance start /data

Create
~~~~~~

#. create the array

   .. code-block:: shell-session

      mkfs.btrfs --csum xxhash -m raid1 -d raid1 /dev/sda1 /dev/sdb1

#. mount normally

   .. code-block:: shell-session

      mount /dev/sdb1 /data

#. To add new partitions, for example ``/dev/sdc1`` and ``/dev/sdd1``, run

   .. code-block:: shell-session

      btrfs device add /dev/sdc1 /dev/sdd1 /data

#. run the balance operation

Removing devices
~~~~~~~~~~~~~~~~

#. remove two devices at the same time

   .. code-block:: shell-session

      btrfs device delete /dev/sdc1 /dev/sdd1 /data

#. run the balance operation

Replace a device
~~~~~~~~~~~~~~~~

#. replace ``/dev/old`` with ``/dev/new``. This operation should run in background

   .. code-block:: shell-session

      btrfs replace start /dev/old /dev/new /data

#. check

   .. code-block:: shell-session

      btrfs replace status /data

Convert metadata profile
~~~~~~~~~~~~~~~~~~~~~~~~

You can clone partitions with ``dd`` from HDDs to SSDs. After that run:

.. code-block:: shell-session

   screen btrfs balance start -v -mconvert=single /data

Check usage
~~~~~~~~~~~

.. code-block:: shell-session

   btrfs filesystem usage /data
   btrfs filesystem df /data

.. rubric:: Footnotes

.. [#f1] https://btrfs.wiki.kernel.org/index.php/Using_Btrfs_with_Multiple_Devices unknown license
.. [#f2] https://wiki.archlinux.org/index.php/Btrfs GNU Free Documentation License 1.3 or later, Copyright (c) ArchWiki contributors
.. [#f3] https://wiki.tnonline.net/w/Btrfs/Checksum_Algorithms CC-BY-SA 4.0, Copyright (c) Forza's ramblings
