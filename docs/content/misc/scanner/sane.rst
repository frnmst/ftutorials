SANE
====

Alternative setup
-----------------

Airscan is used instead of the traditional client-server model with SANEd.

Server
``````

.. seealso::

    - _`Using Raspberry Pi as scan server? · Discussion #115 · alexpevzner/sane-airscan · GitHub` [#f1]_
    - _`SaneOverNetwork - Debian Wiki` [#f2]_

.. note:: These instructions have been tested on Raspberry Pi OS Lite 11 only

#. disable all backends in ``/etc/sane/dll.conf``
#. install these packages

   .. code-block:: shell-session

      apt-get install ipp-usb sane sane-airscan

#. get the scanner URI

   .. code-block:: shell-session

      airscan-discover

Clients
```````

.. seealso::

   - _`Scanner not working in Linux Ubuntu Fedora Mint Debian over the network?  Use sane-airscan! - rtt - IT Resource` [#f3]_

#. install these packages

   .. code-block:: shell-session

      apt-get install sane sane-airscan

#. add the scanner name and URI you got from the airscan discover command to the configuration file.
   In this example the scanner name is ``scanner name`` and the URI is ``http://192.168.0.1:60000/eSCL/``

   .. literalinclude:: includes/etc/sane.d/airscan.conf
      :language: ini
      :linenos:
      :caption: /etc/sane.d/airscan.conf

#. open one of the scanner apps such as simple-scan

Upgrading from Debian 10 to Debian 11
-------------------------------------

After upgrading to Debian 11 the HP AIO scanner took very long to be detected
and sometimes it did not work.

.. seealso::

   - _`Bug #1817214 “Zen and the art of Scanner Maintenance (Bug #17975...” : Bugs : HPLIP` [#f4]_

#. install these packages

   .. code-block:: shell-session

      apt-get install hp-ppd hplip hplip-data libhpmud0 sane

#. reinstall ``libsane-hpaio`` as suggested by reference 1

   .. code-block:: shell-session

      apt --reinstall install libsane-hpaio

#. comment all backends in ``/etc/sane.d/dll.conf``
#. you should have a :download:`file <includes/etc/sane.d/dll.d/hplip>`
   in the SANE configuration, like this

   .. literalinclude:: includes/etc/sane.d/dll.d/hplip
      :language: ini
      :linenos:
      :caption: /etc/sane.d/dll.d/hplip

#. check if scanimage detects the scanner in a matter of seconds

   .. code-block:: shell-session

      scanimage -L

#. check if remote scanning still works

.. rubric:: Footnotes

.. [#f1] https://github.com/alexpevzner/sane-airscan/discussions/115 unknown license
.. [#f2] https://wiki.debian.org/SaneOverNetwork#escl unknown license
.. [#f3] https://realtechtalk.com/Scanner_not_working_in_Linux_Ubuntu_Fedora_Mint_Debian_over_the_network_Use_saneairscan-2338-articles unknown license
.. [#f4] https://bugs.launchpad.net/hplip/+bug/1817214 unknown license
