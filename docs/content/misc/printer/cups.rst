CUPS
====

HTTPS error with CUPS remote administration
-------------------------------------------

You may want disable HTTPS in CUPS (if CUPS uses the default self-signed certificate)
if you experience browsers throwing errors and denying browsing.

.. warning:: Do this only if you are in a safe network environment
             since plaintext credentials can be sniffed!

.. seealso::

   - _`CUPS/Printer sharing - ArchWiki` [#f1]_

#. change the encryption level in
   the :download:`configuration file <includes/etc/cups/cupsd.conf>`

   .. literalinclude:: includes/etc/cups/cupsd.conf
      :language: ini
      :linenos:
      :emphasize-lines: 13
      :caption: /etc/cups/cupsd.conf

#. restart CUPS

   .. code-block:: ini

      systemctl restart cups

.. rubric:: Footnotes

.. [#f1] https://wiki.archlinux.org/title/CUPS/Printer_sharing#Remote_administration GNU Free Documentation License 1.3 or later, Copyright (c) ArchWiki contributors
