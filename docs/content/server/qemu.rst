QEMU
====

.. _qemu server:

Use declarative configuration to configure and run virtual machines
on a server via QEMU

Running
-------

.. seealso::

   - A collection of scripts I have written and/or adapted that I currently use on my systems as automated tasks [#f1]_

Basic steps
```````````

#. install these packages

   .. code-block:: shell-session

      apt-get install qemu-system-x86 openssh-client python3-yaml

#. install fpyutils. See :ref:`reference <installation of fpyutils>`
#. create a new user

   .. code-block:: shell-session

      useradd --system -s /bin/bash -U qvm
      passwd qvm
      usermod -aG jobs qvm

#. create the jobs directories. See :ref:`reference <jobs instructions>`

   .. code-block:: shell-session

      mkdir -p /home/jobs/{scripts,services}/by-user/qvm

#. create the _`qvm` :download:`script <includes/home/jobs/scripts/by-user/qvm/qvm.py>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/qvm/qvm.py
      :language: python
      :caption: /home/jobs/scripts/by-user/qvm/qvm.py

#. create the :download:`configuration <includes/home/jobs/scripts/by-user/qvm/qvm.yaml>` for qvm

   .. literalinclude:: includes/home/jobs/scripts/by-user/qvm/qvm.yaml
      :language: yaml
      :caption: /home/jobs/scripts/by-user/qvm/qvm.yaml

#. create the :download:`Systemd <includes/home/jobs/services/by-user/qvm/qvm.local_test.service>` _`service unit file for qvm`

   .. literalinclude:: includes/home/jobs/services/by-user/qvm/qvm.local_test.service
      :language: ini
      :caption: /home/jobs/services/by-user/qvm/qvm.local_test.service

#. fix the permissions

   .. code-block:: shell-session

      chmod 700 -R /home/jobs/{scripts,services}/by-user/qvm
      chown -R qvm:qvm /home/jobs/{scripts,services}/by-user/qvm

#. create a new virtual hard disk

   .. code-block:: shell-session

      sudo -i -u qvm
      qemu-img create -f qcow2 development.qcow2 64G

#. modify the configuration to point to ``development.qcow2``, the virtual hdd.
   See the commented ``local.test.system.drives[0]`` key

   Also set the cdrom ``device`` and ``enabled`` values in ``local.test.cdrom``

#. run the installation

   .. code-block:: shell-session

      ./qvm ./qvm.py local development

#. once the installation is finished power down the machine
#. create a backup virtual hard disk

   .. code-block:: shell-session

      qemu-img create -f qcow2 -b development.qcow2 development.qcow2.mod

#. set ``local.test.system.cdrom.enabled`` to ``false``.
   Set ``local.test.system.drives[0]`` to ``development.qcow2.mod``

#. run the virual machine

   .. code-block:: shell-session

      ./qvm ./qvm.py local development

#. quit the virtual machine and go back to the root user

   .. code-block:: shell-session

      exit

#. run the :ref:`deploy script <deploy script>`
#. if you are using iptables rules on the host machine modify the rules to let data through the shared ports
#. continue with the :ref:`client configuration <qemu client>`

Automatic shutdown
``````````````````

.. seealso::

   - _`SSH config host match port` [#f2]_

To be able to shutdown automatically when the Systemd service is stopped follow these instructions

#. connect to the guest machine
#. create a new user

   .. code-block:: shell-session

      sudo -i
      useradd -m -s /bin/bash -U powermanager
      passwd powermanager

#. change the sudoers file

   .. code-block:: shell-session

      visudo

   Add this line

   .. code-block:: ini

      powermanager ALL=(ALL) NOPASSWD:/sbin/poweroff

#. go back on the host machine and create an SSH key
   so that the ``qvm`` host user can connect to the ``powermanager`` guest user.
   Do not encrypt the key with a passphrase

   .. code-block:: shell-session

      sudo -i -u qvm
      ssh-keygen -t rsa -b 16384 -C "qvm@host-2022-01-01"

   Save the keys as ``~/.ssh/powermanager_test``.

   Have a look at the ``ExecStop`` command in the
   :ref:`Systemd service unit file <service unit file for qvm>`
#. in the host machine, configure the :download:`SSH config file <includes/home/qvm/.ssh/config>` like this

   .. literalinclude:: includes/home/qvm/.ssh/config
      :language: ini
      :caption: /home/qvm/.ssh/config

#. copy the content of ``/home/qvm/.ssh/powermanager_test.pub`` into
   ``/home/powermanager/.ssh/authorized_keys`` of the guest machine

Using physical partitions
`````````````````````````

Instead of using QCOW2 disk files you can use existing phisical partitions and filesystems.

.. warning:: Remember NOT to mount the partitions while running because data loss occurs in
             that case.

#. uncomment and edit the highlighted lines and comment the original drive line

   .. literalinclude:: includes/home/jobs/scripts/by-user/qvm/qvm.yaml
      :language: yaml
      :lines: 53-63
      :emphasize-lines: 9-11
      :caption: /home/jobs/scripts/by-user/qvm/qvm.yaml

#. run the :ref:`deploy script <deploy script>`

Share a directory
`````````````````

9p filesystem
^^^^^^^^^^^^^

If you need to share a directory you can use a 9p filesystem if the
guest kernel supports it. This is the simplest way to share a directory
and it does not require the root user. The downside is low performance.

#. connect to the guest machine and run the following

   .. code-block:: shell-session

      sudo -i
      modprobe 9pnet_virtio

   In case your kernel does not support 9p you might get a message like the following.
   For example this is the case for Debian's ``linux-image-cloud-amd64`` kernel

   ::

      modprobe: FATAL: Module 9pnet_virtio not found in directory /lib/modules/5.10.0-9-cloud-amd64

   If that is the case you need to find a different method such as SSHFS

#. create the mount directory

   .. code-block:: shell-session

      exit
      mkdir -p /home/qvm/shares/test

#. add a shared directory in the configuration

   .. literalinclude:: includes/home/jobs/scripts/by-user/qvm/qvm.yaml
      :language: yaml
      :lines: 78-85
      :emphasize-lines: 2-6
      :caption: /home/jobs/scripts/by-user/qvm/qvm.yaml

#. connect to the guest machine and add an :download:`fstab <includes/etc/qvm.fstab>`
   entry. In this example the directory is mounted in ``/home/vm/shared``

   .. literalinclude:: includes/etc/qvm.fstab
      :language: ini
      :lines: 2
      :caption: /etc/fstab

#. create the shared directory

   .. code-block:: shell-session

      mkdir /home/vm/shared

#. quit the virtual machine
#. restart the virtual machine

   .. code-block:: shell-session

      systemctl restart qvm.local_test.service

virtiofs
^^^^^^^^

As an alternative to plan9p you can use virtiofs to get more
performance.

.. seealso::

   - _`virtiofs - shared file system for virtual machines / Standalone usage` [#f7]_
   - _`QEMU/KVM + virtio-fs - Sharing a host directory with a virtual machine - TauCeti blog` [#f8]_
   - _`QEMU - ArchWiki - Using filesystem passthrough and VirtFS` [#f9]_

#. add a shared directory in the configuration

   .. literalinclude:: includes/home/jobs/scripts/by-user/qvm/qvm.yaml
      :language: yaml
      :lines: 84-92
      :emphasize-lines: 2-6
      :caption: /home/jobs/scripts/by-user/qvm/qvm.yaml

   By using virtiofs you need to pass the socket not the actual path of the
   shared directory on the host. See below

#. create this :download:`script <includes/home/jobs/scripts/by-user/root/qvm_virtiofs.sh>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/qvm_virtiofs.sh
      :language: shell
      :caption: /home/jobs/scripts/by-user/root/qvm_virtiofs.sh

#. create the :download:`Systemd <includes/home/jobs/services/by-user/qvm/qvm.local_test.service>`
   _`service unit file for the qvm virtiofs script`

   .. literalinclude:: includes/home/jobs/services/by-user/root/qvm-virtiofs.local_test-qemu-vm-001.service
      :language: ini
      :caption: /home/jobs/services/by-user/root/qvm-virtiofs.local_test-qemu-vm-001.service

   The path of the shared directory on the host filesystem is ``/tmp/vm-001``

#. modify the :download:`Systemd <includes/home/jobs/services/by-user/qvm/qvm.local_test.service>`
   service unit file for qvm by uncommening the highlighed lines

   .. literalinclude:: includes/home/jobs/services/by-user/qvm/qvm.local_test.service
      :language: ini
      :emphasize-lines: 4,6,10
      :caption: /home/jobs/services/by-user/qvm/qvm.local_test.service

#. connect to the guest machine and add an :download:`fstab <includes/etc/qvm.fstab>`
   entry. In this example the directory is mounted in ``/mnt``

   .. literalinclude:: includes/etc/qvm.fstab
      :language: ini
      :lines: 5
      :caption: /etc/fstab

#. quit the virtual machine
#. restart the virtual machine

   .. code-block:: shell-session

      systemctl restart qvm.local_test.service

.. important:: I noticed a very high memory usage with the provided options
               (~= 13 GB resident memory for ~= 40 GB of ~= 46000 files).
               Try setting, for example, ``-o cache=none`` instead of
               ``-o cache=always`` in the ``qvm_virtiofs.sh`` script.

Resize disks
------------

.. seealso::

   - _`How to Expand QCOW2` [#f3]_
   - _`Provider::VMonG5K — EnOSlib 8.0.0a12 8.0.0a12 documentation` [#f4]_

#. stop the virtual machine
#. install this package

   .. code-block:: shell-session

      apt-get install libguestfs-tools

   Have a look at `this bug report <https://bugs.launchpad.net/ubuntu/+source/lvm2/+bug/1746582>`_
   if you have problem installing
#. resize for example by increasing to 40G more, where ``virtual_hard_disk`` is a `backing file`.
   I always set the backing file ending in ``.mod``.

   .. code-block:: shell-session

      qemu-img resize ${virtual_hard_disk} +40G

#. make a backup

   .. code-block:: shell-session

      cp -aR image.qcow2.mod image.qcow2.mod.bak

#. get the partition name you want to expand. Usually partition names start from
   ``/dev/sda1`` (note: these partition names are not the same as the host
   system ones!)

   .. code-block:: shell-session

      virt-filesystems --long --human-readable --all --add ${virtual_hard_disk}

#. execute the actual resize operation on the virtual partition
   and filesystem. You can use ``sda1`` for example as ``partition_name``

   .. code-block:: shell-session

      virt-resize --expand ${patition_name} ${virtual_hard_disk}.bak ${virtual_hard_disk}

#. start the virtual machine
#. if everything works remove the ``${virtual_hard_disk}.bak`` file

Rename disk files
-----------------

.. seealso::

   - _`Qemu-img Cheatsheet | Programster's Blog` [#f5]_

Rename backing file
```````````````````

TODO

FSCK
----

You must run FSCK while the virtual machine is off
if you want to fix the root partition.

.. seealso::

   - _`kvm virtualization - How to run fsck on guest VMs from KVM - Server Fault` [#f6]_

#. identify the troubling filesystem by running the virtual
   machine in "display" mode, not via SSH: if the broken partition
   is root your virtual machine might not get to load SSHD.
#. stop the virtual machine
#. load the virtual hard disk

   .. code-block:: shell-session

      guestfish -a ${virtual_hard_disk}
      run
      list-filesystems

   You get a list of partitions with the last command, for example
   `/dev/sda1`.

#. You can run various fsck commands such as

   .. code-block:: shell-session

      e2fsck-f ${partition_name}
      e2fsck ${partition_name} /dev/sda1 forceall:true
      e2fsck ${partition_name} correct:true
      fsck ${partition_name}

#. Quit the program and start the virtual machine

   .. code-block:: shell-session

      exit

.. rubric:: Footnotes

.. [#f1] https://software.franco.net.eu.org/frnmst/automated-tasks GNU GPLv3+, copyright (c) 2019-2022, Franco Masotti
.. [#f2] https://superuser.com/a/870918 CC BY-SA 3.0, (c) 2015, Kenster (at superuser.com)
.. [#f3] https://blog.khmersite.net/2020/11/how-to-expand-qcow2/ unknown license, © 2022
.. [#f4] https://discovery.gitlabpages.inria.fr/enoslib/tutorials/vmong5k.html#changing-resource-size-of-virtual-machines GPLv3+, © Copyright 2017, Ronan-Alexandre Cherrueau, Matthieu Simonin
.. [#f5] https://blog.programster.org/qemu-img-cheatsheet unknown license,
.. [#f6] https://serverfault.com/a/380210 CC BY-SA 3.0, Copyright (c) 2012 mrc (at serverfault.com)
.. [#f7] https://virtio-fs.gitlab.io/howto-qemu.html, Copyright (c) virtio-fs
.. [#f8] https://www.tauceti.blog/posts/qemu-kvm-share-host-directory-with-vm-with-virtio/ © Copyright 2022
.. [#f9] https://wiki.archlinux.org/title/QEMU#Using_filesystem_passthrough_and_VirtFS GNU Free Documentation License 1.3 or later, Copyright (c) ArchWiki contributors
