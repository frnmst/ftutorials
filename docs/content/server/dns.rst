.. _DNS:

DNS
===

Unbound
-------

Use a local DNS proxy server to:

- improve name resoultion speed
- hide unwanted websites
- redirect names to specific local addresses

.. figure:: assets/hblock_unbound_0.png
   :scale: 50 %

   Hblock in action

.. seealso::

   - _`Unbound` [#f1]_
   - _`dnscrypt-proxy 2` [#f2]_
   - _`hblock` [#f3]_
   - _`hmirror` [#f4]_

#. install the dependencies

   .. code-block:: shell-session

      apt-get install git bash unbound dnscrypt-proxy python3-requests python3-yaml make

#. install fpyutils. See :ref:`reference <installation of fpyutils>`
#. configure dnscrypt-proxy

   .. literalinclude:: includes/etc/dnscrypt-proxy/dnscrypt-proxy.toml
      :language: toml
      :caption: /etc/dnscrypt-proxy/dnscrypt-proxy.toml

#. restart the dnscrypt-proxy service

   .. code-block:: shell-session

      systemctl restart dnscrypt-proxy

#. create the jobs directories. See :ref:`reference <jobs instructions>`

   .. code-block:: shell-session

      mkdir -p /home/jobs/{scripts,services}/by-user/root

#. clone the hblock repository

   .. code-block:: shell-session

      pushd /home/jobs/scripts/by-user/root
      git clone https://software.franco.net.eu.org/mirrors-shell/hblock.git
      popd

#. create the :download:`script <includes/home/jobs/scripts/by-user/root/hblock_unbound.py>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/hblock_unbound.py
      :language: python
      :caption: /home/jobs/scripts/by-user/root/hblock_unbound.py

#. create a :download:`configuration file <includes/home/jobs/scripts/by-user/root/hblock_unbound.yaml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/hblock_unbound.yaml
      :language: yaml
      :caption: /home/jobs/scripts/by-user/root/hblock_unbound.yaml

#. create the :download:`hblock header file <includes/home/jobs/scripts/by-user/root/hblock_unbound.header.conf>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/hblock_unbound.header.conf
      :language: ini
      :caption: /home/jobs/scripts/by-user/root/hblock_unbound.header.conf

#. create the :download:`hblock footer file <includes/home/jobs/scripts/by-user/root/hblock_unbound.footer.conf>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/hblock_unbound.footer.conf
      :language: ini
      :caption: /home/jobs/scripts/by-user/root/hblock_unbound.footer.conf

#. configure you :download:`hblock lists <includes/home/jobs/scripts/by-user/root/hblock_unbound_list.txt>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/hblock_unbound_list.txt
      :language: ini
      :caption: /home/jobs/scripts/by-user/root/hblock_unbound_list.txt

#. use this :download:`Systemd service unit file <includes/home/jobs/services/by-user/root/hblock-unbound.service>`

   .. literalinclude:: includes/home/jobs/services/by-user/root/hblock-unbound.service
      :language: ini
      :caption: /home/jobs/services/by-user/root/hblock-unbound.service

#. fix the permissions

   .. code-block:: shell-session

      chmod 700 /home/jobs/scripts/by-user/root/hblock-unbound.*
      chmod 700 -R /home/jobs/services/by-user/root

#. run the :ref:`deploy script <deploy script>`

.. important:: In case something goes wrong with the deployment you can use these fallback commands

               .. code-block:: shell-session

                  pushd /home/jobs/scripts/by-user/root
                  cat hblock_unbound.header.conf hblock_unbound.footer.conf > /etc/unbound/unbound.conf
                  popd
                  systemctl restart unbound

.. [#f1] https://nlnetlabs.nl/projects/unbound/about/ BSD 3-Clause License, Copyright (c) 2007, NLnet Labs. All rights reserved
.. [#f2] https://github.com/jedisct1/dnscrypt-proxy ISC License, Copyright (c) 2018-2022, Frank Denis <j at pureftpd dot org>
.. [#f3] https://github.com/hectorm/hblock MIT, Copyright © 2022, Héctor Molinero Fernández
.. [#f4] https://github.com/hectorm/hmirror MIT, Copyright © 2022, Héctor Molinero Fernández
