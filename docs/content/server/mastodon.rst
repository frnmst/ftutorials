.. _Mastodon:

Mastodon
========

TODO

Services
--------

.. seealso::

   - _`Adventures in Mastodon Self-Hosting: Clean Media Storage with tootctl | Matt Burke` [#f1]_

#. create a Systemd unit file

   .. literalinclude:: includes/home/jobs/services/by-user/root/mastodon-cleanup.service
      :language: ini
      :caption: /home/jobs/services/by-user/root/mastodon-cleanup.service

#. create a Systemd timer unit file

   .. literalinclude:: includes/home/jobs/services/by-user/root/mastodon-cleanup.timer
      :language: ini
      :caption: /home/jobs/services/by-user/root/mastodon-cleanup.timer

.. rubric:: Footnotes

.. [#f1] https://www.mattburkedev.com/adventures-in-mastodon-self-hosting-clean-media-storage-with-tootctl/ unknown license
