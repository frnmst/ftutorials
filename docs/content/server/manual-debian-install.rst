Manual Debian Install
=====================

Standard
--------

In this example we will do a manual installation of Debian GNU/Linux
from another Debian GNU/Linux system.

This gives us more control on the packages to install and more variability on
disk configurations. This method is similar to the one used to install
Arch Linux.

.. seealso::

  - _`D.3. Installing Debian GNU/Linux from a Unix/Linux System` [#f1]_
  - _`RAID - ArchWiki` [#f2]_
  - _`Syslinux - ArchWiki` [#f3]_ [#f4]_

+-------------------+---------------+-------------+
| Partition name    | Mount point   | mdadm name  |
+-------------------+---------------+-------------+
| /dev/nvme0n1p0    | /boot         | /dev/md0    |
+-------------------+---------------+-------------+
| /dev/nvme0n1p1    | /             | /dev/md1    |
+-------------------+---------------+-------------+
| /dev/nvme0n1p2    | /home         | /dev/md2    |
+-------------------+---------------+-------------+
| /dev/nvme0n1p3    | /data         | /dev/md3    |
+-------------------+---------------+-------------+
| /dev/nvme1n1p0    | /boot         | /dev/md0    |
+-------------------+---------------+-------------+
| /dev/nvme1n1p1    | /             | /dev/md1    |
+-------------------+---------------+-------------+
| /dev/nvme1n1p2    | /home         | /dev/md2    |
+-------------------+---------------+-------------+
| /dev/nvme1n1p3    | /data         | /dev/md3    |
+-------------------+---------------+-------------+

#. install the packages to perform a Debian installation

   .. code-block:: shell-session

      apt-get install debootstrap arch-install-scripts

#. create GPT partitions using fdisk and set their type to *Linux RAID*

   .. code-block:: shell-session

      fdisk /dev/nvme0n1
      # [ ... ]

      fdisk /dev/nvme1n1
      # [ ... ]

#. make the RAID devices

   .. code-block:: shell-session

      mdadm --create --verbose --level=1 --metadata=1.0 --raid-devices=2 /dev/md0 /dev/nvme0n1p0 /dev/nvme1n1p0
      mdadm --create --verbose --level=10 --metadata=1.2 --chunk=512 --raid-devices=2 --layout=f2 /dev/md1 /dev/nvme0n1p1 /dev/nvme1n1p1
      mdadm --create --verbose --level=10 --metadata=1.2 --chunk=512 --raid-devices=2 --layout=f2 /dev/md2 /dev/nvme0n1p2 /dev/nvme1n1p2
      mdadm --create --verbose --level=10 --metadata=1.2 --chunk=512 --raid-devices=2 --layout=f2 /dev/md3 /dev/nvme0n1p3 /dev/nvme1n1p3

#. make the filesystems

   .. code-block:: shell-session

      mkfs.ext4 /dev/md0
      mkfs.f2fs -O extra_attr,inode_checksum,sb_checksum /dev/md1
      mkfs.f2fs -O extra_attr,inode_checksum,sb_checksum /dev/md2
      mkfs.f2fs -O extra_attr,inode_checksum,sb_checksum /dev/md3

#. enable a swap partiton optionally
#. mount the partitions

   .. code-block:: shell-session

      mount /dev/md1 /mnt/debinst
      mkdir /mnt/debinst/boot /mnt/debinst/home /mnt/debinst/data
      mount /dev/md0 /mnt/debinst/boot
      mount /dev/md2 /mnt/debinst/home
      mount /dev/md3 /mnt/debinst/data

#. debootstrap

   .. code-block:: shell-session

      /usr/sbin/debootstrap --arch amd64 bullseye /mnt/debinst https://debian.mirror.garr.it/debian/

#. generate the fstab file

   .. code-block:: shell-session

      genfstab -U /mnt/debinst >> /mnt/debinst/etc/fstab

#. do a chroot

   .. code-block:: shell-session

      arch-chroot /mnt/debinst

#. set the timezone, locale and fix the binaries path for the root user

   .. code-block:: shell-session

      echo 'PATH=$PATH:/sbin' >> ~/.bashrc
      dpkg-reconfigure tzdata
      apt-get install locales && dpkg-reconfigure locales
      locale-gen

#. install these packages: these are just a selection of stuff I frequently use

   .. code-block:: shell-session

      apt-get install apt-transport-tor f2fs-tools btrfs-progs htop openssh-server openssh-client sshfs haveged ntpsec apache2 postfix dovecot-core fail2ban cups ffmpeg motion rtorrent docker smartmontools autossh screen nut python3-pip curl wget python3 git sane irqbalance unbound thermald earlyoom dnscrypt-proxy qemu-system-x86 qemu rsync roundcube-core schedtool certbot python-certbot-apache lshw usbutils powertop offlineimap gdisk bash-completion linux-image-amd64 man-db w3m dnsutils lm-sensors borgmatic python3-bs4 console-setup keyboard-configuration

#. update the RAID configuration

   .. code-block:: shell-session

      apt-get install mdadm
      mdadm --detail --scan >> /etc/mdadm.conf

#. install the bootloader. I prefer using Syslinux on servers but unfortunately
   it needs to be installed manually on Debian

   .. code-block:: shell-session

      apt-get install syslinux extlinux
      mkdir /boot/syslinux
      cp /usr/lib/syslinux/modules/bios/*.c32 /boot/syslinux/
      extlinux --install /boot/syslinux
      sgdisk /dev/nvme0n1 --attributes=1:set:2
      sgdisk /dev/nvme1n1 --attributes=1:set:2
      sgdisk /dev/nvme0n1 --attributes=1:show
      sgdisk /dev/nvme1n1 --attributes=1:show
      dd bs=440 count=1 conv=notrunc if=/usr/lib/syslinux/mbr/gptmbr.bin of=/dev/nvme0n1
      dd bs=440 count=1 conv=notrunc if=/usr/lib/syslinux/mbr/gptmbr.bin of=/dev/nvme1n1

#. add this configuration for the bootloader

   .. literalinclude:: includes/boot/syslinux/syslinux.cfg
      :language: ini
      :linenos:
      :caption: /boot/syslinux/syslinux.cfg

   .. note:: The UUID of the root filesystem in this example is
             ``4a276a15-c79b-49ba-ac6e-17564eafb9fd``. You can get this
             value

             .. code-block:: shell-session

                lsblk -o name,uuid | grep md1

             The kernel version is ``5.10.0-13-amd64``

#. add these modules to the initramfs

   .. literalinclude:: includes/etc/initramfs-tools/modules
      :language: ini
      :linenos:
      :caption: /etc/initramfs-tools/modules

#. update the initramfs

   .. code-block:: shell-session

      update-initramfs -k all -u

#. blacklist the kernel package so we don't have to deal changing
   the bootloader configuration every time a new kernel is available.
   This way you are responsible to remove the blacklist and
   update Syslinux's configuration when needed

   .. code-block:: shell-session

      apt-mark hold linux-image-amd64

   .. note:: To remove the hold run

             .. code-block:: shell-session

                apt-mark unhold linux-image-amd64

#. edit ``/etc/network/interfaces`` to get a static IP address
#. enable the OpenSSH server

   .. code-block:: shell-session

      systemctl enable ssh

#. temporarely enable root access via ssh. Set this value

   .. code-block:: ini

      PermitRootLogin yes

   .. warning:: Remember to disable root access once you create new users!

#. add a new root password

   .. code-block:: shell-session

      passwd root

#. run sync and quit

   .. code-block:: shell-session

      sync
      exit

#. unmount the partitions

   .. code-block:: shell-session

      umount -R /mnt/debinst

Variations
----------

Encryption
``````````

If you are encrypting the root partition using LVM on LUKS you have to change
the bootloader configuration and the crypttab file.

.. seealso::

   - _`dm-crypt/Encrypting an entire system - ArchWiki` [#f5]_

#. change Syslinux's ``APPEND`` lines

   .. code-block:: ini
      :caption: /boot/syslinux/syslinux.cfg

      # [ ... ]
        APPEND cryptdevice=UUID=ABCD-0123:cryptlvm root=/dev/MyLVMgroupName/root rw
      # [ ... ]

#. edit the crypttab file like this

   .. code-block:: ini
      :caption: /etc/crypttab

      # <target name>	<source device>		<key file>	<options>
      key              UUID=ABCD-0123  none        luks


.. note:: - ``ABCD-0123`` is the UUID of the physical partition.
          - ``MyLVMgroupName`` is the name of the LVM group where the root partition is installed.

.. rubric:: Footnotes

.. [#f1] https://www.debian.org/releases/stretch/amd64/apds03.html.en GNU GPLv2, copyright (c) Debian GNU/Linux Installation Guide contributors
.. [#f2] https://wiki.archlinux.org/title/RAID#RAID_levels GNU Free Documentation License 1.3 or late, copyright (c) ArchWiki contributors
.. [#f3] https://wiki.archlinux.org/index.php/Syslinux#Manual_install GNU Free Documentation License 1.3 or late, copyright (c) ArchWiki contributors
.. [#f4] https://wiki.archlinux.org/index.php/Syslinux#GUID_partition_table GNU Free Documentation License 1.3 or late, copyright (c) ArchWiki contributors
.. [#f5] https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS GNU Free Documentation License 1.3 or late, copyright (c) ArchWiki contributors
