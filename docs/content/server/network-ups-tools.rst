Network UPS Tools
=================

NUT is a software which is able to monitor UPSs and take action when needed, such
as shutting down a computer when the battery level is low.

.. seealso::

   - _`Network UPS Tools - ArchWiki` [#f1]_
   - _`nut - Debian Wiki` [#f2]_

Example for Eaton 650i USB
--------------------------

#. create the jobs directories

   .. code-block:: shell-session

      mkdir -p /home/jobs/scripts/by-user/nut

#. Install NUT

   .. code-block:: shell-session

      apt-get install nut

#. add the following to the :download:`UPS definitions file <includes/etc/nut/nut.conf>`

   .. literalinclude:: includes/etc/nut/nut.conf
      :language: ini
      :linenos:
      :caption: /etc/nut/nut.conf

#. add the following to the :download:`UPS configuration file <includes/etc/nut/ups.conf>`

   .. literalinclude:: includes/etc/nut/ups.conf
      :language: ini
      :linenos:
      :caption: /etc/nut/ups.conf

#. add the following to the :download:`administrative user definitions file <includes/etc/nut/upsd.users>`

   .. literalinclude:: includes/etc/nut/upsd.users
      :language: ini
      :linenos:
      :caption: /etc/nut/upsd.users

#. add the following to the :download:`configuration file for the monitoring daemon <includes/etc/nut/upsmon.conf>`

   .. literalinclude:: includes/etc/nut/upsmon.conf
      :language: ini
      :linenos:
      :caption: /etc/nut/upsmon.conf

   You can comment ``NOTIFYCMD`` or change it with a custom command.
   You can, for example, create a script like this :download:`script <includes/home/jobs/scripts/by-user/nut/ups_notify.sh>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/nut/ups_notify.sh
      :language: shell
      :linenos:
      :caption: /home/jobs/scripts/by-user/nut/ups_notify.sh

#. restart all NUT services

   .. code-block:: shell-session

      systemctl restart nut-driver.service
      systemctl restart nut-server.service
      systemctl restart nut-monitor.service

#. fix the permissions

   .. code-block:: shell-session

      chmod 700 -R /home/jobs/scripts/by-user/nut
      chown -R nut:nut /home/jobs/scripts/by-user/nut

#. remove the plug from the UPS and check if the ``NOTIFYCMD`` command
   is executed and if you get broadcast messages on the terminal, such as

   .. code-block:: shell-session

      Broadcast message from ...
      UPS my_ups@127.0.0.1 on battery

#. put the plug back in the UPS

.. rubric:: Footnotes

.. [#f1] https://wiki.archlinux.org/title/Network_UPS_Tools GNU Free Documentation License 1.3 or later, Copyright (c) ArchWiki contributors
.. [#f2] https://wiki.debian.org/nut See https://wiki.debian.org/DebianWiki/LicencingTerms
