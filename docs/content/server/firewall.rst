Firewall
========

Use iptables to block IP addresses by country for inbound ports on a server.
This is a whitelist: what is not explicitly allowed is blocked. This is useful, for example, to filter
most SSH bruteforce attacks while leaving a webserver freely available.
This is certainly not the most efficient approach at filtering because
you can easily end with thousands of iptables rules that need to be scanned one by one.

.. seealso::

   - A collection of scripts I have written and/or adapted that I currently use on my systems as automated tasks [#f1]_
   - _`Simple shell script for GNU/Linux, built on iptables, which is able to filter incoming packets based on accepted port numbers and countries. It is aimed to SOHO users.` [#f2]_
   - _`Linux Iptables Just Block By Country - nixCraft` [#f3]_
   - _`Simple stateful firewall - ArchWiki` [#f4]_
   - _`iptables - ArchWiki` [#f5]_
   - _`25 Most Frequently Used Linux IPTables Rules Examples` [#f6]_

Setup
-----

#. install the dependencies

   .. code-block:: shell-session

      apt-get install iptables python3-yaml python3-requests iptables-persistent

#. answer ``Yes`` to all questions
#. install fpyutils. See :ref:`reference <installation of fpyutils>`
#. create the jobs directories. See :ref:`reference <jobs instructions>`

   .. code-block:: shell-session

      mkdir -p /home/jobs/{scripts,services}/by-user/root

#. create the :download:`script <includes/home/jobs/scripts/by-user/root/iptables_geoport.py>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/iptables_geoport.py
      :language: python
      :caption: /home/jobs/scripts/by-user/root/iptables_geoport.py

#. create a :download:`configuration file <includes/home/jobs/scripts/by-user/root/iptables_geoport.yaml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/iptables_geoport.yaml
      :language: yaml
      :caption: /home/jobs/scripts/by-user/root/iptables_geoport.yaml

   .. note:: Rules are scanned sequentially. Move frequently used rules upper in the file to improve performance.

#. use this :download:`Systemd service unit file <includes/home/jobs/services/by-user/root/iptables-geoport.service>`

   .. literalinclude:: includes/home/jobs/services/by-user/root/iptables-geoport.service
      :language: ini
      :caption: /home/jobs/services/by-user/root/iptables-geoport.service

#. fix the permissions

   .. code-block:: shell-session

      chmod 700 -R /home/jobs/scripts/by-user/iptables_geoport.*
      chmod 700 -R /home/jobs/services/by-user/root

#. run the :ref:`deploy script <deploy script>`

SANE
----

To be able to use a remote scanner with `SANE <http://www.sane-project.org/>`_
using this setup you need to follow these steps

.. seealso::

   - _`SANE - ArchWiki` [#f7]_
   - _`Secure use of iptables and connection tracking helpers` [#f8]_

#. open TCP and UDP ports 6566

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/iptables_geoport.yaml
      :language: yaml
      :lines: 65-
      :emphasize-lines: 5-8
      :lineno-start: 65
      :caption: /home/jobs/scripts/by-user/root/iptables_geoport.yaml (extract)

#. add :download:`this file <includes/etc/modprobe.d/nf_conntrack.conf>` to load the kernel module

   .. literalinclude:: includes/etc/modprobe.d/nf_conntrack.conf
      :language: ini
      :caption: /etc/modprobe.d/nf_conntrack.conf

#. add :download:`this file <includes/etc/modules-load.d/nf_conntrack_sane.conf>` to load the kernel module

   .. literalinclude:: includes/etc/modules-load.d/nf_conntrack_sane.conf
      :language: ini
      :caption: /etc/modules-load.d/nf_conntrack_sane.conf

#. reboot
#. check if the rules are active: ``1`` should be returned by the ``cat`` command

   .. code-block:: shell-session

      cat /proc/sys/net/netfilter/nf_conntrack_helper
      1

.. rubric:: Footnotes

.. [#f1] https://software.franco.net.eu.org/frnmst/automated-tasks GNU GPLv3+, copyright (c) 2019-2022, Franco Masotti
.. [#f2] https://software.franco.net.eu.org/frnmst-archives/iptables-geoport-directives GNU GPLv3+ and GNU GPLv2+, copyright (c) 2015 Franco Masotti
.. [#f3] http://www.cyberciti.biz/faq/?p=3402 GPL v.2.0+, copyright (c) Author: nixCraft <www.cyberciti.biz>
.. [#f4] https://wiki.archlinux.org/index.php/Simple_stateful_firewall GNU Free Documentation License 1.3 or late, copyright (c) ArchWiki contributors
.. [#f5] https://wiki.archlinux.org/index.php/Iptables GNU Free Documentation License 1.3 or late, copyright (c) ArchWiki contributors
.. [#f6] http://www.thegeekstuff.com/2011/06/iptables-rules-examples/ unknown license
.. [#f8] https://wiki.archlinux.org/title/SANE#Firewall GNU Free Documentation License 1.3 or late, copyright (c) ArchWiki contributors
.. [#f7] https://home.regit.org/wp-content/uploads/2011/11/secure-conntrack-helpers.html unknown license
