.. _Redis:

Redis
=====

To use Redis with the presented configuration, client programs need to access
Redis's socket at ``/var/run/redis/redis-server.sock``. You need to add the
running client users to the ``redis`` group. As an alternative you can
also use ``socat`` to create a copy of the socket with the appropriate
permissions.

.. important:: In this example we will use ``/redis`` as the database
               destination to avoid filling up the root partition.

.. seealso::

   - _`amazon ec2 - Redis reports read-only filesystem, but it isn't - Server Fault` [#f1]_

#. install the dependencies

   .. code-block:: shell-session

      apt-get update
      apt-get install redis-server rsync

#. create the new redis directory

   .. code-block:: shell-session

      systemctl stop redis.service
      mkdir /redis

#. mount the partition in ``/redis``
#. move the  existing content

   .. code-block:: shell-session

      rsync --avAX /var/lib/redis/dump.rdb /redis

#. edit the :download:`configuration file <includes/etc/redis/redis.conf>` like this

   .. literalinclude:: includes/etc/redis/redis.conf
      :language: ini
      :caption: /etc/redis/redis.conf

#. edit the service unit file

   .. code-block:: shell-session

      systemctl edit redis.service

   Add this content

   .. code-block:: ini

      [Service]
      ReadWritePaths=-/redis

#. restart the service

   .. code-block:: shell-session

      systemctl restart redis.service

.. rubric:: Footnotes

.. [#f1] https://serverfault.com/questions/942328/redis-reports-read-only-filesystem-but-it-isnt CC BY-SA 4.0, Copyright (c) 2018 serverfault.com contributors
