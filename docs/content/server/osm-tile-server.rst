OSM tile server
===============

Host your own OpenStreet Map server.

Via docker-compose
------------------

.. seealso::

   - _`GitHub - Overv/openstreetmap-tile-server: Docker file for a minimal effort OpenStreetMap tile server` [#f1]_

#. follow the :ref:`Docker <Docker>` instructions
#. create the jobs directories

   .. code-block:: shell-session

      mkdir -p /home/jobs/scripts/by-user/root/docker/osm-tile-server
      mkdir -p /home/jobs/services/by-user/root

#. create the directories for the volumes

   .. code-block:: shell-session

      mkdir -p /data/DOCKER_VOLUMES/openstreetmap-tile-server/db
      mkdir -p /data/DOCKER_VOLUMES/openstreetmap-tile-server/openstreetmap-rendered-tiles

#. database must be at least on SSDs to have decent performance, even better if they
   are on RAID 0.
   The faster the better: multiple fast NVME drives on RAID 0 with a fast filesystem
   for flash memory, such as F2FS, would be the best option.

   If a disk breaks you can swap it and re-import the maps since it's not vital data.

   Mount the filesystems on the SSDs on
   ``/data/DOCKER_VOLUMES/openstreetmap-tile-server/db``.

   I didn't see any performace hit in putting the cache on normal HDDs. Put the cache in
   ``/data/DOCKER_VOLUMES/openstreetmap-tile-server/openstreetmap-rendered-tiles``.

   .. note:: Importing Italy, for example, takes aroung 50GB of space for the database

#. download the pbf and poly files from `geofabrik.de <https://www.geofabrik.de>`_

   .. code-block:: shell-session

      cd /data/DOCKER_VOLUMES/openstreetmap-tile-server
      wget https://download.geofabrik.de/europe/italy-latest.osm.pbf
      wget https://download.geofabrik.de/europe/italy.poly
      https://download.geofabrik.de/europe/italy-latest.osm.pbf.md5
      md5sum --check

#. change permissions on the cache directory so that Docker can
   mount it in the container

   .. code-block:: shell-session

      chown 1000:1000 /data/DOCKER_VOLUMES/openstreetmap-tile-server/openstreetmap-rendered-tiles

#. create a :download:`Docker compose file <includes/home/jobs/scripts/by-user/root/docker/openstreetmap-tile-server/docker-compose.yml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/docker/openstreetmap-tile-server/docker-compose.yml
      :language: yaml
      :linenos:
      :caption: /home/jobs/scripts/by-user/root/docker/openstreetmap-tile-server/docker-compose.yml

#. import the data in the database. This operation takes a long time

   .. code-block:: shell-session

      cd /home/jobs/scripts/by-user/root/docker/osm-tile-server
      docker-compose up import

#. create a :download:`Systemd unit file <includes/home/jobs/services/by-user/root/docker-compose.openstreetmap-tile-server.service>`.
   See also the :ref:`Docker compose services <Docker compose services>` section

   .. literalinclude:: includes/home/jobs/services/by-user/root/docker-compose.openstreetmap-tile-server.service
      :language: ini
      :linenos:
      :caption: /home/jobs/services/by-user/root/docker-compose.openstreetmap-tile-server.service

#. fix the permissions

   .. code-block:: shell-session

      chmod 700 /home/jobs/scripts/by-user/root/docker/osm-tile-server
      chmod 700 -R /home/jobs/services/by-user/root

#. run the :ref:`deploy script <deploy script>`
#. connect to http://127.0.0.1:4006 with a JavaScript supported browser.
   You should see a full screen planet map.

   Rendering is very slow the first time.

   Try zooming into a zone and wait until it's fully rendered. Reload the browser
   page with `CTRL+shift+R` and go to the same zone. This time the rendering should
   be istantaneous
#. Add a virtual host in your webserver configuration using port ``4006`` as reverse proxy

.. rubric:: Footnotes

.. [#f1] https://github.com/Overv/openstreetmap-tile-server.git Apache License, Copyright 2019 Alexander Overvoorde
