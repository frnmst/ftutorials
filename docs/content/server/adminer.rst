Adminer
=======

With adminer you can control various databases via a web UI. It is the
successor of phpMyAdmin.

Docker
------

.. seealso::

   - _`GitHub - vrana/adminer: Database management in a single PHP file` [#f1]_
   - _`GitHub - TimWolla/docker-adminer: Database management in a single PHP file` [#f2]_
   - _`GitHub - docker-library/official-images: Primary source of truth for the Docker "Official Images" program` [#f3]_

#. follow the :ref:`Docker <Docker>` instructions
#. create the jobs directories

   .. code-block:: shell-session

      mkdir -p /home/jobs/scripts/by-user/root/docker/adminer
      cd /home/jobs/scripts/by-user/root/docker/adminer

#. create a :download:`Docker compose file <includes/home/jobs/scripts/by-user/root/docker/adminer/docker-compose.yml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/docker/adminer/docker-compose.yml
      :language: yaml
      :linenos:
      :caption: /home/jobs/scripts/by-user/root/docker/adminer/docker-compose.yml

#. create a :download:`Systemd unit file <includes/home/jobs/services/by-user/root/docker-compose.adminer.service>`.
   See also the :ref:`Docker compose services <Docker compose services>` section

   .. literalinclude:: includes/home/jobs/services/by-user/root/docker-compose.adminer.service
      :language: ini
      :linenos:
      :caption: /home/jobs/services/by-user/root/docker-compose.adminer.service

#. fix the permissions

   .. code-block:: shell-session

      chmod 700 /home/jobs/scripts/by-user/root/docker/adminer
      chmod 700 -R /home/jobs/services/by-user/root

#. run the :ref:`deploy script <deploy script>`
#. serve the files via HTTP by creating a new
   :download:`Apache virtual host <includes/etc/apache2/adminer-docker.apache.conf>`.
   Replace ``FQDN`` with the appropriate domain and include this file from the
   Apache configuration

   .. literalinclude:: includes/etc/apache2/adminer-docker.apache.conf
      :language: apache
      :linenos:
      :caption: /etc/apache2/adminer-docker.apache.conf

#. restart Apache

   .. code-block:: shell-session

      systemctl restart apache2.service

Bare metal
----------

#. install the dependencies

   .. code-block:: shell

      apt-get install adminer php7.4-fpm

#. start and enable the php-fpm service

   .. code-block:: shell-session

      systemctl enable --now php7.4-fpm.service

#. enable the php7.4-fpm module for Apache

   .. code-block:: shell-session

      a2enmod php7.4

#. serve the files via HTTP by creating a new
   :download:`Apache virtual host <includes/etc/apache2/adminer-bare_metal.apache.conf>`.
   Replace ``FQDN`` with the appropriate domain and include this file from the
   Apache configuration

   .. literalinclude:: includes/etc/apache2/adminer-bare_metal.apache.conf
      :language: apache
      :linenos:
      :caption: /etc/apache2/adminer-bare_metal.apache.conf

#. restart Apache

   .. code-block:: shell-session

      systemctl restart apache2.service

.. rubric:: Footnotes

.. [#f1] https://github.com/vrana/adminer Apache License 2.0, Copyright (c)
         Adminer contributors
.. [#f2] https://github.com/TimWolla/docker-adminer Apache License 2.0,
         Copyright (c) docker-adminer contributors
.. [#f3] https://github.com/docker-library/official-images/ Apache License 2.0,
         Copyright (c) docker contributors
