Asterisk
========

.. seealso::

   - _`Asterisk and Fritzbox connection [SOLVED]` [#f1]_

.. rubric:: Footnotes

.. [#f1] https://community.asterisk.org/t/asterisk-and-fritzbox-connection-solved/79604
