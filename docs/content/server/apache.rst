Apache
======

Server monitoring
-----------------

By changing the log format you can have a better idea of what is happening
in real time to your web server without relying on external trackers.
You can install programs that are able to read Apache's logs in realtime.

.. seealso::

   - _`GoAccess - Visual Web Log Analyzer` [#f1]_
   - _`mod_log_config - Apache HTTP Server Version 2.4` [#f2]_
   - _`Apache2 Reverse Proxy + GoAccess How To Guide · Issue #1789 · allinurl/goaccess · GitHub` [#f3]_

#. install the dependencies

   .. code-block:: shell-session

      apt-get install goaccess

#. edit the log module setup in the main Apache configuration file. I added
   ``ServerName`` as variable and changed the date-time format to
   `RFC3339 <https://www.rfc-editor.org/rfc/rfc3339>`_.

   .. literalinclude:: includes/etc/apache2/apache2.conf
      :language: apache
      :lines: 1-
      :linenos:
      :caption: /etc/apache2/apache2.conf

#. serve the html file via HTTP by creating a new
   :download:`Apache virtual host <includes/etc/apache2/nextcloud.apache.conf>`.
   Replace ``FQDN`` with the appropriate domain and include this file from the
   Apache configuration. You may notice that the reverse proxy configuration
   uses ``wss`` (WebSocket Secure) instead of ``ws`` (WebSocket)

   .. literalinclude:: includes/etc/apache2/goaccess.apache.conf
      :language: apache
      :linenos:
      :caption: /etc/apache2/goaccess.apache.conf

#. create the jobs directories and files

   .. code-block:: shell-session

      mkdir -p /home/jobs/scripts/by-user/root/goaccess/goaccess.db
      chmod 700 /home/jobs/scripts/by-user/root/goaccess

#. run the program manually without the configuration to check if the supplied
   formats are working correctly. This command open a curses terminal. Press
   ``q`` to quit

   .. code-block:: shell-session

      goaccess /var/log/apache2/access.log \
      --log-format='%v %h %l %u %^[%d %t %^] \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"' \
      --date-format="%F" \
      --time-format="%T"

#. use this :download:`configuration file <includes/home/jobs/scripts/by-user/root/goaccess/goaccess.conf>`
   as example. You can find all the options in Debian's provided configuration file
   at ``/etc/goaccess/goaccess.conf``

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/goaccess/goaccess.conf
      :linenos:
      :caption: /home/jobs/scripts/by-user/root/goaccess/goaccess.conf

   .. note:: Replace ``FQDN`` with appropriate value.

#. create a :download:`Systemd unit file <includes/home/jobs/services/by-user/root/goaccess.service>`

   .. literalinclude:: includes/home/jobs/services/by-user/root/goaccess.service
      :language: ini
      :linenos:
      :caption: /home/jobs/services/by-user/root/goaccess.service

   .. note:: Replace ``FQDN`` with appropriate value.

#. run the :ref:`deploy script <deploy script>`

.. rubric:: Footnotes

.. [#f1] https://goaccess.io/ MIT License, Copyright (c) GoAccess contributors
.. [#f2] https://httpd.apache.org/docs/current/mod/mod_log_config.html Licensed under the Apache License, Version 2.0. Copyright 2022 The Apache Software Foundation.
.. [#f3] https://github.com/allinurl/goaccess/issues/1789 unknown license
