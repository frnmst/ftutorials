Kiwix
=====

With Kiwix it is possible to have a copy of Wikipedia and of other websites and
read them offline.

.. seealso::

   - _`Kiwix lets you access free knowledge – even offline` [#f1]_
   - _`A collection of scripts I have written and/or adapted that I currently use on my systems as automated tasks` [#f2]_
   - _`Base directory for downloading zim files` [#f3]_
   - _`Download large file in python with requests - Stack Overflow` [#f4]_
   - _`python - Performant replacement for IPython !shell-command magic - Stack Overflow` [#f5]_
   - _`shutil — High-level file operations — Python 3.10.3 documentation` [#f6]_

Setup
-----

#. install the dependencies

   .. code-block:: shell-session

      apt-get install python3-requests python3-bs4 python3-yaml aria2 kiwix-tools kiwix

#. install fpyutils. See :ref:`reference <installation of fpyutils>`
#. create a new user

   .. code-block:: shell-session

      useradd --system -s /bin/bash -U kiwix
      passwd kiwix
      usermod -aG jobs kiwix

#. create the jobs directories. See :ref:`reference <jobs instructions>`

   .. code-block:: shell-session

      mkdir -p /home/jobs/{scripts,services}/by-user/kiwix

#. create the :download:`script <includes/home/jobs/scripts/by-user/kiwix/kiwix_manage.py>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/kiwix/kiwix_manage.py
      :language: python
      :linenos:
      :caption: /home/jobs/scripts/by-user/kiwix/kiwix_manage.py

#. create a :download:`configuration file <includes/home/jobs/scripts/by-user/kiwix/kiwix_manage.yaml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/kiwix/kiwix_manage.yaml
      :language: yaml
      :linenos:
      :caption: /home/jobs/scripts/by-user/kiwix/kiwix_manage.yaml

#. create the data directory which must be accessible by the ``kiwix`` user

   .. code-block:: shell-session

      mkdir /data/WEBSERVER/kiwix
      chmod 700 /data/WEBSERVER/kiwix
      chown kiwix:kiwix /data/WEBSERVER/kiwix

#. use this :download:`Systemd service file <includes/home/jobs/services/by-user/kiwix/kiwix-manage.serve.service>` to serve the content

   .. literalinclude:: includes/home/jobs/services/by-user/kiwix/kiwix-manage.serve.service
      :language: ini
      :linenos:
      :caption: /home/jobs/services/by-user/kiwix/kiwix-manage.serve.service

#. use this :download:`Systemd service file <includes/home/jobs/services/by-user/kiwix/kiwix-manage.download.service>` to download the content

   .. literalinclude:: includes/home/jobs/services/by-user/kiwix/kiwix-manage.download.service
      :language: ini
      :linenos:
      :caption: /home/jobs/services/by-user/kiwix/kiwix-manage.download.service

#. fix the permissions

   .. code-block:: shell-session

      chown -R kiwix:kiwix /home/jobs/{scripts,services}/by-user/kiwix
      chmod 700 -R /home/jobs/{scripts,services}/by-user/kiwix

Download
--------

.. figure:: assets/kiwix_0.png
   :scale: 50 %

   Download status of a zim file

#. run the :ref:`deploy script <deploy script>`
#. start downloading the files

   .. code-block:: shell-session

      systemctl start kiwix-manage.download.service

#. wait for the files to be downloaded before going to the :ref:`serve <kiwix serve>` section

.. _kiwix serve:

Serve
-----

.. figure:: assets/kiwix_1.png
   :scale: 50 %

   Example of served files by Kiwix

#. run the :ref:`deploy script <deploy script>`
#. modify the reverse proxy port of your webserver configuration with ``8888``

.. important:: After downloading new files you must rerun this service

.. rubric:: Footnotes

.. [#f1] https://www.kiwix.org/en/ unknown license
.. [#f2] https://software.franco.net.eu.org/frnmst/automated-tasks GNU GPLv3+, copyright (c) 2019-2022, Franco Masotti
.. [#f3] https://download.kiwix.org/zim/ unknown license
.. [#f4] https://stackoverflow.com/a/39217788 CC BY-SA 4.0, copyright (c) 2016, 2021, John Zwinck, Daniel F (at stackoverflow.com)
.. [#f5] https://stackoverflow.com/a/53811881 CC BY-SA 4.0, copyright (c) 2018, Tom Hale (at stackoverflow.com)
.. [#f6] https://docs.python.org/3/library/shutil.html?highlight=shutil#shutil.rmtree.avoids_symlink_attacks Python Software Foundation License Version 2, © Copyright 2001-2022, Python Software Foundation
