Gotify
======

Gotify is a notification service with a web UI and an Android app.

Via docker-compose
------------------

#. follow the :ref:`Docker <Docker>` instructions
#. create the jobs directories

   .. code-block:: shell-session

       mkdir -p /home/jobs/scripts/by-user/root/docker/gotify
       chmod 700 /home/jobs/scripts/by-user/root/docker/gotify

#. create a :download:`Docker compose file <includes/home/jobs/scripts/by-user/root/docker/gotify/docker-compose.yml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/docker/gotify/docker-compose.yml
      :language: yaml
      :linenos:
      :caption: /home/jobs/scripts/by-user/root/docker/gotify/docker-compose.yml

#. create the data directory

   .. code-block:: shell-session

      mkdir /home/jobs/scripts/by-user/root/docker/gotify/data

#. if you are migrating from a non docker version of Gotify,
   copy the original files with ``rsync -avAX`` in
   ``/home/jobs/scripts/by-user/root/docker/gotify/data``.

   Include the Gotify database (``gotify.db``) as well

#. create a :download:`Systemd unit file <includes/home/jobs/services/by-user/root/docker-compose.gotify.service>`.
   See also the :ref:`Docker compose services <Docker compose services>` section

   .. literalinclude:: includes/home/jobs/services/by-user/root/docker-compose.gotify.service
      :language: ini
      :linenos:
      :caption: /home/jobs/services/by-user/root/docker-compose.gotify.service

#. fix the permissions

   .. code-block:: shell-session

      chmod 700 -R /home/jobs/services/by-user/root

#. run the :ref:`deploy script <deploy script>`
#. modify the reverse proxy port of your webserver configuration with ``4001``
