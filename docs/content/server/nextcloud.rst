Nextcloud
=========

All configurations presented here use docker-compose.
Read the :ref:`Docker <Docker>` instructions first.

.. warning:: The contents of this page have not been tested for all Nextcloud
             versions.

Nextcloud on Docker setups
--------------------------

Some variables are obvious while others needs an explanation

===================         ========================================================================================================
Variable name               Description
===================         ========================================================================================================
``DATA_PATH``               directory containing user files, Nextcloud logs and application data
``ES_DATA_PATH``            Elasticsearch data directory
``ES_PLUGINS_PATH``         Elasticsearch plugins directory
``FQDN``                    domain name
``HTML_DATA_PATH``          directory containing Nextcloud's HTML code and configurations. You can mount this directory on a SSD
``NC_USER``                 a username of the Nextcloud instance
``NC_PASSWORD``             a password corresponding to ``${NC_USER}``
===================         ========================================================================================================

MariaDB hosted on a Docker container
````````````````````````````````````

.. seealso::

   - _`Database configuration — Nextcloud latest Administration Manual latest documentation` [#f1]_
   - _`Deploy Nextcloud with docker-compose, Traefik 2, PostgreSQL and Redis | by ismail yenigül | FAUN Publication` [#f2]_
   - _`How to backup data from docker-compose MariaDB container using mysqldump - TechOverflow` [#f3]_
   - _`Issue with Federation and Shares - ℹ️ Support - Nextcloud community` [#f18]_

#. follow the :ref:`Docker <Docker>` instructions
#. create the jobs directories. See :ref:`reference <jobs instructions>`

   .. code-block:: shell-session

      mkdir -p /home/jobs/scripts/by-user/root/docker/nextcloud
      cd /home/jobs/scripts/by-user/root/docker/nextcloud

#. create a :download:`Docker compose file <includes/home/jobs/scripts/by-user/root/docker/nextcloud/docker-compose.yml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/docker/nextcloud/docker-compose.yml
      :language: yaml
      :lines: 4-53
      :caption: /home/jobs/scripts/by-user/root/docker/nextcloud/docker-compose.yml

   .. note:: Replace these variables with appropriate values

             - ``DATABASE_ROOT_PASSWORD``
             - ``DATABASE_PASSWORD``
             - ``DATA_PATH``
             - ``HTML_DATA_PATH``
             - ``FQDN``

#. create a :download:`Systemd unit file <includes/home/jobs/services/by-user/root/docker-compose.nextcloud.service>`.
   See also the :ref:`Docker compose services <Docker compose services>` section

   .. literalinclude:: includes/home/jobs/services/by-user/root/docker-compose.nextcloud.service
      :language: ini
      :caption: /home/jobs/services/by-user/root/docker-compose.nextcloud.service

#. fix the permissions

   .. code-block:: shell-session

      chmod 700 /home/jobs/scripts/by-user/root/docker/nextcloud
      chmod 700 -R /home/jobs/services/by-user/root

#. run the :ref:`deploy script <deploy script>`
#. modify the reverse proxy port of your webserver configuration with ``4005``

Migration from MariaDB on a container container to PostgreSQL in host
`````````````````````````````````````````````````````````````````````

#. check that your PostgreSQL setup is fully configured and running
#. add an entry in the ``pg_hba.conf`` file

   .. code-block:: ini
      :caption: /etc/postgresql/13/main/pg_hba.conf

      host    nextclouddb   nextcloud   172.16.0.0/16  scram-sha-256

#. change this setting in ``postgresql.conf``

   .. code-block:: ini
      :caption: /etc/postgresql/13/main/postgresql.conf

      listen_addresses = '*'

#. restart Postgres

   .. code-block:: shell-session

      systemctl restart postgresql.service

#. stop the Nextcloud container

   .. code-block:: shell-session

      systemctl stop docker-compose.nextcloud.service

#. convert the database type. You will be prompted for the database password

   .. code-block:: shell-session

      docker-compose exec --user www-data app php occ db:convert-type --all-apps pgsql ${DATABASE_USER} ${DATABASE_HOST} ${DATABASE_NAME}

   The Docker container must now comunicate with the PostgreSQL instance running on bare metal.
   The variables used here refer to the PostgreSQL instance.

#. use this :download:`Docker compose file <includes/home/jobs/scripts/by-user/root/docker/nextcloud/docker-compose.yml>` instead.
   See also the :ref:`Docker compose services <Docker compose services>` section

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/docker/nextcloud/docker-compose.yml
      :language: yaml
      :lines: 58-94
      :caption: /home/jobs/scripts/by-user/root/docker/nextcloud/docker-compose.yml

   .. note:: Replace ``DATABASE_NAME``, ``DATABASE_USER``, ``DATABASE_PASSWORD``, ``DATABASE_HOST``, ``DATA_PATH``, ``HTML_DATA_PATH`` and ``FQDN`` with appropriate values.
             ``DATABASE_HOST`` may be different from ``FQDN`` if PostgreSQL is not reachable through Internet or is running on a
             different machine.

#. run the :ref:`deploy script <deploy script>`

A more scalable setup
`````````````````````

To have a more serious setup you must separate some of the components. In this example
Nextcloud's FPM image, the NGINX webserver and ClamAV (optional) will be run in Docker.
Redis will accessed by Docker containers from the host machine.

.. seealso::

   - _`Access host socket from container - socat - IT Playground Blog` [#f6]_

#. follow the :ref:`Docker <Docker>` instructions
#. create the jobs directories. See :ref:`reference <jobs instructions>`

   .. code-block:: shell-session

      mkdir -p /home/jobs/scripts/by-user/root/docker/nextcloud
      cd /home/jobs/scripts/by-user/root/docker/nextcloud

#. create a :download:`Docker compose file <includes/home/jobs/scripts/by-user/root/docker/nextcloud/docker-compose.yml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/docker/nextcloud/docker-compose.yml
      :language: yaml
      :lines: 97-141
      :caption: /home/jobs/scripts/by-user/root/docker/nextcloud/docker-compose.yml

   .. note:: Replace ``DATABASE_ROOT_PASSWORD``, ``DATABASE_PASSWORD``, ``DATA_PATH``, ``HTML_DATA_PATH``,
             ``PHP_CONFIG_DATA_PATH`` and ``FQDN`` with appropriate values.

#. create a :download:`Systemd unit file <includes/home/jobs/services/by-user/root/docker-compose.nextcloud.service>`.
   See also the :ref:`Docker compose services <Docker compose services>` section

   .. literalinclude:: includes/home/jobs/services/by-user/root/docker-compose.nextcloud.service
      :language: ini
      :caption: /home/jobs/services/by-user/root/docker-compose.nextcloud.service

#. follow the :ref:`Redis <Redis>` instructions
#. create a copy of the Redis socket with specific permissions.
   Use this :download:`Systemd service <includes/home/jobs/services/by-user/root/redis-socket.service>`.

   .. literalinclude:: includes/home/jobs/services/by-user/root/redis-socket.service
      :language: ini
      :caption: /home/jobs/services/by-user/root/redis-socket.service

   The Redis docket must have the same user id (in this case ``33``) of the
   nextcloud files in the Docker image. To get the ids run this command

   .. code-block:: shell-session

      docker-compose exec -T --user www-data app id

   You should get something like

   .. code-block::

      uid=33(www-data) gid=33(www-data) groups=33(www-data)

#. copy the php data from the image to the ``PHP_CONFIG_DATA_PATH``. First of all comment the ``PHP_CONFIG_DATA_PATH``
   volume from the docker-compose file then run

   .. code-block:: shell-session

      docker-compose up --remove orphans
      docker cp $(docker container ls | grep nextcloud:24.0.5-fpm | awk '{print $1}'):/usr/local/etc/php/conf.d "${PHP_CONFIG_DATA_PATH}"
      docker-compose down

#. use this :download:`NGINX configuration file <includes/home/jobs/scripts/by-user/root/docker/nextcloud/nginx.conf>` as example.

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/docker/nextcloud/nginx.conf
      :language: nginx
      :caption: /home/jobs/scripts/by-user/root/docker/nextcloud/nginx.conf

   .. note:: Replace ``FQDN`` with appropriate value.

#. edit the redis configuration in the PHP config (``${HTML_DATA_PATH}/config/config.php``) like this

   .. code-block:: php

      # [ ... ]

      'memcache.distributed' => '\\OC\\Memcache\\Redis',
      'memcache.locking' => '\\OC\\Memcache\\Redis',
      'redis' =>
      array (
        'host' => '/var/run/redis.sock',
        'password' => '',
      ),

      # [ ... ]

#. run the :ref:`deploy script <deploy script>`
#. modify the reverse proxy port of your webserver configuration with ``4005``
#. connect to your Nextcloud instance as admin and configure the antivirus app from the settings.
   Go to Settings -> Security -> Antivirus for Files

   - Mode: ``ClamAV Daemon``
   - Host: ``clamav``
   - Port: ``3310``
   - When infected files are found during a background scan: ``Delete file``

Video
~~~~~

- `Redis cache in Nextcloud <https://www.youtube.com/watch?v=MRr1cl3Ijgo>`_

Apache configuration
--------------------

Clearnet
````````

#. serve the files via HTTP by creating a new :download:`Apache virtual host <includes/etc/apache2/nextcloud.apache.conf>`.
   Replace ``FQDN`` with the appropriate domain and include this file from the
   Apache configuration

   .. literalinclude:: includes/etc/apache2/nextcloud.apache.conf
      :language: apache
      :caption: /etc/apache2/nextcloud.apache.conf

TOR
```
.. seealso::

   - _`How to conditionally modify Apache response header 'Location' - Stack Overflow` [#f34]_
   - _`How to conditionally modify Apache response header 'Location' - Stack Overflow - comment99361231_25158718` [#f35]_

#. install TOR

   .. code-block:: shell-session

      apt-get install tor

#. create a new onion service. See the ``/etc/tor/torrc`` file
#. create a new variable in the main Apache configuration file, usually
   ``/etc/apache2/apache2.conf``

   .. code-block:: apache

      Define NEXTCLOUD_DOMAIN your.FQDN.domain.org

#. similarly to the clearnet version, create a new :download:`Apache virtual host <includes/etc/apache2/nextcloud_tor.apache.conf>`.
   Replace the TOR service URL with the one you want to use

   .. literalinclude:: includes/etc/apache2/nextcloud_tor.apache.conf
      :language: apache
      :caption: /etc/apache2/nextcloud_tor.apache.conf

#. include the virtual host file from the mail Apache configuration file:

   .. code-block:: apache

      Include /etc/apache2/nextcloud_tor.conf

Apps
----

Tips and configurations for some Nextcloud apps

Collabora (cloud office suite)
``````````````````````````````

To make Collabora interact with Nextcloud without errors you need to deploy
a new virtual server in Apache. Collabora must work with the same security
protocol as the Nextcloud instance. In all these examples you see that Nextcloud
is deployed in SSL mode so Collabora must be in SSL mode as well.

Collabora is deployed as a Docker container dependency of Nextcloud.

.. seealso::

   - _`Installation example with Docker — Nextcloud latest Administration Manual latest documentation` [#f15]_
   - _`Nextcloud & Collabora docker installation. Error: Collabora Online should use the same protocol as the server installation - 🚧 Installation - Nextcloud community` [#f16]_
   - _`Socket Error when accessing Collabora - ℹ️ Support / 📄 Collabora - Nextcloud community` [#f17]_
   - _`Can't connect to OnlyOffice document server after update to NC19 - ℹ️ Support - Nextcloud community` [#f30]_

#. follow the :ref:`Docker <Docker>` instructions
#. edit the Nextcloud docker-compose file by adding the :download:`Collabora service <includes/home/jobs/scripts/by-user/root/docker/nextcloud/docker-compose.yml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/docker/nextcloud/docker-compose.yml
      :language: yaml
      :lines: 213-
      :caption: /home/jobs/scripts/by-user/root/docker/nextcloud/docker-compose.yml

   .. note:: Replace ``NEXTCLOUD_FQDN`` and ``COLLABORA_FQDN`` with appropriate values.

#. Add ``collabora`` to the ``links`` and ``depends_on`` sections of the
   Nextcloud service.
#. serve Collabora via HTTPS by creating a new
   :download:`Apache virtual host <includes/etc/apache2/collabora.apache.conf>`.
   Replace ``COLLABORA_FQDN`` with the appropriate domain and include this
   file from the Apache configuration

   .. literalinclude:: includes/etc/apache2/collabora.apache.conf
      :language: apache
      :caption: /etc/apache2/collabora.apache.conf

#. restart the Nextcloud container and its dependencies with docker-compose
#. restart Apache

   .. code-block:: shell-session

      systemctl restart apache2

#. install the Nexcloud Office app from the store.
   Go to the add apps admin menu, then to the ``Search`` section

   - `Nextcloud Office <https://apps.nextcloud.com/apps/richdocuments>`_

#. configure the search from the Nextcloud admin settings.
   Go to the ``Office`` tab and use these settings

   - ``Use your own server``
   - ``URL (and Port) of Collabora Online-server``: ``${COLLABORA_FQDN}``

   click on ``Save``

#. create a new office document or open an existing one to check
   if everything is working

Nextcloud calendar to PDF
`````````````````````````

Export a Nextcloud calendar to a PDF file. This is not an app you install from
the Nextcloud store but an external script which interacts with the Nextcloud
API

.. graphviz::

   strict digraph content {
      rankdir=LR;

      nc                [label="Nextcloud (HTTP API)", shape="rectangle"];
      ical              [label="iCalendar", shape="rectangle"];
      html              [label="HTML", shape="rectangle"];
      pdf               [label="PDF", shape="rectangle"];

      nc -> ical        [label="requests"];
      ical -> html      [label="ical2html"];
      html -> pdf       [label="Weasyprint"];
   }

.. seealso::

   - _`Index of /Tools/Ical2html` [#f11]_

#. install the dependencies

   .. code-block:: shell-session

      apt-get install python3-venv ical2html

#. create the jobs directories

   .. code-block:: shell-session

      mkdir -p /home/jobs/scripts/by-user/myuser/nextcloud_calendar_to_pdf
      cd /home/jobs/scripts/by-user/myuser/nextcloud_calendar_to_pdf

#. create a new Nextcloud user specifically for this purpose and share
   the calendar with it
#. connect to your Nextcloud instance using the new user and go to the calendar
   app
#. get the calendar name by opening the calendar options an click on
   ``Copy private link``
#. create the :download:`script <includes/home/jobs/scripts/by-user/myuser/nextcloud_calendar_to_pdf/nextcloud_calendar_to_pdf.py>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/myuser/nextcloud_calendar_to_pdf/nextcloud_calendar_to_pdf.py
      :language: python
      :caption: includes/home/jobs/scripts/by-user/myuser/nextcloud_calendar_to_pdf/nextcloud_calendar_to_pdf.py

#. create the :download:`configuration <includes/home/jobs/scripts/by-user/myuser/nextcloud_calendar_to_pdf/nextcloud_calendar_to_pdf.yaml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/myuser/nextcloud_calendar_to_pdf/nextcloud_calendar_to_pdf.yaml
      :language: yaml
      :caption: /home/jobs/scripts/by-user/myuser/nextcloud_calendar_to_pdf/nextcloud_calendar_to_pdf.yaml

#. create a :download:`launcher <includes/home/jobs/scripts/by-user/myuser/nextcloud_calendar_to_pdf_launcher.sh>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/myuser/nextcloud_calendar_to_pdf_launcher.sh
      :language: shell
      :caption: includes/home/jobs/scripts/by-user/myuser/nextcloud_calendar_to_pdf_laucher.sh

#. create the :download:`requirements.txt <includes/home/jobs/scripts/by-user/myuser/nextcloud_calendar_to_pdf/requirements.txt>` file

   .. literalinclude:: includes/home/jobs/scripts/by-user/myuser/nextcloud_calendar_to_pdf/requirements.txt
      :language: text
      :caption: includes/home/jobs/scripts/by-user/myuser/nextcloud_calendar_to_pdf/requirements.txt

#. fix the permissions

   .. code-block:: shell-session

      chmod 700 /home/jobs/scripts/by-user/myuser
      chmod 700 -R /home/jobs/services/by-user/myuser

#. Run the script

   .. code-block:: shell-session

      /home/jobs/scripts/by-user/myuser/nextcloud_calendar_to_pdf/nextcloud_calendar_to_pdf.sh

Nextcloud news to epub
``````````````````````

#. install the dependencies

   .. code-block:: shell-session

      apt-get install pandoc

#. create the :download:`script <includes/home/jobs/scripts/by-user/myuser/nextcloud_news_to_epub/nc_news_to_epub.py>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/myuser/nextcloud_news_to_epub/nc_news_to_epub.py
      :language: python
      :caption: includes/home/jobs/scripts/by-user/myuser/nextcloud_news_to_epub/nc_news_to_epub.py

#. create the :download:`configuration <includes/home/jobs/scripts/by-user/myuser/nextcloud_news_to_epub/nc_news_to_epub.yaml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/myuser/nextcloud_news_to_epub/nc_news_to_epub.yaml
      :language: yaml
      :caption: includes/home/jobs/scripts/by-user/myuser/nextcloud_news_to_epub/nc_news_to_epub.yaml

#. create the :download:`Makefile <includes/home/jobs/scripts/by-user/myuser/nextcloud_news_to_epub/Makefile>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/myuser/nextcloud_news_to_epub/Makefile
      :language: makefile
      :caption: includes/home/jobs/scripts/by-user/myuser/nextcloud_news_to_epub/Makefile

#. create the :download:`requirements.txt <includes/home/jobs/scripts/by-user/myuser/nextcloud_news_to_epub/requirements.txt>` and
   :download:`requirements-dev.txt <includes/home/jobs/scripts/by-user/myuser/nextcloud_news_to_epub/requirements-dev.txt>` files

   .. literalinclude:: includes/home/jobs/scripts/by-user/myuser/nextcloud_news_to_epub/requirements.txt
      :language: text
      :caption: includes/home/jobs/scripts/by-user/myuser/nextcloud_news_to_epub/requirements.txt

   .. literalinclude:: includes/home/jobs/scripts/by-user/myuser/nextcloud_news_to_epub/requirements-dev.txt
      :language: text
      :caption: includes/home/jobs/scripts/by-user/myuser/nextcloud_news_to_epub/requirements-dev.txt

#. fix the permissions

   .. code-block:: shell-session

      chmod 700 /home/jobs/scripts/by-user/myuser
      chmod 700 -R /home/jobs/services/by-user/myuser

#. install the virtual environment

   .. code-block:: shell-session

      make regenerate-freeze
      make install-dev

#. Run the script

   .. code-block:: shell-session

      make run

Full text search with Elasticsearch
```````````````````````````````````

These apps enable you to search text through all files (no OCR).

.. seealso::

   - _`Docker-compose fails with ingest-attachment - Elastic Stack / Elasticsearch - Discuss the Elastic Stack` [#f7]_
   - _`How to Run Elasticsearch 8 on Docker for Local Development | by Lynn Kwong | Level Up Coding` [#f8]_
   - _`ingest-attachment and node alive problem · Issue #42 · nextcloud/fulltextsearch_elasticsearch · GitHub` [#f9]_
   - _`elasticsearch - Recommended RAM ratios for ELK with docker-compose - Stack Overflow` [#f10]_
   - _`Plugin was built for Elasticsearch version 7.7.0 but version 7.7.1 is runnin · Issue #10 · opendatasoft/elasticsearch-aggregation-geoclustering · GitHub` [#f33]_

#. install these three apps in Nextcloud. Go to the add apps admin menu,
   then to the ``Search`` section

   - `Full text search <https://apps.nextcloud.com/apps/fulltextsearch>`_
   - `Full text search - Elasticsearch Platform <https://apps.nextcloud.com/apps/fulltextsearch_elasticsearch>`_
   - `Full text search - Files <https://apps.nextcloud.com/apps/files_fulltextsearch>`_

#. configure the search from the Nextcloud admin settings.
   Go to the ``Full text search`` tab and use these settings:

   .. figure:: assets/nextcloud_admin_elasticsearch_settings.png
      :scale: 50 %

      Nextcloud administration search settings

#. add this content to the docker compose file

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/docker/nextcloud/docker-compose.yml
      :language: yaml
      :lines: 149-212
      :caption: /home/jobs/scripts/by-user/root/docker/nextcloud/docker-compose.yml

   .. note:: Replace ``DATABASE_NAME``, ``DATABASE_USER``,
             ``DATABASE_PASSWORD``, ``DATABASE_HOST``, ``DATA_PATH``,
             ``HTML_DATA_PATH``, ``FQDN``, ``ES_DATA_PATH``
             and ``ES_PLUGINS_PATH`` with appropriate values.

   .. important:: At the moment only version 8 of Elasticsearch is supported as
                  stated in the GitHub development page:

                      As of [Nextcloud] version 26.0.0 this app is only compatible with Elasticsearch 8

#. go to the Nextcloud docker directory and run the app

   .. code-block:: shell

      pushd /home/jobs/scripts/by-user/root/docker/nextcloud
      docker-compose up --remove-orphans

#. install the attachment plugin

   .. code-block:: shell

      /usr/bin/docker-compose exec -T elasticsearch /usr/share/elasticsearch/bin/elasticsearch-plugin install ingest-attachment

#. check the user id and group id of the data directory in the elasticsearch container

   .. code-block:: shell

      /usr/bin/docker-compose exec -T elasticsearch id elasticsearch
      /usr/bin/docker-compose exec -T elasticsearch ls -ld /usr/share/elasticsearch/data

#. stop the app

   .. code-block:: shell

      docker-compose down

#. change the ownership of the docker volume to the ones resulting from the
   previous command. In my case the owner user is ``1000`` while the owner
   group is ``root``

   .. code-block:: shell

      chown 1000:root esdata

#. enable the commented volume, ``ES_DATA_PATH``, from the docker-compose file
#. create a :download:`Systemd unit file <includes/home/jobs/services/by-user/root/docker-compose.nextcloud.service>`.
   See also the :ref:`Docker compose services <Docker compose services>` section

   .. literalinclude:: includes/home/jobs/services/by-user/root/docker-compose.nextcloud.service
      :language: ini
      :caption: /home/jobs/services/by-user/root/docker-compose.nextcloud.service

#. run the :ref:`deploy script <deploy script>`
#. test if the interaction between the Nextcloud container and Elasticsearch
   is working. The last command does the actual indexing and it may take a
   long time to complete

   .. code-block:: shell

      /usr/bin/docker-compose exec -T --user www-data app php occ fulltextsearch:test
      /usr/bin/docker-compose exec -T --user www-data app php occ fulltextsearch:check
      /usr/bin/docker-compose exec -T --user www-data app php occ fulltextsearch:index

#. create a :download:`Systemd unit file <includes/home/jobs/services/by-user/root/nextcloud-elasticsearch.service>`

   .. literalinclude:: includes/home/jobs/services/by-user/root/nextcloud-elasticsearch.service
      :language: ini
      :caption: /home/jobs/services/by-user/root/nextcloud-elasticsearch.service

#. create a :download:`Systemd timer unit file <includes/home/jobs/services/by-user/root/nextcloud-elasticsearch.timer>`

   .. literalinclude:: includes/home/jobs/services/by-user/root/nextcloud-elasticsearch.timer
      :language: ini
      :caption: /home/jobs/services/by-user/root/nextcloud-elasticsearch.timer

#. run the :ref:`deploy script <deploy script>`

.. important:: When you update to a new Elasticsearch minor version, for
               example from version 7.17.7 to 7.17.8, you must recreate the index like this

               .. code-block:: shell

                  systemctl stop docker-compose.nextcloud.service
                  /usr/bin/docker-compose run --rm elasticsearch /usr/share/elasticsearch/bin/elasticsearch-plugin remove ingest-attachment
                  /usr/bin/docker-compose run --rm elasticsearch /usr/share/elasticsearch/bin/elasticsearch-plugin install ingest-attachment
                  systemctl restart docker-compose.nextcloud.service

.. important:: When you upgrade to a new Elasticsearch major version, for
               example from version 7 to 8, you must:

               1. stop the Elasticsearch container
               2. remove the existing data in the Elasticsearch volume
               3. repeat the Elasticsearch instructions from the start, i.e.:
                  re-test and re-index everything.


A.I. integration
````````````````

Instead of using LocalAI as self-hosted A.I. system, we are going to use
Ollama. See the :doc:`open-webui` page.


Video
~~~~~

- `Full text search in Nextcloud <https://www.youtube.com/watch?v=yPZkrzgue5c>`_

Talk (a.k.a. Spreed)
````````````````````

You can use metered.ca OpenRelay's STUN and TURN servers to improve communication
between WebRTC P2P clients.

.. seealso::

   - _`Free WebRTC TURN Server - Open Relay Project | Open Relay Project - Free WebRTC TURN Server` [#f5]_

Services
--------

Once you have your Nextcloud container running you may want to integrate these
services as well

Required
````````

Crontab
~~~~~~~

.. seealso::

   - _`Background jobs — Nextcloud latest Administration Manual latest documentation` [#f4]_

#. create another :download:`Systemd unit file <includes/home/jobs/services/by-user/root/nextcloud-cron.service>` that will run tasks periodically

   .. literalinclude:: includes/home/jobs/services/by-user/root/nextcloud-cron.service
      :language: ini
      :caption: /home/jobs/services/by-user/root/nextcloud-cron.service

#. create a :download:`Systemd timer file <includes/home/jobs/services/by-user/root/nextcloud-cron.timer>`

   .. literalinclude:: includes/home/jobs/services/by-user/root/nextcloud-cron.timer
      :language: ini
      :caption: /home/jobs/services/by-user/root/nextcloud-cron.timer

#. run the :ref:`deploy script <deploy script>`

Optional
````````

File scanner
~~~~~~~~~~~~

#. create another :download:`Systemd unit file <includes/home/jobs/services/by-user/root/scan-files-nextcloud.service>`
   that will enable nextcloud to periodically scan for new files if
   added by external sources

   .. literalinclude:: includes/home/jobs/services/by-user/root/scan-files-nextcloud.service
      :language: ini
      :caption: /home/jobs/services/by-user/root/scan-files-nextcloud.service

#. create a :download:`Systemd timer file <includes/home/jobs/services/by-user/root/scan-files-nextcloud.timer>`

   .. literalinclude:: includes/home/jobs/services/by-user/root/scan-files-nextcloud.timer
      :language: ini
      :caption: /home/jobs/services/by-user/root/scan-files-nextcloud.timer

#. run the :ref:`deploy script <deploy script>`

Image previews
~~~~~~~~~~~~~~

To have faster image load time you can use the image preview app.

.. seealso::

   - _`Better Nextcloud Photo Albums with Preview Generator and Exiftool - Ray Against the Machine` [#f12]_
   - _`GitHub - nextcloud/previewgenerator: Nextcloud app to do preview generation` [#f13]_
   - _`Could not create folder · Issue #289 · nextcloud/previewgenerator · GitHub` [#f14]_

#. install the apps in Nextcloud. Go to the add apps admin menu,
   then to the ``Search`` section

   - `Preview Generator <https://apps.nextcloud.com/apps/previewgenerator>`_

#. Append these options to the main configuration file

   .. code-block:: php
      :caption: ${HTML_DATA_PATH}/config/config.php

      'preview_max_x' => '4096',
      'preview_max_y' => '4096',
      'jpeg_quality' => '40',

#. restart nextcloud

   .. code-block:: shell-session

      systemctl restart docker-compose.nextcloud.service

#. go to the docker-compose directory and run these commands
   to set the preview sizes

   .. code-block:: shell-session

      cd /home/jobs/scripts/by-user/root/docker/nextcloud
      docker-compose exec -T --user www-data app php occ config:app:set --value="32 128" previewgenerator squareSizes
      docker-compose exec -T --user www-data app php occ config:app:set --value="128 256" previewgenerator widthSizes
      docker-compose exec -T --user www-data app php occ config:app:set --value="128" previewgenerator heightSizes

#. generate the previews and wait

   .. code-block:: shell-session

      docker-compose exec -T --user www-data app php occ preview:generate-all -vvv

   .. note:: You may get an error such as ``Could not create folder``.
             Retry scanning manually later untill it works.

#. create a :download:`Systemd unit file <includes/home/jobs/services/by-user/root/nextcloud-preview.service>`

   .. literalinclude:: includes/home/jobs/services/by-user/root/nextcloud-preview.service
      :language: ini
      :caption: /home/jobs/services/by-user/root/nextcloud-preview.service

#. create a :download:`Systemd timer unit file <includes/home/jobs/services/by-user/root/nextcloud-preview.timer>`

   .. literalinclude:: includes/home/jobs/services/by-user/root/nextcloud-preview.timer
      :language: ini
      :caption: /home/jobs/services/by-user/root/nextcloud-preview.timer

#. run the :ref:`deploy script <deploy script>`

.. note:: If you need to regenerate or delete the previews you must follow the
          steps reported in the repository's readme [#f13]_.

Export
~~~~~~

You can export all calendars, contacts and news sources using this script.
This is useful if your do not have the possibility to make a full backup
and in case something goes wrong during an update to your Nextcloud instance.

#. install the dependencies

   .. code-block:: shell-session

      apt-get install python3-yaml

#. create a new user

   .. code-block:: shell-session

      useradd --system -s /bin/bash -U nextcloud
      passwd nextcloud
      usermod -aG jobs nextcloud

#. create the jobs directories. See :ref:`reference <jobs instructions>`

   .. code-block:: shell-session

      mkdir -p /home/jobs/{scripts,services}/by-user/nextcloud

#. create the :download:`script <includes/home/jobs/scripts/by-user/nextcloud/nextcloud_export.py>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/nextcloud/nextcloud_export.py
      :language: python
      :caption: /home/jobs/scripts/by-user/nextcloud/nextcloud_export.py

#. create a :download:`configuration file <includes/home/jobs/scripts/by-user/nextcloud/nextcloud_export.yaml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/nextcloud/nextcloud_export.yaml
      :language: yaml
      :caption: /home/jobs/scripts/by-user/nextcloud/nextcloud_export.yaml

#. create the data directory which must be accessible by the ``nextcloud`` user

   .. code-block:: shell-session

      mkdir /data/NEXTCLOUD/export
      chmod 700 /data/NEXTCLOUD/export
      chown nextcloud:nextcloud /data/NEXTCLOUD/export

#. use this :download:`Systemd service file <includes/home/jobs/services/by-user/nextcloud/nextcloud-export.service>`

   .. literalinclude:: includes/home/jobs/services/by-user/nextcloud/nextcloud-export.service
      :language: ini
      :caption: /home/jobs/services/by-user/nextcloud/nextcloud-export.service

#. use this :download:`Systemd timer file <includes/home/jobs/services/by-user/nextcloud/nextcloud-export.timer>`

   .. literalinclude:: includes/home/jobs/services/by-user/nextcloud/nextcloud-export.timer
      :language: ini
      :caption: /home/jobs/services/by-user/nextcloud/nextcloud-export.timer

#. fix the permissions

   .. code-block:: shell-session

      chown -R nextcloud:nextcloud /home/jobs/{scripts,services}/by-user/nextcloud
      chmod 700 -R /home/jobs/{scripts,services}/by-user/nextcloud

Video
.....

- `Export Nextcloud calendars, contacts and news <https://www.youtube.com/watch?v=A05dPaKc438>`_

Other notes
-----------

PhoneTrack app
``````````````
.. seealso::

   - _`Fix for GPS rollover bug #252 (!934) · Merge requests · Julien Veyssier / phonetrack-oc · GitLab` [#f26]_ [#f27]_

Some mobile phones OSs have a bug in interpeting the correct date
sent from GPS satellites. This problem is called *GPS week number rollover*
and sets the date, but not the time, 1024 weeks in the past.

Applications like *PhoneTrack* should compensate for this problem.
A fix was proposed but never merged in the code.

You patach the code manually in
``${HTML_DATA_PATH}/custom_apps/phonetrack/lib/Controller/LogController.php``
and then restart your Nextcloud instance.

.. important:: When the PhoneTrack app updates you need to remember to repeat
               this procedure.


Recognize app
`````````````

Important information before using the *Recognize for Nextcloud* app.

.. seealso::

   - _`GitHub - nextcloud/recognize: Smart media tagging for Nextcloud: recognizes faces, objects, landscapes, music genres` [#f19]_
   - _`Error when deleting file or folder - General - Nextcloud community` [#f20]_
   - _`[Bug]: Call to undefined method OCP\Files\Events\Node\NodeDeletedEvent::getSubject() on deltetion of files · Issue #33865 · nextcloud/server · GitHub` [#f21]_
   - _`[3.2.2] Background scan blocks Nextcloud CRON php job indefinetly with 100% CPU load · Issue #505 · nextcloud/recognize · GitHub` [#f22]_

Video
~~~~~

- `Nextcloud's Recognize app <https://www.youtube.com/watch?v=AP3YXkvGw_U>`_

Memories app
````````````

Once the *Memories* app is installed or when you added external files you must run a
reindexing operation

.. seealso::

   - _`GitHub - pulsejet/memories: Fast, modern and advanced photo management suite. Runs as a Nextcloud app.` [#f23]_

.. code-block:: shell-session

   docker-compose exec -T --user www-data app php occ memories:index

News app
````````

.. seealso::

  - _`How can I remove all the rss feeds from the news app? - 🍱 Features & apps / news - Nextcloud community` [#f31]_
  - _`Rss News Reader. Cleanup before new Content - 🍱 Features & apps / news - Nextcloud community` [#f32]_

Improve performance
```````````````````

Opcache
~~~~~~~

You can improve caching by changing options in the ``opcache-recommended.ini`` file.

.. seealso::

   - _`PHP: Introduction - Manual` [#f24]_
   - _`PHP: Runtime Configuration - Manual` [#f25]_
   - _`Security & setup warnings > Which php.ini to edit? · Issue #5044 · nextcloud/server · GitHub` [#f28]_

#. create the directory

   .. code-block:: shell-session

      mkdir /home/jobs/scripts/by-user/root/docker/nextcloud/phpcfg
      pushd /home/jobs/scripts/by-user/root/docker/nextcloud/phpcfg

#. create a :download:`configuration file <includes/home/jobs/scripts/by-user/root/docker/nextcloud/phpcfg/opcache-recommended.ini>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/docker/nextcloud/phpcfg/opcache-recommended.ini
      :language: ini
      :caption: /home/jobs/scripts/by-user/root/docker/nextcloud/phpcfg/opcache-recommended.ini

#. add the docker volume in the docker-compose file

   .. code-block:: yaml
      :caption: /home/jobs/scripts/by-user/root/nextcloud/docker-compose.yml

      # [ ... ]
      volumes:
        # [ ... ]
        - /home/jobs/scripts/by-user/root/docker/nextcloud/phpcfg/opcache-recommended.ini:/usr/local/etc/php/conf.d/opcache-recommended.ini
        # [ ... ]
      # [ ... ]

#. run the :ref:`deploy script <deploy script>`

Transactional file locking
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. seealso::

   - _`Transactional file locking — Nextcloud latest Administration Manual latest documentation` [#f29]_

.. rubric:: Footnotes

.. [#f1] https://docs.nextcloud.com/server/20/admin_manual/configuration_database/linux_database_configuration.html CC BY 3.0, Copyright (c) Nextcloud contributors
.. [#f2] https://faun.pub/deploy-nextcloud-with-docker-compose-traefik-2-postgresql-and-redis-fd1ffc166173 unknown license
.. [#f3] https://techoverflow.net/2020/12/01/how-to-backup-data-from-docker-compose-mariadb-container-using-mysqldump/ unknown license
.. [#f4] https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/background_jobs_configuration.html CC-BY 3.0, Copyright (c) Nextcloud contributors
.. [#f5] https://www.metered.ca/tools/openrelay/#turn-server-for-nextcloud-talk unknown license
.. [#f6] https://blog.it-playground.eu/accessing-host-socket-when-using-namespaces-for-docker-isolation/ unknown license
.. [#f7] https://discuss.elastic.co/t/docker-compose-fails-with-ingest-attachment/173598/5 unknown license
.. [#f8] https://levelup.gitconnected.com/how-to-run-elasticsearch-8-on-docker-for-local-development-401fd3fff829 unknown license
.. [#f9] https://github.com/nextcloud/fulltextsearch_elasticsearch/issues/42 unknown license
.. [#f10] https://stackoverflow.com/questions/51541323/recommended-ram-ratios-for-elk-with-docker-compose CC BY-SA 4.0, Copyright (c) 2018 Stackoverflow contributors
.. [#f11] https://www.w3.org/Tools/Ical2html/ W3C Software Notice and License, Copyright (c) Ical2html contributors
.. [#f12] https://rayagainstthemachine.net/linux%20administration/nextcloud-photos/ unknown license
.. [#f13] https://github.com/nextcloud/previewgenerator GNU AGPLv3, Copyright (c) Preview Generator contributors
.. [#f14] https://github.com/nextcloud/previewgenerator/issues/289 GNU AGPLv3, Copyright (c) Preview Generator contributors
.. [#f15] https://docs.nextcloud.com/server/24/admin_manual/office/example-docker.html CC-BY 3.0, Nextcloud contributors
.. [#f16] https://help.nextcloud.com/t/nextcloud-collabora-docker-installation-error-collabora-online-should-use-the-same-protocol-as-the-server-installation/51582 unknown license
.. [#f17] https://help.nextcloud.com/t/socket-error-when-accessing-collabora/22486 unknown license
.. [#f18] https://help.nextcloud.com/t/issue-with-federation-and-shares/120917/3 unknown license
.. [#f19] https://github.com/nextcloud/recognize
.. [#f20] https://help.nextcloud.com/t/error-when-deleting-file-or-folder/34948 unknown license
.. [#f21] https://github.com/nextcloud/server/issues/33865 unknown license
.. [#f22] https://github.com/nextcloud/recognize/issues/505 unknown license
.. [#f23] https://github.com/pulsejet/memories AGPL-3.0 license, Copyright (c) memories contributors
.. [#f24] https://www.php.net/manual/en/intro.opcache.php unknown license
.. [#f25] https://www.php.net/manual/en/opcache.configuration.php unknown license
.. [#f26] https://gitlab.com/eneiluj/phonetrack-oc/-/merge_requests/934 unknown license
.. [#f27] https://gitlab.com/eneiluj/phonetrack-oc/-/merge_requests/934/diffs unknown license
.. [#f28] https://github.com/nextcloud/server/issues/5044 unknown license
.. [#f29] https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/files_locking_transactional.html CC-BY 3.0, Nextcloud contributors
.. [#f30] https://help.nextcloud.com/t/cant-connect-to-onlyoffice-document-server-after-update-to-nc19/83958 unknown license
.. [#f31] https://help.nextcloud.com/t/how-can-i-remove-all-the-rss-feeds-from-the-news-app/118340/4 unknown license
.. [#f32] https://help.nextcloud.com/t/rss-news-reader-cleanup-before-new-content/33623 unknown license
.. [#f33] https://github.com/opendatasoft/elasticsearch-aggregation-geoclustering/issues/10#issuecomment-1044899338 unknown license
.. [#f34] https://stackoverflow.com/questions/25158443/how-to-conditionally-modify-apache-response-header-location CC-BY-SA 3.0, Stack OVerflow contributors
.. [#f35] https://stackoverflow.com/questions/25158443/how-to-conditionally-modify-apache-response-header-location#comment99361231_25158718 unknown license
