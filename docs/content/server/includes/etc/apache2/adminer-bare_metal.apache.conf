###########
# Adminer #
###########
<IfModule mod_ssl.c>
<VirtualHost *:80>
    ServerName ${FQND}

    # Force https.
    UseCanonicalName on
    RewriteEngine on
    RewriteCond %{SERVER_NAME} =${FQDN}

    # Ignore rewrite rules for 127.0.0.1
    RewriteCond %{HTTP_HOST} !=127.0.0.1
    RewriteCond %{REMOTE_ADDR} !=127.0.0.1

    RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>
</IfModule>

<IfModule mod_ssl.c>
<VirtualHost *:443>
    UseCanonicalName on

    Keepalive On
    RewriteEngine on

    ServerName ${FQDN}

    SSLCompression off

    Include conf-enabled/php7.4-fpm.conf

    DocumentRoot "/usr/share/adminer/adminer/"
    <Directory "/usr/share/adminer/adminer/">
       AllowOverride All
       Options FollowSymlinks
       Require all granted

       # You can restrict access to Adminer to LAN
       # or Docker containers only by adding this.

       # Localhost.
       Require ip 127.0.0.1

       # LAN.
       Require ip 192.168.0.

       # Docker containers.
       Require ip 172.16.
   </Directory>

    Include /etc/letsencrypt/options-ssl-apache.conf
    SSLCertificateFile /etc/letsencrypt/live/${FQDN}/fullchain.pem
    SSLCertificateKeyFile /etc/letsencrypt/live/${FQDN}/privkey.pem
</VirtualHost>
</IfModule>
