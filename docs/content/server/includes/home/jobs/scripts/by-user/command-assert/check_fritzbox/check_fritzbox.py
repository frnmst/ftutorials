#!/usr/bin/env python3
#
# check_fritzbox.py
#
# Copyright (C) 2025 Franco Masotti
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import logging
import pathlib
import shlex
import sys
import time

import apprise
import yaml
from apprise.exception import AppriseException
from fritzconnection import FritzConnection

logging.basicConfig(level=logging.DEBUG)


def notify(config: dict, error_type: str, variable):
    done: bool = False
    error_counter: int = 0
    while not done:
        try:
            body: str = f'{error_type} error, new value: {variable}'
            logging.error(body)
            apobj.notify(
                body=body,
                title=config['apprise']['title'],
                notify_type=apprise.NotifyType.FAILURE,
            )
            done = True
        except AppriseException:
            error_counter += 1
            if error_counter >= config['apprise']['attempts']['max_counter']:
                logging.error('giving up apprise notification')
            else:
                logging.error(
                    f'unable to send notification, retrying in {config["apprise"]["attempts"]["sleep_between_attempts_seconds"]}s'
                )
                time.sleep(config['apprise']['attempts']
                           ['sleep_between_attempts_seconds'])


if __name__ == '__main__':
    configuration_file = shlex.quote(sys.argv[1])
    config = yaml.load(open(configuration_file), Loader=yaml.SafeLoader)

    apobj = apprise.Apprise()
    apobj.add(config['apprise']['uris'])

    p = pathlib.Path(config['log']['directory'])
    p.mkdir(mode=0o700, exist_ok=True, parents=True)

    fc = FritzConnection(address=config['auth']['ip'],
                         password=config['auth']['password'])

    router_state: dict = fc.call_action('DeviceInfo1', 'GetInfo')
    ppp_state: dict = fc.call_action('WANPPPConnection1', 'GetInfo')
    connection_state: dict = fc.call_action('WANIPConn1', 'GetStatusInfo')

    system_uptime: int = router_state['NewUpTime']
    connection_uptime: int = connection_state['NewUptime']
    raw_logs: str = router_state['NewDeviceLog'].split(
        '\n')[:config['log']['device_log_store_messages']]

    status_dump: dict = {
        'system_uptime': system_uptime,
        'connection_uptime': connection_uptime,
        'latest_log_messages': raw_logs,
    }

    uptime_error: bool = False
    connection_error: bool = False
    data_file: pathlib.Path = pathlib.Path(p, 'data.json')
    if data_file.is_file():
        status_dump_old = json.loads(data_file.read_text())
        if status_dump['system_uptime'] < status_dump_old['system_uptime']:
            uptime_error = True
        if status_dump['connection_uptime'] < status_dump_old[
                'connection_uptime']:
            connection_error = True

    if uptime_error:
        notify(config, 'uptime seconds', status_dump['system_uptime'])
    if connection_error:
        notify(config, 'connection seconds', status_dump['connection_uptime'])
    if uptime_error or connection_error:
        notify(config, '(messages dump)', status_dump['latest_log_messages'])

    data_file.write_text(json.dumps(status_dump))
