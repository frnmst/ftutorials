#!/usr/bin/env python3
#
# notify_unit_status.py
#
# Copyright (C) 2015 Pablo Martinez @ Stack Exchange (https://serverfault.com/a/701100)
# Copyright (C) 2018 Davy Landman @ Stack Exchange (https://serverfault.com/a/701100)
# Copyright (C) 2020-2024 Franco Masotti
#
# This script is licensed under a
# Creative Commons Attribution-ShareAlike 4.0 International License.
#
# You should have received a copy of the license along with this
# work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
r"""Send a notification when a Systemd unit fails."""

import shlex
import sys

import apprise
import yaml

if __name__ == '__main__':
    configuration_file = shlex.quote(sys.argv[1])
    config = yaml.load(open(configuration_file), Loader=yaml.SafeLoader)
    failed_unit = shlex.quote(sys.argv[2])

    message = 'Systemd service failure: ' + failed_unit

    # Create an Apprise instance.
    apobj = apprise.Apprise()

    # Add all of the notification services by their server url.
    for uri in config['apprise_notifiers']['dest']:
        apobj.add(uri)

    apobj.notify(body=message, title=config['apprise_notifiers']['title'])
