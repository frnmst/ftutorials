#!/usr/bin/env bash
#
# keepalived_deploy.sh
#
# Copyright (C) 2022 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -euo pipefail

. "${1}"

SRC='/'
DST='/'
ENABLED_FILES=$(find enabled_files/* -type f)
SYSTEMD_DEPLOY_SERVICES=$(cat systemd_deploy_services.txt)

# Sync files.
for f in ${ENABLED_FILES}; do
    printf "%s\n" "${RSYNC_BASE} --files-from="${f}" "${SRC}" \
        "${USER}"@"${HOST}":"${DST}""
    ${RSYNC_BASE} --files-from="${f}" "${SRC}" "${USER}"@"${HOST}":"${DST}"
done

# Restart systemd services.
ssh "${USER}"@"${HOST}" "\
    systemctl daemon-reload \
    && systemctl reenable --all "${SYSTEMD_DEPLOY_SERVICES}" \
    && systemctl restart --all "${SYSTEMD_DEPLOY_SERVICES}" \
    && systemctl status --all --no-pager "${SYSTEMD_DEPLOY_SERVICES}""
