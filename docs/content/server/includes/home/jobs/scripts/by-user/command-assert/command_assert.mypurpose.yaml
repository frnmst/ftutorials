#
# command_assert.mypurpose.yaml
#
# Copyright (C) 2020-2024 Franco Masotti
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# The string that is used for the notifications
message status:
    ok: 'OK'
    error: 'ERROR'

# Default values if a process goes in timeout.
process in timeout:
    retval: -131072
    output: '<--##--##-->'

# XML feed header.
feed:
    enabled: true

    # Path of the XML feed file.
    # This file is most useful if served with a web server.
    feed: '/home/command-assert/out/command_assert.mypurpose.xml'

    # Path of the cache file.
    cache: '/home/jobs/scripts/by-user/command-assert/.command_assert.mypurpose.yml'

    total_last_feeds_to_keep: 128

    # Feed metadata.
    title: 'Outages of mypurpose'
    link: 'https://outage.my.domain'
    author_name: 'bot'
    author_email: 'myusername@gmail.com'
    description: 'Updates on outages'

commands:
    webserver SSL:
        # The command as you would execute in a shell.
        command: 'curl --head https://my-server.com'

        # {stdout,stderr,both}
        file_descriptor: 'stdout'

        # If set to true match for the exact expected_output.
        strict_matching: false

        # A pattern that needs to be matched in the output.
        # Regex are NOT supported.
        expected_output: 'Server: Apache'

        # The return value is usually 0 for successful processes.
        expected_retval: 0

        # Force kill the process after this time interval in seconds.
        timeout_interval: 5

        # if set to true, send notifications even if the process completes correctly.
        log_if_ok: false

        feed:
            enabled: true
            title: 'outage mypurpose'

            # use HTML.
            content: '<em>Sorry</em>, the webserver was down'

            description: 'outage mypurpose'

            # If an error already exists in cache for less than no_repeat_timeout_seconds,
            # then do not repeat the feed.
            no_repeat_timeout_seconds: 3600

    SSH server:
        command: 'ssh -p nonexistent@my-server.com'
        file_descriptor: 'stderr'
        strict_matching: false
        expected_output: 'NOTICE'
        expected_retval: 255
        timeout_interval: 5
        log_if_ok: false
        feed:
            enabled: true
            title: 'outage mypurpose'
            content: '<em>Sorry</em>, the SSH server was down'
            description: 'outage mypurpose'
            no_repeat_timeout_seconds: 3600

apprise_notifiers:
  # Follow the examples on
  # https://github.com/caronc/apprise
  dest:
    - 'nctalks://myuser:mypassword@myhost/roomid/'
    - 'mailtos://myusername:my%20awesome%20password?port=465&mode=ssl&from=myusername@gmail.com&to=myusername@gmail.com'
  title: 'command assert'
