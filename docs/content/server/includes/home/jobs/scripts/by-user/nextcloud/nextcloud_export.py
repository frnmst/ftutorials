#!/usr/bin/env python3
#
# nextcloud_export.py
#
# Copyright (C) 2023 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import io
import pathlib
import re
import sys
import urllib.parse
import urllib.request

import yaml

if __name__ == '__main__':
    configuration_file = sys.argv[1]
    config = yaml.load(open(configuration_file), Loader=yaml.SafeLoader)

    total_elements: int = len(config['element'])
    extension: str
    element_api_url: str
    element_error_message: str = 'This is the WebDAV interface. It can only be accessed by WebDAV clients such as the Nextcloud desktop sync client.'

    for i, c in enumerate(config['element']):
        print('getting Nextcloud element (' + str(i + 1) + ' of ' +
              str(total_elements) + ') ' + c)
        if config['element'][c]['type'] == 'calendar':
            element_api_url = ('https://' + config['element'][c]['host'] +
                               '/remote.php/dav/calendars/' +
                               config['element'][c]['user'] + '/' +
                               config['element'][c]['name'] + '?export')
            extension = 'vcs'
        elif config['element'][c]['type'] == 'address_book':
            element_api_url = ('https://' + config['element'][c]['host'] +
                               '/remote.php/dav/addressbooks/users/' +
                               config['element'][c]['user'] + '/' +
                               config['element'][c]['name'] + '?export')
            extension = 'vcf'
        elif config['element'][c]['type'] == 'news':
            element_api_url = ('https://' + config['element'][c]['host'] +
                               '/apps/news/export/opml')
            extension = 'opml'
        else:
            print('unsupported element: ' + config['element'][c]['type'])
            sys.exit(1)

        # Check if the computed string is a valid URL.
        try:
            element_string = urllib.parse.urlparse(element_api_url)
            if (element_string.scheme == '' or element_string.netloc == ''
                    or not re.match('^http(|s)', element_string.scheme)):
                raise ValueError
        except ValueError:
            print('URL parse error')
            sys.exit(1)

        # Get the element using credentials.
        # Send credentials immediately, do not wait for the 401 error.
        password_manager = urllib.request.HTTPPasswordMgrWithPriorAuth()
        password_manager.add_password(None,
                                      element_api_url,
                                      config['element'][c]['user'],
                                      config['element'][c]['password'],
                                      is_authenticated=True)

        handler = urllib.request.HTTPBasicAuthHandler(password_manager)
        opener = urllib.request.build_opener(handler)
        urllib.request.install_opener(opener)

        try:
            with urllib.request.urlopen(element_api_url) as response:
                nc_element: io.BytesIO = response.read()
            if nc_element.decode('UTF-8') == element_error_message:
                raise ValueError
        except (urllib.error.URLError, ValueError) as e:
            print(e)
            print('error getting element from the Nextcloud instance at ' +
                  element_api_url)
            sys.exit(1)

        # Save the element using the directory structure "basedir/username/element".
        top_directory = pathlib.Path(config['output_basedir'],
                                     config['element'][c]['user'])
        top_directory.mkdir(mode=0o755, parents=True, exist_ok=True)

        final_path = str(
            pathlib.Path(top_directory,
                         config['element'][c]['name'] + '.' + extension))
        with open(final_path, 'wb') as f:
            f.write(nc_element)
