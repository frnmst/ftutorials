#!/usr/bin/env python3
#
# nextcloud_calendar_to_pdf.py
#
# Copyright (C) 2022-2023 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# See more copyrights and licenses below.
import calendar
import datetime
import io
import pathlib
import shlex
import subprocess
import sys
import urllib.parse
import urllib.request

import yaml
from weasyprint import HTML

if __name__ == '__main__':
    configuration_file = sys.argv[1]
    config = yaml.load(open(configuration_file), Loader=yaml.SafeLoader)

    input_year: str = input('Year (empty for current) : ').zfill(4)
    input_month: str = input('Month (empty for current): ').zfill(2)
    if input_year == '0000':
        input_year = str(datetime.datetime.now().year).zfill(4)
    if input_month == '00':
        input_month = str(datetime.datetime.now().month).zfill(2)
    input('Are you sure, year=' + input_year + ' month=' + input_month + '? ')
    # Day is always fixed at 1.
    input_day: str = '01'
    input_date: str = input_year + input_month + input_day

    calendar_error_message: str = 'This is the WebDAV interface. It can only be accessed by WebDAV clients such as the Nextcloud desktop sync client.'

    try:
        # Format yyyymmdd
        ttime = datetime.datetime.strptime(input_date, '%Y%m%d')
        month_range: tuple = calendar.monthrange(ttime.year, ttime.month)
        # Pan days = current_month.days - 1
        pan_days: str = str(month_range[1] - 1)
    except ValueError:
        print('date parse error')
        sys.exit(1)

    calendar_api_uri: str = ('https://' + config['nextcloud']['host'] +
                             '/remote.php/dav/calendars/' +
                             config['nextcloud']['user'] + '/' +
                             config['nextcloud']['calendar_name'] + '?export')
    # Check if the computed string is a valid URL.
    try:
        nc_calendar_string = urllib.parse.urlparse(calendar_api_uri)
        if (nc_calendar_string.scheme == ''
                or nc_calendar_string.netloc == ''):
            raise ValueError
    except ValueError:
        print('URL parse error')
        sys.exit(1)

    # Get the calendar using credentials.
    # Send credentials immediately, do not wait for the 401 error.
    password_manager = urllib.request.HTTPPasswordMgrWithPriorAuth()
    password_manager.add_password(None,
                                  calendar_api_uri,
                                  config['nextcloud']['user'],
                                  config['nextcloud']['password'],
                                  is_authenticated=True)

    handler = urllib.request.HTTPBasicAuthHandler(password_manager)
    opener = urllib.request.build_opener(handler)
    urllib.request.install_opener(opener)

    try:
        with urllib.request.urlopen(calendar_api_uri) as response:
            nc_calendar: io.BytesIO = response.read()
        if nc_calendar.decode('UTF-8') == calendar_error_message:
            raise ValueError
    except (urllib.error.URLError, ValueError) as e:
        print(e)
        print('error getting element from the Nextcloud instance at ' +
              calendar_api_uri)
        sys.exit(1)

    # Transform the ical file to HTML.
    command: str = ('ical2html --footer=' + config['ical2html']['footer'] +
                    ' --timezone=' + config['ical2html']['timezone'] + ' ' +
                    input_date + ' P' + pan_days + 'D ' +
                    config['ical2html']['options'])
    command: str = shlex.split(command)
    try:
        process = subprocess.run(command,
                                 input=nc_calendar,
                                 check=True,
                                 capture_output=True)
    except subprocess.CalledProcessError:
        print('ical2html error')
        sys.exit(1)

    # Transform the HTML file to PDF.
    try:
        output_file = str(
            pathlib.Path(
                config['output_basedir'],
                config['nextcloud']['calendar_name'] + '_' + input_date +
                '_dayrange_' + str(month_range[1])))
        html = HTML(string=process.stdout)
        final_file = output_file + '.pdf'
        html.write_pdf(final_file)
        print('output file = ' + final_file)
    except (TypeError, ValueError):
        print('weasyprint error')
        sys.exit(1)
