#!/usr/bin/env bash
#
# nextcloud_calendar_to_pdf_laucher.sh
#
# Copyright (C) 2022-2023 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

#!/usr/bin/env bash

set -euo pipefail

pushd /home/jobs/scripts/by-user/myuser/nextcloud_calendar_to_pdf

# Create the virtual environment.
if [ ! -d '.venv' ]; then
    python3 -m venv .venv
    . .venv/bin/activate
    pip3 install --requirement requirements.txt
    deactivate
fi

. .venv/bin/activate && ./nextcloud_calendar_to_pdf.py ./nextcloud_calendar_to_pdf.yaml && deactivate

popd
