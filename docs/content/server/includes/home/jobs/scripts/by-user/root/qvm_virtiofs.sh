#!/usr/bin/env bash

set -euo pipefail

SOCKET_PATH="${1}"
SOURCE_DIR="${2}"
PID_FILE="${3}"

/usr/lib/qemu/virtiofsd --socket-path="${SOCKET_PATH}" -o source="${SOURCE_DIR}" -o cache=always &
pid=${!}
echo "virtiofsd pid: ${pid}"
sleep 1
chgrp --verbose kvm "${SOCKET_PATH}"
chmod --verbose g+rxw "${SOCKET_PATH}"
echo ${pid} > "${PID_FILE}"
wait ${pid}
