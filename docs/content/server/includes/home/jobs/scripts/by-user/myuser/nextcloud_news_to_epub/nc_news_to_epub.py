#!/usr/bin/env python3
#
# SPDX-FileCopyrightText: 2023 Franco Masotti
#
# SPDX-License-Identifier: MIT

import datetime
import json
import pathlib
import shlex
import shutil
import sys
from urllib.parse import urljoin

import fpyutils
import httpx
import nh3
import platformdirs
import yaml


def get_sources(base_url: str, user: str, password: str) -> dict:
    sources_endpoint: str = urljoin(base_url, 'feeds')

    try:
        r = httpx.get(sources_endpoint, auth=(user, password))
        r.raise_for_status()
    except httpx.HTTPError as exc:
        print(f'HTTP Exception for {exc.request.url} - {exc}')
        sys.exit(1)

    return json.loads(r.text)


def get_feeds(base_url, user: str, password: str, feed_id: int,
              number_of_feeds_per_source: int):
    latest_feeds_from_source_endpoint: str = urljoin(
        base_url, 'items?id=' + str(feed_id) + '&batchSize=' +
        str(number_of_feeds_per_source) + '&type=0&getRead=false&offset=0')

    try:
        r = httpx.get(latest_feeds_from_source_endpoint, auth=(user, password))
        r.raise_for_status()
    except httpx.HTTPError as exc:
        print(f'HTTP Exception for {exc.request.url} - {exc}')
        sys.exit(1)

    return json.loads(r.text)


def build_html(feed_struct: dict) -> str:
    url: str = feed_struct['url']
    title: str = feed_struct['title']
    author: str = feed_struct['author']
    pub_date: int = feed_struct['pubDate']
    if feed_struct['title'] is None:
        title = ''
    if feed_struct['url'] is None:
        url = ''
    if feed_struct['author'] is None:
        author = ''
    if feed_struct['pubDate'] is None:
        pub_date = ''
    else:
        pub_date = datetime.datetime.fromtimestamp(
            pub_date, datetime.timezone.utc).strftime('%Y-%m-%d %H:%M:%S')

    return ''.join([
        '<html><body><h1>', title, '</h1><h2>', url, '</h2><h2>', author,
        '</h2><h2>', pub_date, '</h2>', feed_struct['body'], '</body></html>'
    ])


def post_actions(base_url: str, user: str, password: str, feeds_ids_read: list,
                 action: str):
    try:
        r = httpx.post(urljoin(base_url, 'items/' + action + '/multiple'),
                       json={'itemIds': feeds_ids_read},
                       auth=(user, password))
        r.raise_for_status()
    except httpx.HTTPError as exc:
        print(f'HTTP Exception for {exc.request.url} - {exc}')
        sys.exit(1)


if __name__ == '__main__':

    def main():

        configuration_file = shlex.quote(sys.argv[1])
        config = yaml.load(open(configuration_file), Loader=yaml.SafeLoader)

        feeds_ids_read: list = list()

        base_url: str = urljoin('https://' + config['server']['domain'],
                                '/index.php/apps/news/api/v1-3/')

        # Remove cache dir.
        shutil.rmtree(str(
            pathlib.Path(platformdirs.user_cache_dir(), 'ncnews2epub')),
                      ignore_errors=True)
        # Create cache and output dirs.
        pathlib.Path(platformdirs.user_cache_dir(),
                     'ncnews2epub').mkdir(mode=0o700,
                                          parents=True,
                                          exist_ok=True)
        pathlib.Path(config['files']['output_directory']).mkdir(mode=0o700,
                                                                parents=True,
                                                                exist_ok=True)

        sources: dict = get_sources(base_url, config['server']['user'],
                                    config['server']['password'])

        for s in sources['feeds']:
            feeds: dict = get_feeds(
                base_url, config['server']['user'],
                config['server']['password'], s['id'],
                config['feeds']['number_of_feeds_per_source'])

            for feed in feeds['items']:
                # Build and sanitize HTML.
                html: str = nh3.clean(build_html(feed))

                feeds_ids_read.append(feed['id'])
                html_file_name: str = feed['guidHash'] + '.html'
                html_file: str = str(
                    pathlib.Path(platformdirs.user_cache_dir(), 'ncnews2epub',
                                 html_file_name))

                with open(html_file, 'w') as f:
                    f.write(html)

        # Get all '.html' files in the cache directory
        html_files: str = shlex.join([
            str(f) for f in pathlib.Path(platformdirs.user_cache_dir(),
                                         'ncnews2epub').iterdir()
            if f.is_file() and f.suffix == '.html'
        ])

        formatted_date: str = datetime.datetime.now(
            datetime.timezone.utc).strftime('%Y-%m-%d-%H-%M-%S')
        epub_filename: str = ''.join(
            ['nextcloud_news_export_', formatted_date, '.epub'])
        epub_full_path: str = str(
            pathlib.Path(config['files']['output_directory'], epub_filename))
        pandoc_command: str = ''.join([
            shlex.quote(config['binaries']['pandoc']),
            ' --from=html --metadata title="',
            str(pathlib.Path(epub_filename).stem), '" --to=epub ', html_files,
            ' -o ', epub_full_path
        ])

        fpyutils.shell.execute_command_live_output(pandoc_command)

        if config['feeds']['mark_read']:
            post_actions(
                base_url,
                config['server']['user'],
                config['server']['password'],
                feeds_ids_read,
                'read',
            )
        if config['feeds']['mark_starred']:
            post_actions(
                base_url,
                config['server']['user'],
                config['server']['password'],
                feeds_ids_read,
                'star',
            )

    main()
