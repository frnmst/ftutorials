#!/usr/bin/env bash

REPOSITORY='https://software.franco.net.eu.org/frnmst/python-packages-source.git'

pushd /home/jobs/scripts/by-user/python-source-packages-updater
export PATH=$PATH:/home/python-source-packages-updater/.local/bin/

git clone "${REPOSITORY}"

pushd python-packages-source

git checkout dev
git pull

# Always commit and push to dev only.
[ "$(git branch --show-current)" = 'dev' ] || exit 1

# Update all submodules and the stats.
make install-dev
make submodules-update
make submodules-add-gitea
make stats
git add -A
git commit -m "Submodule updates."
git push

popd

# Compile the packages.
./build_python_packages.py ./build_python_packages.yaml

# Cleanup.
rm -rf python-packages-source

popd
