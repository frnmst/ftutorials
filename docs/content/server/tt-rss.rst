Tiny Tiny RSS
=============

Upgrade
-------

Updating to a major TT-RSS version requires a database migration. This happens for example when
you upgrade from Debian 10 (TT-RSS 18) to Debian 11 (TT-RSS 21)

.. seealso::

   - _`Tiny Tiny RSS - ArchWiki` [#f1]_

#. make a backup of the database
#. update TT-RSS

   .. code-block:: shell-session

      sudo -u www-data -- /usr/bin/php /usr/share/tt-rss/www/update.php --update-schema=force-yes

Torification
------------

TT-RSS can download all feeds through TOR.

Some websites actively block TOR exit node addresses. In this case you will need
to configure an RSS bridge/proxy which can be simply a script that downloads
the RSS XML file through clearnet. This file must then be served on a system
that is accessible to your TT-RSS instance.

.. seealso::

   - _`Tiny Tiny RSS over TOR` [#f2]_

#. install TOR and Privoxy

   .. code-block:: shell-session

      apt-get install tor privoxy

#. Check that TOR is running

   .. code-block:: shell-session

      systemctl status tor@default.service

#. configure Privoxy. Add this content to the
   :download:`configuration file <includes/etc/privoxy/config>`

   .. literalinclude:: includes/etc/privoxy/config
      :language: ini
      :linenos:
      :caption: /etc/privoxy/config

   .. note:: You can add more forwarding rules to ignore routing the
             traffic through TOR for specific domains. Have a look
             at the ``/etc/privoxy/config`` file provided
             by the package which is well documented.

#. append the following at the end of
   :download:`TT-RSS's configuration file <includes/etc/tt-rss/config.php>`

   .. literalinclude:: includes/etc/tt-rss/config.php
      :language: php
      :linenos:
      :caption: /etc/tt-rss/config.php

   These variables should cover recent TT-RSS versions.

#. restart the services

   .. code-block:: shell-session

      systemctl restart tt-rss privoxy ${webserver}

Feed proxy
----------

Sometimes TT-RSS is unable to fetch feeds. You can use this script to act as
a bridge to download the XML files directly. You need a running
webserver, in this example `Apache HTTPD <https://httpd.apache.org/>`_,
and then point TT-RSS to the new RSS feed URLs.

#. install the dependencies

   .. code-block:: shell-session

      apt-get install python3-yaml

#. install fpyutils. See :ref:`reference <installation of fpyutils>`
#. create a new user

   .. code-block:: shell-session

      useradd -m -s /bin/bash -U rss
      passwd rss
      usermod -aG jobs rss

#. create the jobs directories. See :ref:`reference <jobs instructions>`

   .. code-block:: shell-session

      mkdir -p /home/jobs/{scripts,services}/by-user/rss

#. create the
   :download:`script <includes/home/jobs/scripts/by-user/rss/feed_proxy.py>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/rss/feed_proxy.py
      :language: python
      :linenos:
      :caption: /home/jobs/scripts/by-user/rss/feed_proxy.py

#. create a
   :download:`configuration file <includes/home/jobs/scripts/by-user/rss/feed_proxy.mypurpose.yaml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/rss/feed_proxy.mypurpose.yaml
      :language: yaml
      :linenos:
      :caption: /home/jobs/scripts/by-user/rss/feed_proxy.mypurpose.yaml

   .. important:: ``files.base_path`` must point to a
                  readable directory of the webserver, for example ``/var/www``

#. create the :download:`Systemd service unit file <includes/home/jobs/services/by-user/rss/feed-proxy.mypurpose.service>`

   .. literalinclude:: includes/home/jobs/services/by-user/rss/feed-proxy.mypurpose.service
      :language: ini
      :linenos:
      :caption: /home/jobs/services/by-user/rss/feed-proxy.mypurpose.service

   .. note:: You can use ``torsocks`` to retrieve feeds via TOR.

#. create the :download:`Systemd service timer unit file <includes/home/jobs/services/by-user/rss/feed-proxy.mypurpose.service>`

   .. literalinclude:: includes/home/jobs/services/by-user/rss/feed-proxy.mypurpose.timer
      :language: ini
      :linenos:
      :caption: /home/jobs/services/by-user/rss/feed-proxy.mypurpose.timer

#. add the ``rss`` user to the group of the user running the webserver,
   ``www-data``

    .. code-block:: shell-session

       usermod -aG www-data rss

#. run the :ref:`deploy script <deploy script>`

.. rubric:: Footnotes

.. [#f1] https://wiki.archlinux.org/title/Tiny_Tiny_RSS#Pacman_hook GNU Free Documentation License 1.3 or later, Copyright (c) ArchWiki contributors
.. [#f2] https://blog.franco.net.eu.org/notes/tt-rss-torification.html CC BY-SA 4.0, Copyright (c) 2021 Franco Masotti
