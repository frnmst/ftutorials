Ollama
======

All configurations presented here use docker-compose.
Read the :ref:`Docker <Docker>` instructions first.

.. warning:: This contents of this page have not been tested for all Ollama
             versions.

Ollama on Docker setup (CPU only)
---------------------------------

===================         ========================================================================================================
Variable name               Description
===================         ========================================================================================================
``DATA_PATH``               directory containing model files for Ollama
``OLLAMA_BASE_URL``         the base URL where the Ollama Docker instance is listening on, usually ``http://${ADDR}:11434``.
===================         ========================================================================================================

.. _ollama-basic-setup:

Basic setup
```````````

These instructions will cover the basic docker-compose setup to get Ollama
running.

.. seealso::

   - _`Ollama` [#f1]_
   - _`GitHub - ollama/ollama: Get up and running with Llama 3, Mistral, Gemma, and other large language models.` [#f2]_
   - _`ollama/envconfig/config.go at v0.4.2 · ollama/ollama · GitHub` [#f3]_
   - _`Global Configuration Variables for Ollama · Issue #2941 · ollama/ollama · GitHub` [#f4]_
   - _`Ollama crashes with Deepseek-Coder-V2-Lite-Instruct · Issue #6199 · ollama/ollama · GitHub` [#f5]_

#. follow the :ref:`Docker <Docker>` instructions
#. create the jobs directories. See :ref:`reference <jobs instructions>`

   .. code-block:: shell-session

      mkdir -p /home/jobs/scripts/by-user/root/docker/ollama
      cd /home/jobs/scripts/by-user/root/docker/ollama

#. create a :download:`Docker compose file <includes/home/jobs/scripts/by-user/root/docker/ollama/docker-compose.yml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/docker/ollama/docker-compose.yml
      :language: yaml
      :caption: /home/jobs/scripts/by-user/root/docker/ollama/docker-compose.yml

   .. note:: Replace these variables with the appropriate values

             - ``DATA_PATH``

   .. note:: These settings have been tested on CPU-only setups

#. create a :download:`Systemd unit file <includes/home/jobs/services/by-user/root/docker-compose.ollama.service>`.
   See also the :ref:`Docker compose services <Docker compose services>` section

   .. literalinclude:: includes/home/jobs/services/by-user/root/docker-compose.ollama.service
      :language: ini
      :caption: /home/jobs/services/by-user/root/docker-compose.ollama.service

#. run the :ref:`deploy script <deploy script>`

Improving performance
`````````````````````

To possibly improve performance you can change the CPU governor. I haven't
verified the imact of these settings.

.. seealso::

   - _`Set CPU governor to performance in 18.04 - Ask Ubuntu` [#f6]_
   - _`CPU frequency scaling - ArchWiki` [#f7]_

.. warning:: There might be some tools already managing the governor such as
             cpupower, thermald, power-profiles-daemon, etc... Before
             following the steps below make sure not to have any of them active
             as this might cause conflicts!

#. login as root

   .. code-block:: shell-session

      sudo -i

#. check the current governor

   .. code-block:: shell-session

      cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor

#. to change to the performance governor simply echo it to the device

   .. code-block:: shell-session

      echo performance | tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor

#. if you have an Intel CPU there is another setting you can check. It works
   in a similar way

   .. code-block:: shell-session

      cat /sys/devices/system/cpu/cpu*/power/energy_perf_bias

#. set this one to maximum performance

   .. code-block:: shell-session

      echo 0 | tee /sys/devices/system/cpu/cpu*/power/energy_perf_bias

#. to make these changes persistent add them to the root crontab

   .. code-block:: shell-session

      crontab -e

   A text editor will open. Add this to the first line after the comments

   .. code-block::

      @reboot /usr/bin/sleep 120 && echo performance | tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor && echo 0 | tee /sys/devices/system/cpu/cpu*/power/energy_perf_bias

Extras
------

===================         ========================================================================================================
Variable name               Description
===================         ========================================================================================================
``AUTH_TOKEN``              the authorization token to be used by HTTPS clients to connect to Ollama
===================         ========================================================================================================

.. _ollama-https-backend:

HTTPS backend with Authorization
````````````````````````````````

To be able to use Ollama remotely and in a safe manner, without having to
deal with SSH tunnelling, you can use a reverse proxy.

At the current stage Ollama does not support any kind of authentication. A
trick is to have something basic is by setting an authorization header in the
reverse proxy. You'll be limited to just one token.

In this example we'll use Apache HTTPD.

.. seealso::

   - _`Requesting support for basic auth or API key authentication · Issue #1053 · ollama/ollama · GitHub` [#f8]_

#. generate a random authorization token that you'll later need to copy. Use a
   Python shell

   .. code-block:: shell-session

      python3 -c 'import uuid; print(uuid.uuid4())'

#. serve Ollama via HTTPS by creating a new :download:`Apache virtual host <includes/etc/apache2/ollama.apache.conf>`.
   Include this file from the Apache configuration

   .. literalinclude:: includes/etc/apache2/ollama.apache.conf
      :caption: /etc/apache2/ollama.apache.conf

   .. note:: Replace these variables with the appropriate values

             - ``FQDN``
             - ``AUTH_TOKEN``

   .. note:: If you use Python's uuid4 function, the ``RewriteCond`` line can
      be something like:

      .. code-block::

         RewriteCond %{HTTP:Authorization} !^Bearer\s+db0d7378-f45a-432a-a795-ad48bd6da621$

      or

      .. code-block::

         RewriteCond %{HTTP:Authorization} !^Bearer\s+bfe7e7be-45f0-44d2-891e-038df7b74738$

#. test the connection

   #. with the correct header value

      .. code-block:: shell-session

         curl -H 'Authorization: Bearer ${AUTH_TOKEN}' https://${FQDN}

      results in

      .. code-block::

         Ollama is running

   #. with a wrong header value

      .. code-block:: shell-session

         curl -H 'Authorization: Bearer fake-000' https://${FQDN}

      results in

      .. code-block::

         <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
         <html><head>
         <title>401 Unauthorized</title>
         </head><body>
         <h1>Unauthorized</h1>
         <p>This server could not verify that you
         are authorized to access the document
         requested.  Either you supplied the wrong
         credentials (e.g., bad password), or your
         browser doesn't understand how to supply
         the credentials required.</p>
         <p>Additionally, a 401 Unauthorized
         error was encountered while trying to use an ErrorDocument to handle the request.</p>
         </body></html>

   #. without the header alltogether

      .. code-block:: shell-session

         curl https://${FQDN}

      results in

      .. code-block::

         <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
         <html><head>
         <title>401 Unauthorized</title>
         </head><body>
         <h1>Unauthorized</h1>
         <p>This server could not verify that you
         are authorized to access the document
         requested.  Either you supplied the wrong
         credentials (e.g., bad password), or your
         browser doesn't understand how to supply
         the credentials required.</p>
         <p>Additionally, a 401 Unauthorized
         error was encountered while trying to use an ErrorDocument to handle the request.</p>
         </body></html>

Android app
```````````

There are a couple of Android apps on F-Droid capable of connecting to Ollama.
For them to work it is required to follow :ref:`ollama-basic-setup` and
:ref:`ollama-https-backend`. You also need to pull some models: you can use
:doc:`open-webui` to perform this operation using an admin user.

.. seealso::

   - _`maid | F-Droid - Free and Open Source Android App Repository` [#f9]_
   - _`GPTMobile | F-Droid - Free and Open Source Android App Repository` [#f10]_

#. install GPTMobile
#. open the settings and fill in the API URL, key (authorization token) and
   model name
#. start a new chat

.. rubric:: Footnotes

.. [#f1] https://ollama.com/ unknown license
.. [#f2] https://github.com/ollama/ollama MIT License, Ollama contributors
.. [#f3] https://github.com/ollama/ollama/blob/v0.4.2/envconfig/config.go MIT License, Ollama contributors
.. [#f4] https://github.com/ollama/ollama/issues/2941 unknown license
.. [#f5] https://github.com/ollama/ollama/issues/6199 unknown license
.. [#f6] https://askubuntu.com/a/1084727 2018-2020, CC BY-SA 4.0, askubuntu.com contributors
.. [#f7] https://wiki.archlinux.org/title/CPU_frequency_scaling#Intel_performance_and_energy_bias_hint GNU Free Documentation License 1.3 or later license, ArchWiki contributors
.. [#f8] https://github.com/ollama/ollama/issues/1053#issuecomment-2253445923 unknown license
.. [#f9] https://f-droid.org/en/packages/com.danemadsen.maid/ unknown license
.. [#f10] https://f-droid.org/en/packages/dev.chungjungsoo.gptmobile/ unknown license
