RSS-Bridge
==========

Docker
------

.. seealso::

   - _`Docker Installation - RSS-Bridge` [#f1]_
   - _`Whitelisting - RSS-Bridge` [#f2]_
   - _`A self-hosted RSS setup // steve gattuso - Converting Social Networks to RSS` [#f3]_

#. follow the :ref:`Docker <Docker>` instructions
#. create the jobs directories

   .. code-block:: shell-session

      mkdir -p /home/jobs/scripts/by-user/root/docker/rssbridge
      cd /home/jobs/scripts/by-user/root/docker/rssbridge

#. create the whitelist file and enable the modules you want to use.
   If you use the asterisk character (``*``) all modules will be enabled

   .. code-block:: shell-session

      mkdir config
      echo '*' > config/whilelist.txt

#. create a :download:`Docker compose file <includes/home/jobs/scripts/by-user/root/docker/rssbridge/docker-compose.yml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/docker/rssbridge/docker-compose.yml
      :language: yaml
      :linenos:
      :caption: /home/jobs/scripts/by-user/root/docker/rssbridge/docker-compose.yml

#. create a :download:`Systemd unit file <includes/home/jobs/services/by-user/root/docker-compose.rssbridge.service>`.
   See also the :ref:`Docker compose services <Docker compose services>` section

   .. literalinclude:: includes/home/jobs/services/by-user/root/docker-compose.rssbridge.service
      :language: ini
      :linenos:
      :caption: /home/jobs/services/by-user/root/docker-compose.rssbridge.service

#. fix the permissions

   .. code-block:: shell-session

      chmod 700 /home/jobs/scripts/by-user/root/docker/rssbridge
      chmod 700 -R /home/jobs/services/by-user/root

#. run the :ref:`deploy script <deploy script>`
#. serve the files via HTTP by creating a new
   :download:`Apache virtual host <includes/etc/apache2/rssbridge-docker.apache.conf>`.
   Replace ``FQDN`` with the appropriate domain and include this file from the
   Apache configuration

   .. literalinclude:: includes/etc/apache2/rssbridge-docker.apache.conf
      :language: apache
      :linenos:
      :caption: /etc/apache2/rssbridge-docker.apache.conf

#. restart Apache

   .. code-block:: shell-session

      systemctl restart apache2.service

Bare metal
----------

#. install the dependencies

   .. code-block:: shell

      apt-get install rss-bridge php7.4-fpm

#. create the whitelist file and enable the modules you want to use.
   If you use the asterisk character (``*``) all modules will be enabled

   .. code-block:: shell-session

      echo '*' > /usr/share/rss-bridge/whitelist.txt

#. start and enable the php-fpm service

   .. code-block:: shell-session

      systemctl enable --now php7.4-fpm.service

#. enable the php7.4-fpm module for Apache

   .. code-block:: shell-session

      a2enmod php7.4

#. serve the files via HTTP by creating a new
   :download:`Apache virtual host <includes/etc/apache2/rssbridge-bare_metal.apache.conf>`.
   Replace ``FQDN`` with the appropriate domain and include this file from the
   Apache configuration

   .. literalinclude:: includes/etc/apache2/rssbridge-bare_metal.apache.conf
      :language: apache
      :linenos:
      :caption: /etc/apache2/rssbridge-bare_metal.apache.conf

#. restart Apache

   .. code-block:: shell-session

      systemctl restart apache2.service

.. rubric:: Footnotes

.. [#f1] https://rss-bridge.github.io/rss-bridge/For_Hosts/Docker_Installation.html The Unlicense, RSS-Bridge contributors
.. [#f2] https://rss-bridge.github.io/rss-bridge/For_Hosts/Whitelisting.html, RSS-Bridge contributors
.. [#f3] https://www.stevegattuso.me/2020/11/30/self-host-rss.html#converting-social-networks-to-rss CC BY-SA 4.0, ©2022 Steve Gattuso
