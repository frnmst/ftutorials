TOR
===

TOR services
-------------

Instructions to replicate an HTTP service both on clearnet and on TOR using
apache as a webserver.

Interfaces
``````````

Create a new IP address for the wired interface on a different subnet. This is
done for two reasons:

- to be able to distinguish between TOR and localhost traffic. This way programs
  like Fail2ban will still be able to detect abuses and ban the TOR address
  if necessary.

  You will also know if people connect to your onion services.

- to avoid exposing private information (if using ``127.0.0.1`` instead)

We will use ``192.168.0.1`` on the ``eth0`` interface as the existing network
address while ``192.168.1.1`` will be the listening address used by TOR services.

.. important:: Use an IP address of a subnet NOT used in your network setup to
               avoid interferences.

.. seealso::

   - _`IP addresses - Network configuration - ArchWiki` [#f1]_
   - _`NetworkConfiguration - Debian Wiki` [#f2]_

#. create a new address on the fly

   .. code-block:: shell-session

      ip address add 192.168.1.1/24 broadcast + dev eth0

#. check

   .. code-block:: shell-session

      ip a
      ping -c 1 192.168.1.1

#. add the new address in the :download:`interfaces file <includes/etc/network/interfaces>`

   .. literalinclude:: includes/etc/network/interfaces
      :language: ini
      :linenos:
      :emphasize-lines: 26-28
      :caption: /etc/network/interfaces

#. reboot and check if everything is still working

TOR configuration
`````````````````

In this example we use the following schema:

- all TOR services configuration are under ``/var/lib/tor/services``
- services are divided by protocols: for example ``http``, ``ssh``, etc...
- protocols are divided by clearnet domains (if existing)

For example:

::

    /var/lib/tor/services/http/docs.my.domain

.. seealso::

   - _`Hosting Onion Services - riseup.net` [#f3]_
   - _`Tor Project | Set up Your Onion Service` [#f4]_

#. install TOR

   .. code-block:: shell-session

      apt-get install tor

#. stop the TOR service

   .. code-block:: shell-session

      systemctl stop tor@default

#. create the service directories

   .. code-block:: shell-session

      cd /var/lib/tor
      mkdir -p services/http services/ssh
      chown -R debian-tor:debian-tor

#. add the highlighted lines to :download:`TOR's configuration <includes/etc/tor/torrc>`

   .. literalinclude:: includes/etc/tor/torrc
      :language: ini
      :linenos:
      :emphasize-lines: 75-77
      :caption: /etc/tor/torrc

#. restart TOR

   .. code-block:: shell-session

      systemctl start tor@default

#. check that TOR has created a new directory for the service

   .. code-block:: shell-session

      ls -ld /var/lib/tor/services/http/docs.my.domain

.. _onion hostname:

#. get the onion hostname

   .. code-block:: shell-session

      cat /var/lib/tor/services/http/docs.my.domain/hostname

   In this example the generated onion hostname is ``abc.onion``.

Apache
``````

If you are deploying a service both on clearnet and on TOR you have to create
a new virtual host. You can also optionally add a header in the clearnet
service that will redirect TOR browser automatically to the onion website.

#. copy the existing ``VirtualHost`` entry of your interest and remove all
   SSL related options such as ``<IfModule mod_ssl.c>``, ``SSLCertificateFile``
   ``SSLCertificateKeyFile``, etc...

#. change the server name to the onion host name. See previous :ref:`section <onion hostname>`.
   This will serve as the configuration for the TOR service.
   Include :download:`this file <includes/etc/apache2/tor_services.apache.conf>`
   from the Apache configuration

   .. literalinclude:: includes/etc/apache2/tor_services.apache.conf
      :language: apache
      :linenos:
      :lines: 2-12
      :emphasize-lines: 7
      :caption: /etc/apache2/tor_services.apache.conf

#. add the new header in the clearnet virtual host.
   Include :download:`this file <includes/etc/apache2/tor_services.apache.conf>`
   from the Apache configuration

   .. literalinclude:: includes/etc/apache2/tor_services.apache.conf
      :language: apache
      :linenos:
      :lines: 15-31
      :emphasize-lines: 10-12
      :caption: /etc/apache2/tor_services.apache.conf

#. restart Apache

   .. code-block:: shell-session

      systemctl restart apache2

#. test both services

   .. code-block:: shell-session

      curl --silent --head https://docs.my.domain | grep onion-location
      torsocks --isolate curl http://abc.onion

.. rubric:: Footnotes

.. [#f1] https://wiki.archlinux.org/title/Network_configuration#IP_addresses GNU Free Documentation License 1.3 or later, Copyright (c) ArchWiki contributors
.. [#f2] https://wiki.debian.org/NetworkConfiguration#Multiple_IP_addresses_on_one_Interface unknown license
.. [#f3] https://riseup.net/it/security/network-security/tor/onionservices-best-practices unknown license, Copyright (c) riseup contributors
.. [#f4] https://community.torproject.org/onion-services/setup/ unknown license, Copyright (c) Tor project contributors
