.. _Homebox:

Homebox
=======

.. seealso::

   - _`Home - Homebox` [#f1]_
   - _`GitHub - hay-kot/homebox: Homebox is the inventory and organization system built for the Home User` [#f2]_
   - _`Apache/2.4.52 (Ubuntu) VHOST/Reverse Proxy Help · Issue #707 · hay-kot/homebox · GitHub` [#f3]_

Homebox is a free and open source inventory and organization system.

Installation
------------

#. follow the :ref:`Docker <Docker>` instructions
#. create the jobs directories. See :ref:`reference <jobs instructions>`

   .. code-block:: shell-session

      mkdir -p /home/jobs/scripts/by-user/root/docker/homebox
      cd /home/jobs/scripts/by-user/root/docker/homebox

#. create a :download:`Docker compose file <includes/home/jobs/scripts/by-user/root/docker/homebox/docker-compose.yml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/docker/homebox/docker-compose.yml
      :language: yaml
      :caption: /home/jobs/scripts/by-user/root/docker/homebox/docker-compose.yml

   .. note:: Replace ``MAILER_FQDN``, ``MAILER_PASSWORD``, and ``DATA_PATH`` with appropriate values.

#. create a :download:`Systemd unit file <includes/home/jobs/services/by-user/root/docker-compose.homebox.service>`.
   See also the :ref:`Docker compose services <Docker compose services>` section

   .. literalinclude:: includes/home/jobs/services/by-user/root/docker-compose.homebox.service
      :language: ini
      :caption: /home/jobs/services/by-user/root/docker-compose.homebox.service

#. fix the permissions

   .. code-block:: shell-session

      chmod 700 /home/jobs/scripts/by-user/root/docker/homebox
      chmod 700 -R /home/jobs/services/by-user/root

#. run the :ref:`deploy script <deploy script>`
#. add a block by creating a new :download:`Apache virtual host <includes/etc/apache2/homebox.apache2.conf>`.
   Replace ``HOMEBOX_FQDN`` with the appropriate domain and include this file from the
   Apache configuration

   .. literalinclude:: includes/etc/apache2/homebox.apache2.conf
      :language: apache
      :caption: /etc/apache2/homebox.apache2.conf

#. restart Apache

   .. code-block:: shell-session

      systemctl restart apache

.. rubric:: Footnotes

.. [#f1] https://hay-kot.github.io/homebox/ unknown license
.. [#f2] https://github.com/hay-kot/homebox AGPL-3.0 license, Homebox contributors
.. [#f3] https://github.com/hay-kot/homebox/issues/707 unknown license
