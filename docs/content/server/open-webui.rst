Open WebUI
==========

All configurations presented here use docker-compose.
Read the :ref:`Docker <Docker>` instructions first.

.. warning:: This contents of this page have not been tested for all Open WebUI
             versions.

Open WebUI on Docker setup (CPU only)
-------------------------------------

===================         ========================================================================================================
Variable name               Description
===================         ========================================================================================================
``OLLAMA_BASE_URL``         the base URL where the Ollama Docker instance is listening on, usually ``http://${ADDR}:11434``.
===================         ========================================================================================================

Open WebUI on a server, Ollama on another
`````````````````````````````````````````

This method is used to reduce the load on the main server CPU by delegating the
AI work to another computer. Three steps are necessary:

1. configuring an SSH port forwarding between the main server and the secondary one. The main server
   connects via autossh, using an SSH key, to the secondary server
2. setting up Ollama on the secondary server
3. setting up Open WebUI on the main server

SSH setup
~~~~~~~~~

.. seealso::

   - _`SSH keys - ArchWiki` [#f1]_
   - _`OpenSSH - ArchWiki - Forwarding other ports` [#f2]_
   - _`OpenSSH - ArchWiki - Run autossh automatically at boot via systemd` [#f3]_

SSH port forwarding is one possible method used by the server running Open WebUI
to communicate with the server running Ollama. The advantages of this are:

- encrpyted communication
- Ollama server can be placed on a remote, non-LAN host easily

#. install the dependencies

   .. code-block:: shell-session

      apt-get install autossh

#. create the SSH key

   .. code-block:: shell-session

      mkdir /root/.ssh
      chmod 700 .ssh
      cd .ssh
      ssh-keygen -t rsa -b 16384 -C "$(whoami)@$(uname -n)-$(date -I)"

   Use ``otherserver_root`` when prompted for the file name

   .. important:: Do not set a passphrase when prompted since a Systemd script
                  will need to interact with this key.

#. copy the local SSH pubkey to the remote server in ``/root/.ssh/authorized_keys``
#. update the :download:`local SSH configuration <includes/root/.ssh/config>`

   .. literalinclude:: includes/root/.ssh/config
      :language: ini
      :caption: /root/.ssh/config

#. add this section to the OpenSSH configuration of the secondary server

   .. literalinclude:: includes/etc/ssh/sshd_config
      :language: ini
      :caption: /etc/ssh/sshd_config

#. on the main server, test the SSH connection: you should not be prompted for a password

   .. code-block:: shell-session

      ssh root@otherserver

#. create a :download:`Systemd unit file <includes/home/jobs/services/by-user/root/autossh.otherserver.service>`

   .. literalinclude:: includes/home/jobs/services/by-user/root/autossh.otherserver.service
      :language: ini
      :caption: /home/jobs/services/by-user/root/autossh.otherserver.service

#. run the :ref:`deploy script <deploy script>`

Ollama setup
~~~~~~~~~~~~

See the :doc:`ollama` page. This setup is to be done on the secondary server.

Open WebUI setup
~~~~~~~~~~~~~~~~

This setup is to be done on the main server.

.. seealso::

   - _`Open WebUI` [#f6]_
   - _`GitHub - open-webui/open-webui: User-friendly WebUI for LLMs (Formerly Ollama WebUI)` [#f7]_
   - _`GitHub - open-webui/open-webui: User-friendly WebUI for LLMs (Formerly Ollama WebUI) - Error on Slow Responses for Ollama` [#f8]_

#. follow the :ref:`Docker <Docker>` instructions
#. create the jobs directories. See :ref:`reference <jobs instructions>`

   .. code-block:: shell-session

      mkdir -p /home/jobs/scripts/by-user/root/docker
      cd /home/jobs/scripts/by-user/root/docker

#. clone the repository

   .. code-block:: shell-session

      git clone https://github.com/open-webui/open-webui.git openwebui

#. create a :download:`Docker compose file <includes/home/jobs/scripts/by-user/root/docker/openwebui/docker-compose.yml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/docker/openwebui/docker-compose.yml
      :language: yaml
      :caption: /home/jobs/scripts/by-user/root/docker/openwebui/docker-compose.yml

   .. note:: Replace these variables with the appropriate values

             - ``OLLAMA_BASE_URL``: since we are using SSH port forwarding, the
               address used must be the same as the ``inet`` or ``inet6``
               address you get from ``$ ip a`` on the main ethernet interface.
               For example, if your address is ``192.168.0.2``, ``OLLAMA_BASE_URL``
               might be ``http://192.168.0.2:11434``


#. remove the original docker-compose file from the repository

   .. code-block:: shell-session

      rm docker-compose.yaml

#. build the Docker image

   .. code-block:: shell-session

      docker-compose build

#. create a :download:`Systemd unit file <includes/home/jobs/services/by-user/root/docker-compose.openwebui.service>`.
   See also the :ref:`Docker compose services <Docker compose services>` section

   .. literalinclude:: includes/home/jobs/services/by-user/root/docker-compose.openwebui.service
      :language: ini
      :caption: /home/jobs/services/by-user/root/docker-compose.openwebui.service

#. run the :ref:`deploy script <deploy script>`
#. modify the reverse proxy port of your web server configuration with ``4018``

.. rubric:: Footnotes

.. [#f1] https://wiki.archlinux.org/title/SSH_keys GNU Free Documentation License 1.3 or later, Copyright (c) ArchWiki contributors
.. [#f2] https://wiki.archlinux.org/title/OpenSSH#Forwarding_other_ports GNU Free Documentation License 1.3 or later, Copyright (c) ArchWiki contributors
.. [#f3] https://wiki.archlinux.org/title/OpenSSH#Run_autossh_automatically_at_boot_via_systemd GNU Free Documentation License 1.3 or later, Copyright (c) ArchWiki contributors
.. [#f6] https://openwebui.com/ unknown license
.. [#f7] https://github.com/open-webui/open-webui MIT license, Copyright (c) 2023 Timothy Jaeryang Baek
.. [#f8] https://github.com/open-webui/open-webui/blob/main/TROUBLESHOOTING.md#error-on-slow-responses-for-ollama MIT license, Open WebUI contributors
