.. _Docker:

Docker
======

Migration from Docker repositories to Debian official repositories
------------------------------------------------------------------

If you installed Docker using the Docker repository
(:ref:`reference <Install Docker Engine on Debian>`) you can migrate to
the official Debian stable repository

.. seealso::

   - _`Install Docker Engine on Debian` [#f1]_

#. stop the existing docker daemon

   .. code-block:: shell-session

      systemctl stop docker docker.socket

#. backup all files related to docker, for example ``/etc/docker/daemon.json``
   , the service file (if you modified it) and the Docker file directory

#. remove the existing Docker packages

   .. code-block:: shell-session

      apt-get purge docker-ce docker-ce-cli containerd.io

#. install the new packages

   .. code-block:: shell-session

      apt-get update
      apt-get install --reinstall docker docker.io containerd

#. check that the Docker daemon is working

   .. code-block:: shell-session

      systemctl status docker docker.socket

#. remove the Docker repository from ``/etc/apt/sources.list``

docker-compose
--------------

Installation
````````````

.. seealso::

   - _`Docker - ArchWiki` [#f2]_

#. install Docker and docker-compose

   .. code-block:: shell-session

       apt-get update
       apt-get install docker docker.io containerd docker-compose

#. create the jobs directories. All docker compose files
   will be under ``/home/jobs/scripts/by-user/root/docker/${project_name}``

   .. code-block:: shell-session

      mkdir -p /home/jobs/scripts/by-user/root/docker
      chmod 700 /home/jobs/scripts/by-user/root/docker

#. make sure that Docker containers uses network addresses from a known range. Edit the :download:`Docker daemon file <includes/etc/docker/daemon.json>`

   .. literalinclude:: includes/etc/docker/daemon.json
      :language: json
      :linenos:
      :caption: /etc/docker/daemon.json

#. restart Docker

   .. code-block:: shell-session

      systemctl restart docker.service docker.socket

#. check that the Docker daemon is still working

   .. code-block:: shell-session

      systemctl status docker docker.socket

.. note:: If you installed docker-compose using the ``root`` user from pip you
          must first manually remove docker-compose and all its dependencies
          from ``/usr/local/lib/python3.*/dist-packages/`` using
          ``pip uninstall`` as command.

.. _Docker compose services:

docker-compose services
```````````````````````

.. seealso::

   - _`Run multiple Docker Compose services on Debian/Ubuntu` [#f3]_

All docker compose services reported here are an adaptation of :ref:`this reference <Run multiple Docker Compose services on Debian/Ubuntu>`.

Go into the directory containing the compose file and test the service with

.. code-block:: shell-session

   docker-compose pull
   docker-compose up --remove-orphans

Run the following to quit the service

.. code-block:: shell-session

   docker-compose down --remove-orphans

.. rubric:: Footnotes

.. [#f1] https://docs.docker.com/engine/install/debian/#install-using-the-repository Apache License, copyright (c) Docker contributors
.. [#f2] https://wiki.archlinux.org/title/docker#Images_location GNU Free Documentation License 1.3 or late, copyright (c) ArchWiki contributors
.. [#f3] https://community.hetzner.com/tutorials/docker-compose-as-systemd-service MIT License, copyright (c) 2019 Hetzner Online GmbH
