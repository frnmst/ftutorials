.. _Storj:

Storj
=====

docker-compose
--------------

Storj is a distributed filesystem where node operators are rewarded with
cryptocurrency for storing data.

.. seealso::

   - _`Sign up and host a Node on Storj` [#f1]_
   - _`Identity | Storj Docs` [#f2]_
   - _`QUIC requirements | Storj Docs` [#f3]_
   - _`Storage Node | Storj Docs` [#f4]_
   - _`How do I hold STORJ? What is a valid address or compatible wallet? – Storj` [#f5]_

#. follow the :ref:`Docker <Docker>` instructions
#. create an ERC-20 compatible wallet
#. open the 28967 TCP and UDP ports on your firewalls. These ports are for data
   transfers. Port 14002 is instead used to monitor your Storj node with a web browser.
   Port 14002 is not required to be exposed
#. create the jobs directories

   .. code-block:: shell-session

      mkdir -p /home/jobs/scripts/by-user/root/docker/storj

#. create a :download:`Docker compose file <includes/home/jobs/scripts/by-user/root/docker/storj/docker-compose.yml>`

   .. literalinclude:: includes/home/jobs/scripts/by-user/root/docker/storj/docker-compose.yml
      :language: yaml
      :linenos:
      :caption: /home/jobs/scripts/by-user/root/docker/storj/docker-compose.yml

   .. note:: Replace ``MAIN_DOMAIN``, ``WALLET_ADDRESS`` and ``DATA_PATH`` with appropriate values.

#. download the identity executable

   .. code-block:: shell-session

      cd /home/jobs/scripts/by-user/root/docker/storj
      curl -L https://github.com/storj/storj/releases/latest/download/identity_linux_amd64.zip -o identity_linux_amd64.zip
      unzip -o identity_linux_amd64.zip
      chmod +x identity

#. create the authorization token from the `host a node page <https://www.storj.io/host-a-node>`_
#. create and authorize the identity

   .. code-block:: shell-session

      ./identity create storagenode
      ./identity authorize storagenode ${authorization_token}

#. move and backup the identity files

   .. code-block:: shell-session

      cp -aR /root/.local/share/storj/identity/storagenode identity
      cp -aR /root/.local/share/storj key_backup
      rm -rf /root/.local/share/storj

#. run the setup

   .. code-block:: shell-session

      docker-compose up --remove orphans
      docker-compose down --remove orphans

#. set the ``SETUP`` environment variable to ``false`` in the docker-compose file
#. use this :download:`Systemd service unit file <includes/home/jobs/services/by-user/root/docker-compose.storj.service>`

   .. literalinclude:: includes/home/jobs/services/by-user/root/docker-compose.storj.service
      :language: ini
      :linenos:
      :caption: /home/jobs/services/by-user/root/docker-compose.storj.service

#. fix the permissions

   .. code-block:: shell-session

      chmod 700 /home/jobs/scripts/by-user/root/docker/storj/docker-compose.yml
      chmod 700 -R /home/jobs/services/by-user/root

#. run the :ref:`deploy script <deploy script>`

.. rubric:: Footnotes

.. [#f1] https://www.storj.io/host-a-node
.. [#f2] https://docs.storj.io/node/dependencies/identity/
.. [#f3] https://docs.storj.io/node/dependencies/quic-requirements/
.. [#f4] https://docs.storj.io/node/setup/cli/storage-node/
.. [#f5] https://support.storj.io/hc/en-us/articles/360026611692-How-do-I-hold-STORJ-What-is-a-valid-address-or-compatible-wallet
