Meta
====

Source code
-----------

- https://software.franco.net.eu.org/frnmst/ftutorials
- https://codeberg.org/frnmst/ftutorials
- https://framagit.org/frnmst/ftutorials

Workflow
--------

See :doc:`Python workflow <./content/programming/python/workflow>`.

Contributing
------------

See :doc:`Python contributing <./content/programming/python/contributing>`.

Copyright and License
---------------------

Unless otherwise noted:

ftutorials (c) by Franco Masotti

ftutorials is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
