# ftutorials

[![Buy me a coffee](assets/buy_me_a_coffee.svg)](https://buymeacoff.ee/frnmst)

Documentation for service deployments and other tips.

<!--TOC-->

- [ftutorials](#ftutorials)
  - [Documentation](#documentation)
    - [Update](#update)
  - [License](#license)
  - [Changelog and trusted source](#changelog-and-trusted-source)
  - [Support this project](#support-this-project)

<!--TOC-->

## Documentation

<https://docs.franco.net.eu.org/ftutorials/>

### Update

Run `./deploy_docs.sh`

## License

Unless otherwise noted:

ftutorials (c) by [Franco Masotti](https://blog.franco.net.eu.org/about/#contacts)

ftutorials is licensed under a Creative Commons Attribution-ShareAlike
4.0 International License.

You should have received a copy of the license along with this work. If
not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

## Changelog and trusted source

You can check the authenticity of new releases using my public key.

Changelogs, instructions, sources and keys can be found at
[blog.franco.net.eu.org/software/#ftutorials](https://blog.franco.net.eu.org/software/#ftutorials).

## Support this project

- [Buy Me a Coffee](https://www.buymeacoffee.com/frnmst)
- [Liberapay](https://liberapay.com/frnmst)
- Bitcoin: `bc1qnkflazapw3hjupawj0lm39dh9xt88s7zal5mwu`
- Monero: `84KHWDTd9hbPyGwikk33Qp5GW7o7zRwPb8kJ6u93zs4sNMpDSnM5ZTWVnUp2cudRYNT6rNqctnMQ9NbUewbj7MzCBUcrQEY`
- Dogecoin: `DMB5h2GhHiTNW7EcmDnqkYpKs6Da2wK3zP`
- Vertcoin: `vtc1qd8n3jvkd2vwrr6cpejkd9wavp4ld6xfu9hkhh0`
